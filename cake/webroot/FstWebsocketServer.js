/**
 * websocket server na vymenu dat 
 */
var http = require('http');
var Moo = require('requireg')('mootools');
var os = require("os");
var fs = require("fs");
var WebSocketServer = require('requireg')('websocket').server;


var FstWebsocketServer = new Class({
	port: 8005,
	debug: true,
	serverTime: null,
	domain: 'http://'+os.hostname(),
	// domain: '127.0.0.1',
	Implements: [],
    
    lockList: {}, // zamcene ID
    connectionsDevices: {}, // pripojene zarizeni
	connectionIDCounter : [], // pocitadlo pripojenych
	options: {
		
	},


	serverData: {},
	
	initialize: function(options){
        if (this.debug) console.log('initialize fst websocket server');
     	this.create_websocket_server();
	},
    

    /** 
     * create websocket server
    */
	create_websocket_server: function(){
		// vytvoreni serveru 
		var server = http.createServer(function(request, response) {
			console.log((new Date()) + ' Received request for ' + request.url);
			response.writeHead(404); 
			response.end();
		});
		// server.domain = this.domain;
		// console.log(server);
		
		server.listen(this.port, function() {
            var date = new Date().toLocaleDateString("cs-CS") +' '+new Date().toLocaleTimeString("cs-CS");
            console.log((date) + ' Server is domain '+this.domain);
			console.log((date) + ' Server is listening on port '+this.port);
		}.bind(this));

		// vyrvoreni websocket serveru
		wsServer = new WebSocketServer({ 
			httpServer: server,
			maxReceivedFrameSize: 0x100000000,
			maxReceivedMessageSize: 0x100000000,
			fragmentOutgoingMessages: true,
			fragmentationThreshold: 0x4000,
			 
		});

		
		function originIsAllowed(origin) {
		  // put logic here to detect whether the specified origin is allowed.
		  return true;
		}

        /**
         * pokud prichazi request
         */
		wsServer.on('request', (function(request) {
			
			if (!originIsAllowed(request.origin)) {
			  // Make sure we only accept requests from an allowed origin
			  request.reject();
			  console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.');
			  return;
			}
			
			var connection = request.accept(null, request.origin);

            /** prichazi message od klienta */
			connection.on('message', (function(message) {
				// console.log(message);
				this.socket_receive(connection,message);
			}).bind(this));
			
			
			/** spojeni od klienta uzavreno */
			connection.on('close', (function(reasonCode, description) {
				//console.log((new Date()) + ' Peer ' + connection.remoteAddress +    ' disconnected. ' + "Connection ID: " + connection.id);
                
				// Make sure to remove closed connections from the global pool
				if (typeof system_id != 'undefined' && this.connectionsDevices[system_id] && this.connectionsDevices[system_id]['clients'][connection.id]){
					
						delete this.connectionsDevices[system_id]['clients'][connection.id];
						
						// zjisteni pocet pripojenych 
						this.countClientsConnected(system_id);
						
						
						
					
					}
				
			}).bind(this));
			
		}).bind(this));
	},
	
	/**
	 * poslani vsem pocet pripojenych klientu
	 */
	sendAllConnectedClients: function(system_id,data){
		if (system_id) {
			console.log('send data from server');
			Object.each(this.connectionsDevices[system_id]['clients'],(function(client,keyClient){
				sendTo = this.connectionsDevices[system_id]['clients'][keyClient];
				console.log(keyClient);
				console.log(jsonData.serverData);
				this.socketSendMessage(sendTo,jsonData);

			}).bind(this));
		}
	},
	
	/**
	 * poslani zpravy na client side
	 */
	socketSendMessage: function(sendTo,jsonData){
		// console.log(sendTo);
		time = Number(new Date());
		jsonData.serverTime =  time;
		// console.log('send ',jsonData)
		console.log('send DATA to client',jsonData)
		sendTo.send(
			JSON.stringify(jsonData)
		);

	},

    
	// receive data from client
	socket_receive: function(connection,message){
		if (message.type === 'utf8') {
			// console.log('Received Message: ' + message.utf8Data);
			var message = message.utf8Data;
			if (message){
				var mdata = JSON.parse(message);
				
					this.serverTime = mdata.time;
					switch(mdata.type){
						case 'init':
						break;
					}
					// init connection list
					if (mdata.type == 'init'){
						this.messageInit(connection,mdata);		
					}
							
					// prichozi data od klienta
					if (mdata.type == 'sendData'){
						this.messageSendData(connection,mdata);		
					}
					
					// get server data
					if (mdata.type == 'getServerData'){
						this.getServerData(connection,mdata);		
					}
					
					// set lock list
					if (mdata.type == 'lock'){
						this.setLockList(connection,mdata);		
					}
						
			} 
		} else if (message.type === 'binary') {
			//console.log('Received Binary Message of ' + message.binaryData.length + ' bytes');
			//connection.sendBytes(message.binaryData);
		}
	},
	

	/**
	 * set lock list
	 */
	setLockList: function(connection,jsonData){
		// console.log(jsonData);
		if (jsonData){
			// if (!this.lockList[jsonData.elementId]){
				// this.lockList[jsonData.elementId] = jsonData.terminal_id;
				this.lockList[jsonData.terminal_id] = {
					'table_group_index':jsonData.table_group_index,
					'table_index':jsonData.table_index,
					'terminal_name':jsonData.terminal_name,
					'terminal_id':jsonData.terminal_id,
				}; 
				
				// jsonData.terminal_id;

			// }
		} else {
			jsonData = {};
		}
		console.log('aaa',this.lockList);
		Object.each(this.connectionsDevices[system_id]['clients'],(function(client,keyClient){
			jsonData.type = 'lockElement';
			jsonData.serverData = this.lockList;
			// if (this.connectionsDevices[system_id]['clients'][keyClient].id != connection.id){
				sendTo = this.connectionsDevices[system_id]['clients'][keyClient];
				// console.log(keyClient);
				this.socketSendMessage(sendTo,jsonData);
			// } else {
			// 	sendTo = this.connectionsDevices[system_id]['clients'][keyClient];
				
			// 	// console.log(keyClient);
			// 	this.socketSendMessage(sendTo,{'empty':true});
				
			// }

		}).bind(this));
	

	},
    
    /**
     * receive message type: init
     */
    messageInit: function(connection,data){

        system_id = data.system_id;
							
        if (!this.connectionIDCounter[system_id]) this.connectionIDCounter[system_id] = 0;
        if (!this.connectionsDevices[system_id]) {
            this.connectionsDevices[system_id] = {
                'system_id':system_id,
                'clients':{},
            }
        }
        connection.id = this.connectionIDCounter[system_id] ++;
        this.connectionsDevices[system_id]['clients'][connection.id] = connection;

        // console.log('connections',this.connectionsDevices);
        //console.log('id',connection.id);
        
        // this.create_socket_client(system_id,connection.id,this.connections);
        //if (this.socket_clients[system_id])
		this.countClientsConnected(system_id);
					
        
	},

	/**
	 * message receive sendData
	 */
	messageSendData: function(connection,data){
		console.log('Received time: ' + data.time);
		console.log('server time: ' + this.serverTime);
		console.log('receive data: ');
		// console.log(data.jsonData);
		if (data.time < this.serverTime){ 
			console.log('error time');
			jsonData = {
				'type':'serverData',
				'serverData':this.serverData,
				'message':'Máte neaktuální data',
				'countClients':Object.keys(this.connectionsDevices[system_id]['clients']).length
			};
			// console.log(jsonData);
			this.sendAllConnectedClients(system_id,jsonData);
			
		} else {
			this.serverData = data.jsonData;
			jsonData = {
				'type':'serverData',
				'serverData':this.serverData,
				'countClients':Object.keys(this.connectionsDevices[system_id]['clients']).length
			};
			// console.log(jsonData);
			this.sendAllConnectedClients(system_id,jsonData);
			
			//console.log(this.serverData);
		}
	},
	
	/**
	 * message receive test
	 */
	messageTest: function(connection,data){
		// console.log(data);
		console.log('Received time: ' + data.time);
		console.log('server time: ' + this.serverTime);
		if (data.time < this.serverTime){ 
			console.log('error time');
		
		} else {
			// console.log(this.lockList[data.lockId]);
			if (this.lockList[data.lockId]){
				delete this.lockList[data.lockId];
			}
			// console.log(this.lockList[data.lockId]);
			this.setLockList(connection);

			this.serverData = data.data;
			// console.log(this.serverData);	
			Object.each(this.connectionsDevices[system_id]['clients'],(function(client,keyClient){
				jsonData.type = 'test';
				jsonData.serverData = this.serverData;
				sendTo = this.connectionsDevices[system_id]['clients'][keyClient];
				// console.log(keyClient);
				this.socketSendMessage(sendTo,jsonData);

			}).bind(this));
		}
	},

	

	/**
	 * get server data
	 */
	getServerData: function(connection,data){
		//console.log(connection.id);
		jsonData = { 
			'type':'serverData',
			'serverData':this.serverData
		};
		sendTo = this.connectionsDevices[system_id]['clients'][connection.id];
		// console.log(keyClient);
		console.log('send server data',jsonData);
		this.socketSendMessage(sendTo,jsonData);

	},

	/**
	 * pocet pripojenych klientu
	 */
	countClientsConnected: function(system_id){
		if (this.debug){
			console.log(new Date().toLocaleTimeString() + ' pocet pripojenych clients SYSTEM_ID '+system_id+':: '+Object.keys(this.connectionsDevices[system_id]['clients']).length+' ks');

		}
		// poslani vsem kolik je pripojeno
		jsonData = {
			'type':'connectedResult',
			'connected':true,
			'countClients':Object.keys(this.connectionsDevices[system_id]['clients']).length
		};
		this.sendAllConnectedClients(system_id,jsonData);
		
	},
	
	
    
    
    
    
	
});
var fst_server = new FstWebsocketServer();	


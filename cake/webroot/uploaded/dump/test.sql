-- MySQL dump 10.16  Distrib 10.1.25-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: pokladna
-- ------------------------------------------------------
-- Server version	10.1.25-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `zdroj_id` int(11) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `trash` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=100 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,'test',1,'2017-10-14 11:12:13',NULL,NULL,1),(31,'test9',1,'2017-10-14 11:12:13','2017-10-25 04:06:29',NULL,1),(30,'test8',1,'2017-10-14 11:12:13','2017-10-25 04:09:21',NULL,1),(29,'test7',1,'2017-10-14 11:12:13','2017-10-25 04:40:17',NULL,1),(28,'test6',1,'2017-10-14 11:12:13','2017-10-01 11:12:13',NULL,1),(27,'test5',1,'2017-10-14 11:12:13','2017-10-19 11:12:13',NULL,1),(26,'test4',1,'2017-10-14 11:12:13','2017-10-19 11:12:13',NULL,1),(25,'test3',1,'2017-10-21 11:12:13','2017-10-21 11:12:13',NULL,1),(24,'test2',1,'2017-10-20 11:12:13','2017-10-20 11:12:13',NULL,1),(32,'Viktor Gáborík',1,'2017-10-24 23:22:39','2017-10-24 23:22:39',NULL,1),(33,'Radim Sewerin',1,'2017-10-24 23:19:40','2017-10-24 23:19:40',NULL,1),(34,'Jan  Kičin',1,'2017-10-24 23:16:41','2017-10-24 23:16:41',NULL,1),(35,'Roman Kvasnička',1,'2017-10-24 23:02:21','2017-10-24 23:02:21',NULL,1),(36,'David Ohradka',1,'2017-10-24 22:50:25','2017-10-24 22:50:25',NULL,1),(37,'Pavel Topič',1,'2017-10-24 22:46:43','2017-10-24 22:46:43',NULL,1),(38,'Roman  Klein',1,'2017-10-24 22:46:08','2017-10-24 22:46:08',NULL,1),(39,'Dominik Vavrla',1,'2017-10-24 22:43:49','2017-10-24 22:43:49',NULL,1),(40,'X Martin Cimba',3,'2017-10-24 22:43:03','2017-10-24 22:43:03',NULL,1),(41,'Filip Kotkolík',1,'2017-10-24 22:41:53','2017-10-24 22:41:53',NULL,1),(42,'Mates Harušťák',1,'2017-10-24 22:39:38','2017-10-24 22:39:38',NULL,1),(43,'Sofia Ploskonkova',1,'2017-10-24 22:34:06','2017-10-24 22:34:06',NULL,1),(44,'Petr Velca',1,'2017-09-02 22:33:07','2017-09-02 22:33:07',NULL,1),(45,'Patrik Vošmík',1,'2017-10-24 22:31:55','2017-10-24 22:31:55',NULL,1),(46,'Jan Sedlačík',1,'2017-10-24 22:31:44','2017-10-24 22:31:44',NULL,1),(47,'Michal Jeřábek',1,'2017-10-24 22:30:45','2017-10-24 22:30:45',NULL,1),(48,'X Nicole Nozar',3,'2017-10-24 22:29:39','2017-10-24 22:29:39',NULL,1),(49,'X Aleš Vozňák',3,'2017-10-24 22:29:35','2017-10-24 22:29:35',NULL,1),(50,'Lucie Buková',1,'2017-10-24 22:28:52','2017-10-24 22:28:52',NULL,1),(51,'Daniel Sura',1,'2017-10-24 22:28:46','2017-10-24 22:28:46',NULL,1),(52,'X Michal Kotrba',3,'2017-10-24 22:26:17','2017-10-24 22:26:17',NULL,1),(53,'Barbora  Fellerová ',1,'2017-10-24 22:26:17','2017-10-24 22:26:17',NULL,1),(54,' Veronika Sedláčková',1,'2017-10-24 22:23:46','2017-10-24 22:23:46',NULL,1),(55,'X Eva Kokertová',3,'2017-10-24 22:20:05','2017-10-24 22:20:05',NULL,1),(56,'X Adrián andraščík',3,'2017-10-24 22:18:49','2017-10-24 22:18:49',NULL,1),(57,'X Richard',3,'2017-10-24 22:14:29','2017-10-24 22:14:29',NULL,1),(58,'X František Maša',3,'2017-10-24 22:14:21','2017-10-24 22:14:21',NULL,1),(59,'Martin Hejna',1,'2017-10-24 22:13:28','2017-10-24 22:13:28',NULL,1),(60,'Jan Sabo',1,'2017-10-24 22:13:14','2017-10-24 22:13:14',NULL,1),(61,'Marek Toth',1,'2017-10-24 22:10:54','2017-10-24 22:10:54',NULL,1),(62,'X Michal Kacafírek',3,'2017-10-24 22:10:04','2017-10-24 22:10:04',NULL,1),(63,'X František Branda',3,'2017-10-24 22:09:24','2017-10-24 22:09:24',NULL,1),(64,'Martin Strycek',1,'2017-10-24 22:09:16','2017-10-24 22:09:16',NULL,1),(65,'Tereza Honsová',1,'2017-10-24 22:06:42','2017-10-24 22:06:42',NULL,1),(66,'X Samir Al Sharua',3,'2017-10-24 22:04:26','2017-10-24 22:04:26',NULL,1),(67,'Marek Brandejský',1,'2017-10-24 22:03:17','2017-10-24 22:03:17',NULL,1),(68,'X Elizabeth Pallot',3,'2017-10-24 22:02:58','2017-10-24 22:02:58',NULL,1),(69,'X Mariya Ivanyková',3,'2017-10-24 22:02:56','2017-10-24 22:02:56',NULL,1),(70,'X Mariya Ivanyková',3,'2017-10-24 22:02:55','2017-10-24 22:02:55',NULL,1),(71,'X Elizabeth Pallot',3,'2017-10-24 22:02:50','2017-10-24 22:02:50',NULL,1),(72,'X Mariya Ivanyková',3,'2017-10-24 22:02:49','2017-10-24 22:02:49',NULL,1),(73,'X Mariya Ivanyková',3,'2017-10-24 22:02:49','2017-10-24 22:02:49',NULL,1),(74,'X Elizabeth Pallot',3,'2017-10-24 22:02:47','2017-10-24 22:02:47',NULL,1),(75,'X Mariya Ivanyková',3,'2017-10-24 22:02:29','2017-10-24 22:02:29',NULL,1),(76,'X Radek Valla',3,'2017-10-24 22:01:06','2017-10-24 22:01:06',NULL,1),(77,'Juraj Kováčaj',1,'2012-06-05 12:00:00','2012-07-04 22:00:11',NULL,1),(78,'Marek  Plášek',1,'2017-10-24 21:56:22','2017-10-25 08:42:10','2017-10-25 08:42:10',1),(79,'X Daniel Nečesal',3,'2017-10-24 21:55:26','2017-10-25 06:30:03','2017-10-25 06:30:03',1),(80,'Jakub Manderla',1,'2017-10-24 21:53:45','2017-10-25 06:29:57','2017-10-25 06:29:57',1),(81,'Klára  Šnajdrová',1,'2017-10-24 21:52:33','2017-10-25 04:54:48','2017-10-25 04:54:48',1),(82,'aa',2,'2017-11-01 00:00:00','2017-11-01 00:00:00',NULL,0),(83,'bb',2,'2017-11-01 09:23:04','2017-11-01 09:23:04',NULL,0),(84,'a',2,'2017-11-01 09:25:00','2017-11-01 09:25:00',NULL,0),(85,'gr',2,'2017-11-01 09:30:29','2017-11-01 09:30:29',NULL,0),(86,'a',2,'2017-11-01 09:31:08','2017-11-01 09:31:08',NULL,0),(87,'aa',2,'2017-11-01 09:31:34','2017-11-01 09:31:34',NULL,0),(88,'a',3,'2017-11-01 09:32:25','2017-11-01 09:32:25',NULL,0),(89,'aa',2,'2017-11-01 09:32:58','2017-11-01 09:32:58',NULL,0),(90,'bgf',2,'2017-11-01 09:33:52','2017-11-01 09:33:52',NULL,0),(91,'aa',2,'2017-11-01 09:34:24','2017-11-01 09:34:24',NULL,0),(92,'aaaa',3,'2017-11-01 09:36:18','2017-11-01 09:36:18',NULL,0),(93,'ggg',2,'2017-11-01 09:37:15','2017-11-01 09:37:15',NULL,0),(94,'ccc',2,'2017-11-01 09:38:36','2017-11-01 09:38:36',NULL,1),(95,'aa',3,'2017-09-02 12:40:48','2017-09-02 12:40:48',NULL,1),(96,'vfdoi',2,'2017-08-06 14:50:05','2017-08-06 14:50:05',NULL,1),(97,'cfda',2,'2017-09-02 15:22:39','2017-09-02 15:22:39',NULL,1),(98,'ttřča',3,'2017-03-03 22:26:47','2017-03-03 22:26:47',NULL,1),(99,'aaa',1,'2017-11-06 13:53:40','2017-11-06 13:53:40',NULL,1);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  KEY `title` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (0,'1'),(0,'2');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts2`
--

DROP TABLE IF EXISTS `posts2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8_czech_ci DEFAULT NULL,
  `title2` varchar(100) COLLATE utf8_czech_ci DEFAULT NULL,
  `title5` text COLLATE utf8_czech_ci,
  `title4` int(11) unsigned DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts2`
--

LOCK TABLES `posts2` WRITE;
/*!40000 ALTER TABLE `posts2` DISABLE KEYS */;
INSERT INTO `posts2` VALUES (1,'name','name2',NULL,1);
/*!40000 ALTER TABLE `posts2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts3`
--

DROP TABLE IF EXISTS `posts3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8_czech_ci DEFAULT NULL,
  `title2` varchar(100) COLLATE utf8_czech_ci DEFAULT NULL,
  `title5` text COLLATE utf8_czech_ci,
  `title4` int(11) unsigned DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts3`
--

LOCK TABLES `posts3` WRITE;
/*!40000 ALTER TABLE `posts3` DISABLE KEYS */;
/*!40000 ALTER TABLE `posts3` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `username` varchar(100) COLLATE utf8_czech_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `trash` datetime DEFAULT NULL,
  `user_group_id` int(5) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (3,'test','test','2017-10-17 08:38:09','2017-10-17 08:38:09','$2y$10$xikXtSDdE1rn8qLGw2UKOeoNEZYBh.gr3XRsLsNLKcTEqlOh..a.e',NULL,1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-08  9:28:57

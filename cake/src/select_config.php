<?php
$select_config = [
    'close_order_list'=>[1=>'Nedokončeno',0=>'Dokončeno'],
    'yes_no'=>[1=>'Ano',0=>'Ne'],
    'no_yes'=>[0=>'Ne',1=>'Ano'],
    'storno_type_list'=>[1=>'Tvrdé',2=>'Měkké'],
    'source_list'=>[1=>'Pokladna',2=>'Web',3=>'DJ',4=>'mPizza'],
    'payment_list'=>[
        2=>'Terminál',
        1=>'Hotově',
    ],
    'payment_list_save'=>[
        2=>'pay_card',
        1=>'pay_casch',
    ],
    'stock_type_list'=>[
        1=>'Příjem',
        2=>'Převodka minus',
        11=>'Převodka plus',
        12=>'Storno obj.',
        3=>'Odpis',
        4=>'Prodej',
        5=>'Zrcadlo plus',
        6=>'Zrcadlo minus',
        9=>'Zrcadlo plus - oprava',
        10=>'Zrcadlo minus - oprava',
        7=>'Příjem Makro',
        8=>'Příjem BidFood',
    ],
    'stock_unit_list' => [
        1=>'ks',
        2=>'l',
        3=>'g',
        4=>'kg',
    ],
    'tables_groups'=>[
        0=>['name'=>'Restaurace','id'=>1],
		1=>['name'=>'S sebou','id'=>2],
        2=>['name'=>'Zahrádka','id'=>3],
        3 => ['name' => 'Rozvoz', 'id' => 4, 'delivery' => 1]
    ],
    'tables_groups_list'=>[
        1=>'Restaurace',
		2=>'S sebou',
        3=>'Zahrádka',
        4 => 'Rozvoz'
    ],
    'tables_types'=>[
        1=>'Normální',
        2=>'Zaměstnanec',
        3=>'Firma',
    ],
    'user_group_list'=>[
        1=>'Administrátor',
        2=>'Obsluha',
    ],
    'price_tax_list' => [
		1=>'15%',
		2=>'21%',
		3=>'0%',
		4=>'10%',
    ],
    'price_tax_list_con'=>[
		1=>0.1304, // 15
		2=>0.1736, // 21
		3=>0, // 0
		4=>0.0909, // 10
    ],
    'price_tax_list_rates'=>[
		1=>1.15, // 15
		2=>1.21, // 21
		3=>0, // 0
		4=>1.1, // 10
    ],
    'group_trzba_list' => [
        1=>'Cukrárna Vlastní výrobky',
        2=>'Zboží',
        3=>'Bistro Vlastní výrobky',
    ],
	
    'terminal_list'=>[
        1=>'Terminál 1',
        2=>'Terminál 2',
        3=>'Terminál 3',
        4=>'Terminál 4',
        5=>'Terminál 5',
        
    ],
];
?>

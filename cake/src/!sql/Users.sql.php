<?php

$sql = array (
  'users' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'name' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '50',
      ),
      'username' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '100',
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'modified' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'password' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '255',
      ),
      'trash' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'user_group_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
        'default' => '1',
      ),
      'status' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
        'default' => '1',
      ),
      'nodelete' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
      ),
      'code' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
    ),
    'indexs' => 
    array (
      'name' => 
      array (
        'col' => 
        array (
          0 => 'name',
        ),
      ),
      'user_group_id' => 
      array (
        'col' => 
        array (
          0 => 'user_group_id',
        ),
      ),
      'trash' => 
      array (
        'col' => 
        array (
          0 => 'trash',
        ),
      ),
      'status' => 
      array (
        'col' => 
        array (
          0 => 'status',
        ),
      ),
    ),
  ),
)

?>
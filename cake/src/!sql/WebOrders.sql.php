<?php

$sql = array (
  'web_orders' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '10',
      ),
      'web_id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '10',
      ),
      'web_client_id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '10',
      ),
      'delivery' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '1',
      ),
      'price' => 
      array (
        'type' => 'decimal',
        'null' => false,
        'length' => '10,2',
      ),
      'price_with_tax' => 
      array (
        'type' => 'decimal',
        'null' => false,
        'length' => '10,2',
      ),
      'doprava_price' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'doprava_price_tax' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'platba_price' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'platba_price_tax' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'doprava_zdarma' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '1',
      ),
      'shop_doprava_id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '10',
      ),
      'shop_platba_id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '10',
      ),
      'poznamka' => 
      array (
        'type' => 'text',
        'null' => true,
      ),
      'poznamka_interni' => 
      array (
        'type' => 'text',
        'null' => true,
      ),
      'client_name' => 
      array (
        'type' => 'varchar',
        'null' => false,
        'length' => '120',
      ),
      'client_address' => 
      array (
        'type' => 'varchar',
        'null' => false,
        'length' => '120',
      ),
      'client_email' => 
      array (
        'type' => 'varchar',
        'null' => false,
        'length' => '120',
      ),
      'client_phone' => 
      array (
        'type' => 'varchar',
        'null' => false,
        'length' => '30',
      ),
      'order_date' => 
      array (
        'type' => 'date',
        'null' => true,
      ),
      'pickup_time' => 
      array (
        'type' => 'time',
        'null' => true,
      ),
      'gifted_name' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '40',
      ),
      'gifted_phone' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '30',
      ),
      'web_created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'modified' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'trash' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
    ),
    'indexs' => 
    array (
      'web_id' => 
      array (
        'col' => 
        array (
          0 => 'web_id',
        ),
      ),
      'web_client_id' => 
      array (
        'col' => 
        array (
          0 => 'web_client_id',
        ),
      ),
      'shop_doprava_id' => 
      array (
        'col' => 
        array (
          0 => 'shop_doprava_id',
        ),
      ),
      'shop_platba_id' => 
      array (
        'col' => 
        array (
          0 => 'shop_platba_id',
        ),
      ),
      'delivery' => 
      array (
        'col' => 
        array (
          0 => 'delivery',
        ),
      ),
    ),
  ),
)

?>
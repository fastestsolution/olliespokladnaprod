<?php

$sql = array (
  'shifts' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '10',
      ),
      'system_id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '10',
      ),
      'user_id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '10',
      ),
      'start' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'end' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'cash_limit' => 
      array (
        'type' => 'decimal',
        'null' => false,
        'length' => '10,0',
      ),
      'safe_limit' => 
      array (
        'type' => 'decimal',
        'null' => false,
        'length' => '10,0',
      ),
      'grinder_count' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '10',
      ),
      'paid_cash' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,0',
      ),
      'recieved_vouchers' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,0',
      ),
      'company_bills' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,0',
      ),
      'cash_deadline' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => false,
      ),
    ),
    'indexs' => 
    array (
      'system_id' => 
      array (
        'col' => 
        array (
          0 => 'system_id',
        ),
      ),
      'user_id' => 
      array (
        'col' => 
        array (
          0 => 'user_id',
        ),
      ),
      'start' => 
      array (
        'col' => 
        array (
          0 => 'start',
        ),
      ),
      'end' => 
      array (
        'col' => 
        array (
          0 => 'end',
        ),
      ),
    ),
  ),
)

?>
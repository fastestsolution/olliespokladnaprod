<?php

$sql = array (
  'order_items' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'order_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'name' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '50',
      ),
      'amount' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '11',
      ),
      'code' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '11',
      ),
      'num' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '11',
      ),
      'price' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'count' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'product_group_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'web_group_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'modified' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'createdTime' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '10',
      ),
      'trash' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'product_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'system_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'price_default' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'free' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
      ),
      'price_addon' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'addon' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'storno' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '3',
      ),
      'storno_user_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'storno_type' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'soft_storno_count' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'price_without_tax' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'price_total' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'price_total_without_tax' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'addonAdd' => 
      array (
        'type' => 'text',
        'null' => true,
      ),
      'tax_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '2',
      ),
      'user_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
    ),
    'indexs' => 
    array (
      'free' => 
      array (
        'col' => 
        array (
          0 => 'free',
        ),
      ),
      'order_id' => 
      array (
        'col' => 
        array (
          0 => 'order_id',
        ),
      ),
      'product_group_id' => 
      array (
        'col' => 
        array (
          0 => 'product_group_id',
        ),
      ),
      'product_id' => 
      array (
        'col' => 
        array (
          0 => 'product_id',
        ),
      ),
      'system_id' => 
      array (
        'col' => 
        array (
          0 => 'system_id',
        ),
      ),
      'trash' => 
      array (
        'col' => 
        array (
          0 => 'trash',
        ),
      ),
      'web_group_id' => 
      array (
        'col' => 
        array (
          0 => 'web_group_id',
        ),
      ),
      'soft_storno_count' => 
      array (
        'col' => 
        array (
          0 => 'soft_storno_count',
        ),
      ),
    ),
  ),
)

?>
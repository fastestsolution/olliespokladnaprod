<?php

$sql = array (
  'eet_queues' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '10',
      ),
      'order_id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '10',
      ),
      'bkp' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '100',
      ),
      'fik' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '100',
      ),
      'uid' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '100',
      ),
      'storno' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '1',
      ),
      'price' => 
      array (
        'type' => 'decimal',
        'null' => false,
        'length' => '10,2',
      ),
      'sended' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => false,
        'default' => 'current_timestamp()',
      ),
    ),
    'indexs' => 
    array (
      'order_id' => 
      array (
        'col' => 
        array (
          0 => 'order_id',
        ),
      ),
    ),
  ),
)

?>
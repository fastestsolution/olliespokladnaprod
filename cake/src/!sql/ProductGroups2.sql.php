<?php

$sql = array (
  'product_groups2' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'name' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '100',
      ),
      'web_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'order_num' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'modified' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'status' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
        'default' => '1',
      ),
      'nodelete' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
      ),
      'trash' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'color' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '10',
      ),
    ),
    'indexs' => 
    array (
      'name' => 
      array (
        'col' => 
        array (
          0 => 'name',
        ),
      ),
      'trash' => 
      array (
        'col' => 
        array (
          0 => 'trash',
        ),
      ),
      'status' => 
      array (
        'col' => 
        array (
          0 => 'status',
        ),
      ),
      'web_id' => 
      array (
        'col' => 
        array (
          0 => 'web_id',
        ),
      ),
    ),
  ),
)

?>
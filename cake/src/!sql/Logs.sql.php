<?php

$sql = array (
  'logs' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'system_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'url' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '200',
      ),
      'message' => 
      array (
        'type' => 'text',
        'null' => true,
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
    ),
    'indexs' => 
    array (
    ),
  ),
)

?>
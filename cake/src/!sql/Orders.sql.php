<?php

$sql = array (
  'orders' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'sended' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'name' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '50',
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'modified' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'trash' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'status' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
        'default' => '1',
      ),
      'note' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '100',
      ),
      'close_order' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
      ),
      'total_price' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'payment_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
        'default' => '1',
      ),
      'createdTime' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '10',
      ),
      'storno' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
      ),
      'fik' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '250',
      ),
      'bkp' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '250',
      ),
      'system_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '10',
      ),
      'table_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'table_type_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'table_open' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'table_close' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'pay_casch' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
        'default' => '0.00',
      ),
      'pay_card' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
        'default' => '0.00',
      ),
      'pay_voucher' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
        'default' => '0.00',
      ),
      'pay_discount' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
        'default' => '0.00',
      ),
      'pay_employee' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
        'default' => '0.00',
      ),
      'pay_company' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
        'default' => '0.00',
      ),
      'print_user_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '10',
      ),
      'delivery' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '1',
      ),
      'storno_user_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'storno_reason' => 
      array (
        'type' => 'text',
        'null' => true,
      ),
      'storno_sended' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
    ),
    'indexs' => 
    array (
      'payment_id' => 
      array (
        'col' => 
        array (
          0 => 'payment_id',
        ),
      ),
      'name' => 
      array (
        'col' => 
        array (
          0 => 'name',
        ),
      ),
      'status' => 
      array (
        'col' => 
        array (
          0 => 'status',
        ),
      ),
      'trash' => 
      array (
        'col' => 
        array (
          0 => 'trash',
        ),
      ),
      'close_order' => 
      array (
        'col' => 
        array (
          0 => 'close_order',
        ),
      ),
      'print_user_id' => 
      array (
        'col' => 
        array (
          0 => 'print_user_id',
        ),
      ),
      'storno' => 
      array (
        'col' => 
        array (
          0 => 'storno',
        ),
      ),
      'created' => 
      array (
        'col' => 
        array (
          0 => 'created',
        ),
      ),
      'createdTime' => 
      array (
        'col' => 
        array (
          0 => 'createdTime',
        ),
      ),
      'system_id' => 
      array (
        'col' => 
        array (
          0 => 'system_id',
        ),
      ),
      'table_id' => 
      array (
        'col' => 
        array (
          0 => 'table_id',
        ),
      ),
      'table_type_id' => 
      array (
        'col' => 
        array (
          0 => 'table_type_id',
        ),
      ),
      'sended' => 
      array (
        'col' => 
        array (
          0 => 'sended',
        ),
      ),
    ),
  ),
)

?>
<?php

$sql = array (
  'product_conn_addons' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '10',
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'modified' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'product_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'product_addon_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
    ),
    'indexs' => 
    array (
      'product_id' => 
      array (
        'col' => 
        array (
          0 => 'product_id',
        ),
      ),
      'product_addon_id' => 
      array (
        'col' => 
        array (
          0 => 'product_addon_id',
        ),
      ),
    ),
  ),
)

?>
<?php

$sql = array (
  'stock_queues' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'confirm' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
      ),
      'system_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'system_id_from' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'modified' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'trash' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'data' => 
      array (
        'type' => 'text',
        'null' => true,
      ),
      'system_name_from' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '50',
      ),
    ),
    'indexs' => 
    array (
      'trash' => 
      array (
        'col' => 
        array (
          0 => 'trash',
        ),
      ),
    ),
  ),
)

?>
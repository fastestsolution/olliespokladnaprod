<?php

$sql = array (
  'settings' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'name' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '50',
      ),
      'data' => 
      array (
        'type' => 'text',
        'null' => true,
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'modified' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'status' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
        'default' => '1',
      ),
      'nodelete' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
      ),
      'trash' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'logo' => 
      array (
        'type' => 'text',
        'null' => true,
      ),
      'printer' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '200',
      ),
      'operations' => 
      array (
        'type' => 'text',
        'null' => true,
      ),
    ),
    'indexs' => 
    array (
    ),
  ),
)

?>
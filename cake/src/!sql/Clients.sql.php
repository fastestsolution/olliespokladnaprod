<?php

$sql = array (
  'clients' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'name' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '100',
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'modified' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'status' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
        'default' => '1',
      ),
      'trash' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'first_name' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '48',
      ),
      'last_name' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '48',
      ),
      'email' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '50',
      ),
      'phone' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '9',
      ),
      'operating' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
      ),
      'problem' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
      ),
      'problem_message' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '20',
      ),
      'phone_pref' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '6',
      ),
    ),
    'indexs' => 
    array (
      'name' => 
      array (
        'col' => 
        array (
          0 => 'name',
        ),
      ),
      'phone_pref' => 
      array (
        'col' => 
        array (
          0 => 'phone_pref',
        ),
      ),
      'phone' => 
      array (
        'col' => 
        array (
          0 => 'phone',
        ),
      ),
      'problem' => 
      array (
        'col' => 
        array (
          0 => 'problem',
        ),
      ),
      'operating' => 
      array (
        'col' => 
        array (
          0 => 'operating',
        ),
      ),
      'email' => 
      array (
        'col' => 
        array (
          0 => 'email',
        ),
      ),
      'status' => 
      array (
        'col' => 
        array (
          0 => 'status',
        ),
      ),
      'trash' => 
      array (
        'col' => 
        array (
          0 => 'trash',
        ),
      ),
    ),
  ),
)

?>
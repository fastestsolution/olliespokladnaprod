<?php

$sql = array (
  'deadlines' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'order_id_from' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'order_id_to' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'type_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '2',
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'modified' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'status' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
        'default' => '1',
      ),
      'nodelete' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
      ),
      'trash' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'createdTime' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '10',
      ),
      'delivery_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'system_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '10',
      ),
      'sended' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
      ),
      'date_open' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'date_close' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'pay_cash' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
        'default' => '0.00',
      ),
      'total_price' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
        'default' => '0.00',
      ),
      'pay_card' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
        'default' => '0.00',
      ),
      'pay_bonus' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
        'default' => '0.00',
      ),
      'pay_discount' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
        'default' => '0.00',
      ),
      'pay_employee' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
        'default' => '0.00',
      ),
      'sum_storno' => 
      array (
        'type' => 'decimal',
        'null' => false,
        'length' => '10,2',
        'default' => '0.00',
      ),
      'pay_company' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
        'default' => '0.00',
      ),
      'user_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'pay_voucher' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
        'default' => '0.00',
      ),
    ),
    'indexs' => 
    array (
      'order_id_from' => 
      array (
        'col' => 
        array (
          0 => 'order_id_from',
        ),
      ),
      'order_id_to' => 
      array (
        'col' => 
        array (
          0 => 'order_id_to',
        ),
      ),
      'type_id' => 
      array (
        'col' => 
        array (
          0 => 'type_id',
        ),
      ),
      'trash' => 
      array (
        'col' => 
        array (
          0 => 'trash',
        ),
      ),
      'status' => 
      array (
        'col' => 
        array (
          0 => 'status',
        ),
      ),
      'created' => 
      array (
        'col' => 
        array (
          0 => 'created',
        ),
      ),
      'createdTime' => 
      array (
        'col' => 
        array (
          0 => 'createdTime',
        ),
      ),
      'delivery_id' => 
      array (
        'col' => 
        array (
          0 => 'delivery_id',
        ),
      ),
      'sended' => 
      array (
        'col' => 
        array (
          0 => 'sended',
        ),
      ),
    ),
  ),
)

?>
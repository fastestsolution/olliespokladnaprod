<?php

$sql = array (
  'stocks_items' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'stock_global_item_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'product_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'name' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '200',
      ),
      'jednotka_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'jednotka_id_dodavatel' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '5',
      ),
      'group_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'min' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'max' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'dodavatel_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'modified' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'kos' => 
      array (
        'type' => 'tinyint',
        'null' => true,
        'length' => '1',
      ),
      'tax_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'ean' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '100',
      ),
      'bidfood_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'jednotka_prevod' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'trash' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
    ),
    'indexs' => 
    array (
      'stock_global_item_id' => 
      array (
        'col' => 
        array (
          0 => 'stock_global_item_id',
        ),
      ),
      'group_id' => 
      array (
        'col' => 
        array (
          0 => 'group_id',
        ),
      ),
      'dodavatel_id' => 
      array (
        'col' => 
        array (
          0 => 'dodavatel_id',
        ),
      ),
      'ean' => 
      array (
        'col' => 
        array (
          0 => 'ean',
        ),
      ),
      'name' => 
      array (
        'col' => 
        array (
          0 => 'name',
        ),
      ),
    ),
  ),
)

?>
<?php

$sql = array (
  'product_addons' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '10',
      ),
      'name' => 
      array (
        'type' => 'varchar',
        'null' => false,
        'length' => '50',
      ),
      'created' => 
      array (
        'type' => 'timestamp',
        'null' => false,
        'default' => 'current_timestamp()',
      ),
      'modified' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'trash' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'system_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'group_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'price' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
    ),
    'indexs' => 
    array (
      'system_id' => 
      array (
        'col' => 
        array (
          0 => 'system_id',
        ),
      ),
      'trash' => 
      array (
        'col' => 
        array (
          0 => 'trash',
        ),
      ),
    ),
  ),
)

?>
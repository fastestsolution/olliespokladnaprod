<?php

$sql = array (
  'permissions' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '10',
      ),
      'user_group_id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '10',
      ),
      'module' => 
      array (
        'type' => 'varchar',
        'null' => false,
        'length' => '50',
      ),
      'action' => 
      array (
        'type' => 'varchar',
        'null' => false,
        'length' => '50',
      ),
      'permission' => 
      array (
        'type' => 'tinyint',
        'null' => false,
        'length' => '4',
      ),
    ),
    'indexs' => 
    array (
      'user_group_id' => 
      array (
        'col' => 
        array (
          0 => 'user_group_id',
        ),
      ),
      'module' => 
      array (
        'col' => 
        array (
          0 => 'module',
        ),
      ),
      'action' => 
      array (
        'col' => 
        array (
          0 => 'action',
        ),
      ),
    ),
  ),
)

?>
<?php

$sql = array (
  'product_recipes' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '10',
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'modified' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'product_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'stock_item_global_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'value' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,4',
      ),
      'loss' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'unit_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '5',
      ),
      'trash' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
    ),
    'indexs' => 
    array (
      'product_id' => 
      array (
        'col' => 
        array (
          0 => 'product_id',
        ),
      ),
      'stock_item_global_id' => 
      array (
        'col' => 
        array (
          0 => 'stock_item_global_id',
        ),
      ),
      'trash' => 
      array (
        'col' => 
        array (
          0 => 'trash',
        ),
      ),
    ),
  ),
)

?>
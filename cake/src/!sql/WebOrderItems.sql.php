<?php

$sql = array (
  'web_order_items' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'web_id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'web_order_id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'shop_order_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'ean' => 
      array (
        'type' => 'varchar',
        'null' => false,
        'length' => '100',
      ),
      'name' => 
      array (
        'type' => 'varchar',
        'null' => false,
        'length' => '150',
      ),
      'nd' => 
      array (
        'type' => 'tinyint',
        'null' => false,
        'length' => '1',
      ),
      'price' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'price_with_tax' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'row_price' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'row_price_with_tax' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,2',
      ),
      'ks' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'shop_product_id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'dph' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'shop_dodavatel_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'updated' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
    ),
    'indexs' => 
    array (
      'name' => 
      array (
        'col' => 
        array (
          0 => 'name',
        ),
      ),
      'web_order_id' => 
      array (
        'col' => 
        array (
          0 => 'web_order_id',
        ),
      ),
      'web_id' => 
      array (
        'col' => 
        array (
          0 => 'web_id',
        ),
      ),
      'shop_order_id' => 
      array (
        'col' => 
        array (
          0 => 'shop_order_id',
        ),
      ),
    ),
  ),
)

?>
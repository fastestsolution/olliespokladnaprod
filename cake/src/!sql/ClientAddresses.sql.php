<?php

$sql = array (
  'client_addresses' => 
  array (
    'cols' => 
    array (
      'id' => 
      array (
        'type' => 'int',
        'null' => false,
        'length' => '11',
      ),
      'client_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'type_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'street' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '40',
      ),
      'city' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '50',
      ),
      'zip' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '10',
      ),
      'area_id' => 
      array (
        'type' => 'int',
        'null' => true,
        'length' => '11',
      ),
      'created' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'modified' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'trash' => 
      array (
        'type' => 'datetime',
        'null' => true,
      ),
      'lat' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,7',
      ),
      'lng' => 
      array (
        'type' => 'decimal',
        'null' => true,
        'length' => '10,7',
      ),
      'company' => 
      array (
        'type' => 'varchar',
        'null' => true,
        'length' => '30',
      ),
    ),
    'indexs' => 
    array (
      'client_id' => 
      array (
        'col' => 
        array (
          0 => 'client_id',
        ),
      ),
      'type_id' => 
      array (
        'col' => 
        array (
          0 => 'type_id',
        ),
      ),
      'area_id' => 
      array (
        'col' => 
        array (
          0 => 'area_id',
        ),
      ),
      'trash' => 
      array (
        'col' => 
        array (
          0 => 'trash',
        ),
      ),
    ),
  ),
)

?>
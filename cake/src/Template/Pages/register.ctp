<fieldset>
    <?= $this->Form->create(); ?>
        <?= $this->Form->input('username', ['label'=>'Uživatel']); ?>
        <?= $this->Form->input('password', ['label'=>'Heslo', 'type'=>'password']); ?>
        <?= $this->Form->submit('Registrovat', []); ?>
    <?= $this->Form->end(); ?>
</fieldset>
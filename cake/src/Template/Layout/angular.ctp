<!DOCTYPE html>
<html class="full-height" >
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title><?php echo $appName.' '.$version;?></title>
    <base href="http://<?= $_SERVER["HTTP_HOST"]; ?>/" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge; IE=10">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<link rel="icon" href="favicon.ico?v=2" />
    
    <?php //= $this->element('Layout/Angular/styles');?>
   
    <?= $this->element('Layout/LTE/head'); ?>
    <?php // $this->Html->css('default.css', ['type'=>"text/css", 'rel'=>'stylesheet/less', 'media'=>"screen"]) ?>
    <?= $this->Html->script('less.js') ?>
</head>

<body class="">
    <div class="wrapper">
           
		   <?php //= $this->element('Layout/LTE/header'); ?>
           
          <?php //pr($appPath); ?>
          <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
			<?= $this->fetch('content') ?>
            </div>
          
    </div>
    
    <?php //= $this->element('Layout/Angular/scripts');?>
</body>
</html>

<style>
body {
	background:#D7EEF7;
	font-size:15px;
}
#obal {
	background:#ffffff;
	width:80%;
	margin:auto;
	display:block;
	position:relative;
	padding:10px;
}
.text_right {
	text-align:right;
}
table {
	width:100%;
}
table tr th {font-size:15px;text-align:left;border-bottom:1px solid #ccc;}
table tr td {font-size:15px;text-align:left;border-bottom:1px solid #ccc;}
</style>
<div id="obal">
<?= $data ?>
</div>
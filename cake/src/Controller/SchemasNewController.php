<?php
/**
 * controller pro vytvareni databaze ze souboru
 * Jakub Tysoň
 * vytvoreni souboru /sql/createSql/
 * obnoveni souboru /sql/appendSql/
 * obnoveni souboru /sql/appendSql/?run - povoleno append mimo localhost
 */
namespace App\Controller;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;
use Cake\Datasource\TableSchemaInterface;
use Cake\Database\Schema\Table;
use Cake\Datasource\ConnectionManager;
use Cake\Utility\Inflector;

class SchemasController extends AppController{
    public $outResult = [];
    public $messages = [];
    public $folderSql = '../src/!sql/';

    /**
     * kontrola zda je ip kancelar
     */
    private function inOffice(){
        if (!in_array($_SERVER['REMOTE_ADDR'],[
            '89.103.18.65',
            '94.242.105.214',
            '81.30.228.160',
            '94.242.105.214',
            '46.135.100.112',
            '::1']
        )){
            die('Povoleno je v kancelar');
        }
    }

    /**
     * vytvoreni tabulky mysql
     */
    private function createTable($table){
       
        try{
            $query = 'CREATE TABLE `'.$table.'` ( `id` INT NOT NULL AUTO_INCREMENT , PRIMARY KEY (`id`)) ENGINE = MyISAM;';
            $result = $this->db->execute($query);
        }catch(\Exception $e){
            if(!isset($this->outResult['errors'])){
                $this->outResult['errors'] = [];
            }
            $this->outResult['errors']['tableCreate_'. $table] = $e->getMessage();
            $result = false;
        }
        return $result;
    }

    /**
     * nastaveni autoincrement v tabulce
     */
    public function setAutoincrement($value = 40000){
        $this->db = ConnectionManager::get('default');
        try{
            $query = 'ALTER TABLE orders AUTO_INCREMENT='.$value;
            $this->db->execute($query);
        }catch(\Exception $e){
            die(json_encode(['result'=>false,'message'=> $e->getMessage()]));
        }
        die(json_encode(['result'=>true,'message'=>'Nastaveno na '.$value]));
    }


    /**
     * pridani sloupce do databaze
     */
    private function addColumn($table,$col_name,$col_params){
        try{
            
            if ($col_params['type'] != 'varchar'){
                // $query = 'ALTER TABLE `'.$table.'` ADD IF NOT EXISTS `'.$col_name.'` '.$col_params['type'].(isset($col_params['length'])?'('.$col_params['length'].') '.(isset($col_params['disable_UNSIGNED'])?'':'UNSIGNED').' ':'').' '.(isset($col_params['null'])?'NULL':'').(isset($col_params['default'])?' DEFAULT '.$col_params['default']:'').';';
                $query = 'ALTER TABLE `'.$table.'` ADD  `'.$col_name.'` '.$col_params['type'].(isset($col_params['length'])?'('.$col_params['length'].') '.(isset($col_params['disable_UNSIGNED'])?'':'UNSIGNED').' ':'').' '.(isset($col_params['null'])?'NULL':'').(isset($col_params['default'])?' DEFAULT '.$col_params['default']:'').';';
            } else {
                // $query = 'ALTER TABLE `'.$table.'` ADD IF NOT EXISTS `'.$col_name.'` '.$col_params['type'].(isset($col_params['length'])?'('.$col_params['length'].')':'').' CHARACTER SET utf8 COLLATE utf8_czech_ci '.(isset($col_params['null'])?'NULL':'').(isset($col_params['default'])?' DEFAULT '.$col_params['default']:'').';';
                $query = 'ALTER TABLE `'.$table.'` ADD  `'.$col_name.'` '.$col_params['type'].(isset($col_params['length'])?'('.$col_params['length'].')':'').' CHARACTER SET utf8 COLLATE utf8_czech_ci '.(isset($col_params['null'])?'NULL':'').(isset($col_params['default'])?' DEFAULT '.$col_params['default']:'').';';
            }
            $result = $this->db->execute($query);
        
        } catch(\Exception $e){
                if(!isset($this->outResult['errors'])){
                    $this->outResult['errors'] = [];
                }
                $this->outResult['errors']['columnAdd_'. $table] = $e->getMessage();
                $result = false;
        }
       
        return $result;
    }

    /**
     * odebrani sloupce z tabulky
     */
    private function removeColumn($table,$col_name){
        try{

            $query = 'ALTER TABLE `'.$table.'` DROP COLUMN `'.$col_name.'`;';
            $result = $this->db->execute($query);
        }catch(\Exception $e){
                if(!isset($this->outResult['errors'])){
                    $this->outResult['errors'] = [];
                }
                $this->outResult['errors']['columnDrop_'. $table] = $e->getMessage();
                $result = false;
        }
        return $result;
    }
    

    /**
     * pridani index do tabulky
     */
    private function addIndex($table,$indexName,$indexData){
        //$query .= 'ALTER TABLE `'.$table.'` ADD INDEX(`'.implode($indexData['col'],',').'`);';
        $query = 'SHOW INDEX FROM '.$table.';';
        $result = $this->db->execute($query)->fetchAll('assoc');
        $index_list = [];
        if ($result){
            foreach($result AS $r){
                $index_list[] = $r['Column_name'];
            }
        }
        //pr($index_list);
        if ($index_list){
            foreach($index_list AS $ind){
                if ($ind != 'id' && $ind == $indexName){
                    try{
                        if ($this->indexList($table,$indexName)){
                            $query = 'DROP INDEX '.$ind.' ON '.$table.';';
                            $this->db->execute($query);
                        
                        }
                    
                    }catch(\Exception $e){
                        if(!isset($this->outResult['errors'])){
                            $this->outResult['errors'] = [];
                        }
                        $this->outResult['errors']['indexDrop_'. $table] = $e->getMessage();
                    }
                }
                
            }
        }
        try{
            $query = 'CREATE INDEX '.$indexName.' ON '.$table.' ('.implode($indexData['col'],',').'); ';
            $this->db->execute($query);
        }catch(\Exception $e){
            if(!isset($this->outResult['errors'])){
                $this->outResult['errors'] = [];
            }
            $this->outResult['errors']['indexAdd_'. $table] = $e->getMessage();
            $result = false;
        }
       // pr($query);
    }

    /**
     * seznam sloupcu v tabulce
     */
    private function colList($table){
        $query = 'SHOW COLUMNS FROM '.$table.';';
        $result = $this->db->execute($query)->fetchAll('assoc');
        $list = [];
        foreach($result AS $r){
            $list[] = $r['Field'];
        }
        //pr($list);
        return $list;
    }

    /**
     * seznam index v tabulce
     */
    private function indexList($table,$index){
        $query = "SHOW INDEX FROM ".$table." WHERE KEY_NAME = '".$index."';";
        $result = $this->db->execute($query)->fetchAll('assoc');
        if ($result){
            return true;
        } else {
            return false;
        }
        // pr($result);die('aa');
        return $list;
    }

    private function createLog($table,$colName){
        if (!isset($this->createColsResult[$table])){
            $this->createColsResult[$table] = [];
        }
        $this->createColsResult[$table][] = $colName;
    }

    /**
     * vytvoreni sloupce v tabulce
     */
    private function createCols($table, $sql){
        $cols = $this->colList($table);
// pr($cols);die();
        $this->createColsResult = [];
        $this->removeColsResult = [];
        if (isset($sql['cols'])){
            $currentCol = [];
            $importCol = [];
            foreach($sql['cols'] AS $colName=>$colData){
                $currentCol[] = $colName;
                // pr($colName);
                if (!in_array($colName,$cols)){
                    $this->createLog($table,$colName);
                    // pr($cols);die();
                    $this->addColumn($table,$colName,$colData);
                } else {
                    
                }
            }

            $diff = array_diff($cols,$currentCol);
            if (count($diff)>0){
                foreach($diff AS $d){
                    $this->removeColsResult[$table][] = $d;
                    $this->removeColumn($table,$d);
                }
            }
            if (!empty($this->createColsResult)){
                $this->logs(true,htmlentities('SQL: vytvoreny sloupce tabulky <b>'.$table.'</b>:  ').count($this->createColsResult[$table]).' '.((empty($this->createColsResult))?0:''),$this->createColsResult,'created');
            
            }
            if (count($diff)>0){
                
                $this->logs(true,htmlentities('SQL: Smazano sloupcu tabulky <b>'.$table.'</b>: '.count($diff)).' ',$this->removeColsResult,'deleted').'';
            
            }
            // pr($diff);
            
        }

        // pridani indexu
        if (isset($sql['indexs'])){
            // SHOW INDEX FROM table_name WHERE KEY_NAME = 'index_name'
            foreach($sql['indexs'] AS $indexName=>$indexData){
                
                 $this->addIndex($table,$indexName,$indexData);
            }
        }

        // pridani vychozi hodnoty
        if (isset($sql['defaultValues']) && isset($this->defaulValues)){
            foreach($sql['defaultValues'] AS $key=>$data){
                $this->addValue($table,$data);
            }
        }
    }

    /**
     * vlozeni vychozi hodnoty do tabulky
     */
    private function addValue($table,$data){
        $keys = array_keys($data);
        $values = array_values($data);
        
        
        $query = "SELECT EXISTS(SELECT 1 FROM ".$table." WHERE id=".$values[0].") AS exist";
        $res = $this->db->execute($query)->fetchAll('assoc');
        
        if ($res[0]['exist'] == 0){
            try{
                $query = "INSERT INTO ".$table." (".implode($keys,',').") VALUES (\"".implode($values,'","')."\");";
                $this->db->execute($query);
            }catch(\Exception $e){
                if(!isset($this->outResult['errors'])){
                    $this->outResult['errors'] = [];
                }
                $this->outResult['errors']['valueInsert_'. $table] = $e->getMessage();
                $result = false;
            }
        }
        
        //pr($data);
        //die();
    }


    /**
     * zjisteni pokud !sql existuje
     */

    private function checkExistFolder(){
        if (!file_exists($this->folderSql)) {
            die('Slozka !SQL neexistuje');
        }
        if ( ! is_writable(dirname($this->folderSql))) {
            die (dirname($this->folderSql) . ' musi mit povoleny zapis!!!');
        }
        
    } 

    /**
     * nahrani z adresare !sql !sql
     * 
     */
    private function loadFolder(){
        $this->checkExistFolder();

        $sql_list = [];
        $files = scandir($this->folderSql);
        //pr(getcwd());
        unset($files[0]);
        unset($files[1]);
        //pr($files);
        foreach($files AS $file){
            require_once($this->folderSql.$file);
            $sql_list = array_merge($sql_list,$sql);
        }
        // pr($sql_list);
        return ($sql_list);
        //die('a');
    }

    /**
     * spusteni scriptu
     * pokud parametr true tak importuje vychozi data
     * 
     */
    public function appendSql($defaulValues=false){
        $this->outResult = [];
        ini_set('max_execution_time', 300);
        set_time_limit ( 300 );

        $this->inOffice();
        if (strpos($_SERVER['HTTP_HOST'],'localhost') !== false){
            if (!isset($this->request->query['run'])){
                die('Mozno spustit mimo localhost');

            }
        }

        $this->startScript();
        

        $this->checkExistFolder();

        if ($defaulValues == true){
            $this->defaulValues = true;
        }

        $this->db = ConnectionManager::get('default');
        $this->collection = $this->db->schemaCollection();
        
        // Get the table names
        $tables = $this->collection->listTables();
        
        $sql_list = $this->loadFolder();
        
        echo $this->htmlRender($sql_list);
        
        
    }

    public function runAppendSql($table){
        ini_set('max_execution_time', 300);
        set_time_limit ( 300 );
        $sql_list = $this->loadFolder();
        $this->db = ConnectionManager::get('default');
        $this->collection = $this->db->schemaCollection();
        
        $tables = $this->collection->listTables();
        if (isset($sql_list[$table])){
            $sql = $sql_list[$table];
            // pr($sql_list[$table]);die();
        // if (isset($sql_list) && count($sql_list)>0){
        //     foreach($sql_list AS $table=>$sql){
                if (in_array($table, $tables)){
                    $this->createCols($table,$sql);
                } else {
                    if($this->createTable($table)){
                        $this->createCols($table,$sql);
                    }
                }
        //     }
        // }
        }

        $time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];

        $this->logs(true,'SQL: Vytvoreno tabulka: '.$table);
        $this->logs(true,"SQL: Vlozeny výchozí data: ".(isset($this->defaulValues)?'ANO':'NE'));

        // $this->outResult['time'] = round($time,2);
        
        //$result = "Process Time: ".round($time,2)."s , Vytvoreno tabulek: ".count($sql_list).", Vlozeny výchozí data: ".(isset($this->defaulValues)?'ANO':'NE');
        
        //pr($result);
        $this->autoRender = false;
        
        /*if(isset($this->outResult['errors'])){
            pr('Chyby:'); 
            pr($this->outResult['errors']);
        
        }*/

        if (isset($this->outResult['errors'])){
            $this->outResult['result'] = false;
        }
        $this->outResult['table'] = $table;

        $this->outResult['message'] = '';
        if (isset($this->messages['created'])) $this->outResult['message'] .= ' '.$this->messages['created'];
        if (isset($this->messages['deleted'])) $this->outResult['message'] .= ' '.$this->messages['deleted'];
       
        if (isset($this->request->query['debug'])){
            pr($this->outResult); 
        
        }        
        // $this->endScript();
        die(json_encode($this->outResult)); 
    } 


    function htmlRender($data){
        echo '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">';
        echo '<script src="http://scripts.fastesthost.cz/js/mootools1.4/core1.6.js"></script>';
        echo '<script src="http://scripts.fastesthost.cz/js/mootools1.4/requestQueue.js"></script>';
        echo '<style>
            #list {list-style-type:none;}
            #list li {vertical-align:middle;border-bottom:1px solid #ccc;margin-bottom:2px;padding:5px;}
            #list li span{margin-right:5px;background:#efefef;vertical-align:middle;display:inline-block;width:20px;height:20px;border:1px solid #efefef;}
            #list li span.preloader{background:orange;}
            #list li span.done{background:green;}
            #list li span.error{background:red;}
            #list li var{display:inline-block;vertical-align:middle;margin-left:10px;font-size:11px;}
            #list li .alert{margin-top:5px;}
            .none {
                display:none;
            }
        </style>';
        echo '<div class="container"><br />';
        echo '<a href="?start" class="btn btn-primary" id="start">Spustit</a>';
        echo '<ul id="list">';
        // pr($data);
        foreach($data AS $table=>$d){
            echo '<li class="table'.$table.'" data-table="'.$table.'"><span></span>'.$table.'<var></var><div class="alert alert-danger none"></div></li>';
        }

        echo '</ul>';
        echo '</div>';
        echo '<script>';
            echo "window.addEvent('domready',function(){
                $('start').addEvent('click',function(e){
                    e.stop();
                    var list = $('list').getElements('li');
                    window.requestQueue = {};
                    list.each(function(item){
                        table = item.get('data-table');
                        // console.log(table);
                        window.requestQueue[table] = new Request.JSON({
                            url:'/sql/runAppendSql/'+table,	
                            onStart: function(){

                            },
                            onError: (function(e){
                                alert('Chyba ulozeni')
                            }),
                            onComplete: (function(json){
                                json = null;
                            }).bind(this)
                        });
                        window.requestQueue[table].setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
                        
                    });
                    var myQueueReceive = new Request.Queue({
                        requests: window.requestQueue,
                        concurrent: Object.getLength(window.requestQueue),
                        onException: function(name){
                            
                        }.bind(this),
                        onLoadstart: function(name){
                            $('list').getElement('.table'+name).getElement('span').addClass('preloader');
                        }.bind(this),
                        onEnd: function(name, instance, text, xml) {
                        }.bind(this),
                        onComplete: function(name, instance, json, xml){
                            if (json && json.result == true){
                                //FstAlert(json.message);
                                $('list').getElement('.table'+json.table).getElement('span').removeClass('error');
                                $('list').getElement('.table'+json.table).getElement('span').removeClass('preloader');
                                $('list').getElement('.table'+json.table).getElement('span').addClass('done');
                                $('list').getElement('.table'+json.table).getElement('var').set('html',json.message);
                                //$('loadCars').click();         
                            } else {
                                $('list').getElement('.table'+json.table).getElement('span').addClass('error');
                                $('list').getElement('.table'+json.table).getElement('.alert').removeClass('none');
                                html = '';
                                Object.each(json.errors,function(value,er){
                                    console.log(er);
                                    console.log(value);
                                    html += '<strong>'+er+'</strong><br />'+value;
                                });
                                console.log(html);
                                $('list').getElement('.table'+json.table).getElement('.alert').set('html',html);
                                //console.log(json);
                                //alert('Chyba ulozeni');
                            }
        
                            // console.log(json)
                            // this.sendReqPredano.cancel();
                      
                        }.bind(this)
                    });
                    list.each(function(item){
                        table = item.get('data-table');
                        window.requestQueue[table].send();
                    }.bind(this));
                });
            });";
        echo '</script>';
        die();
    }

    function logs($result,$value,$data=null,$type=null){
        if (!isset($this->outResult)){
            $this->outResult = [];
        }
        
        if ($data){
            $tables = '';
            foreach($data AS $table=>$col){
                $tables .= " ".implode(',',$data[$table]).'';
            }
            if ($type == 'created'){
                $this->messages['created'] = '<strong>Vytvořeno:</strong> '.$tables;
            }
            if ($type == 'deleted'){
                $this->messages['deleted'] = '<br /><strong>Smazáno:</strong> '.$tables;
            }
               
            $value .= $tables;
        }
        
        $this->outResult['log'][] = date('Y-m-d H:i:s').': '.$value;
        $this->outResult['result'] = $result;
        
    }
     
    /**
     * tracking script time
     */

    private function startScript(){
        $this->time_start = microtime(true); 

    } 

    private function endScript(){
                
        $this->time_end = microtime(true);

        //dividing with 60 will give the execution time in minutes otherwise seconds
        $this->execution_time = ($this->time_end - $this->time_start);
        $this->execution_time = round($this->execution_time,3);

        //execution time of the script
        echo "<b>Total Execution Time:</b> ".$this->execution_time." sec <br/>";
    } 

    

    /**
     * vytvoreni sql strukturu
     */
    public function createSql(){
        
        $this->inOffice();
        echo '<div class="container"><br />';
        echo '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">';
        
        if (!isset($_GET['start'])){
            echo '<a href="?start" class="btn btn-primary" id="start">Spustit</a>';
        } else {

        if (strpos($_SERVER['HTTP_HOST'],'localhost') === false){
            die('Mozno spustit jen v localhost');
        }
        $this->startScript();
        
        $this->checkExistFolder();

        // pr($_SERVER);die();
        
        $conn = ConnectionManager::get('default');
        $dbName = $conn->config()['database']; 
        // pr($conn->config()['database']);die();
        $table_list = [];
        $query = $conn->execute('SHOW TABLES FROM '. $dbName);
        $resultsTables = $query ->fetchAll('assoc');
        $res = [];
       
        // pr($resultsTables);
        foreach($resultsTables AS $table){
            if(substr($table['Tables_in_'. $dbName],0,5) != 'kopie'){
                $table_list[] = $table['Tables_in_'. $dbName];
            }
        }

        // pr($table_list);die();
        // foreach($table_list AS $table){
        //     $model = Inflector::camelize($table);
        //     pr($model);
        // }
        // die('aaa');
        
        foreach($table_list AS $table){
        //die('a');
        
        
        
        // $table = 'products';
        $model = Inflector::camelize($table);
        //pr($model);die();

        $sql = [
            $table=>[
                'cols'=>[],
                'indexs'=>[]
            ]
        ];

        // nacteni indexu
        $query = $conn->execute('SHOW INDEX FROM '.$table);
        $resultsIndex = $query ->fetchAll('assoc');
        // pr($resultsIndex);
        foreach($resultsIndex AS $i){
            if ($i['Column_name'] != 'id')
            $sql[$table]['indexs'][$i['Column_name']] = ['col'=>[$i['Column_name']]]; 
        }
        

        // sloupce tabulky
        $query = $conn->execute('DESCRIBE '.$table);
        $resultsCol = $query ->fetchAll('assoc');
        // pr($resultsCol);
        foreach($resultsCol AS $c){
            $type_tmp = explode(' ',$c['Type']);
            //pr($type);
            $type = explode('(',$type_tmp[0]);
            if (isset($type[1]))
            $type[1] = rtrim($type[1],')');

            //pr($type);

            
            $sql[$table]['cols'][$c['Field']] = [
                'type' => $type[0],
                //'length' => strtr($type[1],['('=>'',')'=>'']),
                'null' => (($c['Null'] == 'YES')?true:false),
            ];

            // delka pole
            if (isset($type[1])){
                $sql[$table]['cols'][$c['Field']]['length'] = strtr($type[1],['('=>'',')'=>'']);
            }

            // default value
            if (!empty($c['Default'])){
                $sql[$table]['cols'][$c['Field']]['default'] = $c['Default'];
            }
                
        }
        // exportovani a ulozeni config souboru
        $v = var_export($sql,true);
        $phpData = "<?php\n\n";
        $phpData .= "\$sql = ".$v;
        $phpData .= "\n\n?>";
        // echo($v);
        $res[] = $model;
        file_put_contents('../src/!sql/'.$model.'.sql.php',$phpData);
        // pr($sql);
        
        }

        echo "<b>Vytvoreny soubory: </b>".count($res);
        echo '<ul>';
        foreach($res AS $r){
            echo '<li>'.$r.'.sql.php</li>';
        }
        echo '</ul>';

        // pr($res);
        $this->endScript();
        
        
        }
        echo '</div>';
        die('');
        
    }
}

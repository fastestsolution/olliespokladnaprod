<?php

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;

use Cake\I18n\Time;

class DeadlinesController extends AppController
{

    /**
     * vytvoreni select listu
     */
    public function getSelectList($check=true){
        if ($check == false || isset($this->request->query['firstLoad'])){
            $this->loadModel('Deliverys');
            $delivery_list = $this->Deliverys->deliveryList();
            return $select_list = [
                'delivery_list'=>$delivery_list,
            ];
        } else {
            return false;
        }  
    }

    /**
     * INDEX orders
     */
    public function index()
    {
        $this->loadComponent('vI');
        // pr($this->request->params['type_id']);die();
        $select_list = $this->getSelectList();
        $this->type_id = $this->request->params['type_id'];

        $conditions = [
            'type_id'=>$this->request->params['type_id'],
        ];
        if ($this->type_id == 1){
            $fields_defined = [
                ['col'=>'id','title'=>'ID','type'=>'text'],
                ['col'=>'date_open','title'=>'Otevření','type'=>'datetime'],
                ['col'=>'date_close','title'=>'Zavření','type'=>'datetime'],
                ['col'=>'created','title'=>'Vytvořeno','type'=>'datetime'],
                ['col'=>'total_price','title'=>'Celkem','type'=>'text'],
                ['col'=>'pay_cash','title'=>'Hotovost','type'=>'text'],
                ['col'=>'pay_card','title'=>'Karta','type'=>'text'],
                ['col'=>'pay_voucher','title'=>'Poukázka','type'=>'text'],
                ['col'=>'pay_discount','title'=>'Sleva','type'=>'text'],
                ['col'=>'pay_employee','title'=>'Zam','type'=>'text'],
                ['col'=>'pay_company','title'=>'Firma','type'=>'text'],
                ['col'=>'sum_storno','title'=>'Storna','type'=>'text'],
                ['col'=>'user_id','title'=>'Uživatel','type'=>'list','list_data'=>$select_list['delivery_list']],
                ['col'=>'order_id_from','title'=>'Objednávky od','type'=>'text'],
                ['col'=>'order_id_to','title'=>'Objednávky do','type'=>'text'],
                //3=>['col'=>'type_id','title'=>'Zdroj','type'=>'list','list_data'=>$select_list['source_list']],
            ];
        }
        if ($this->type_id == 2){
            $fields_defined = [
                ['col'=>'id','title'=>'ID','type'=>'text'],
                ['col'=>'date_open','title'=>'Otevření','type'=>'datetime'],
                ['col'=>'date_close','title'=>'Zavření','type'=>'datetime'],
                ['col'=>'created','title'=>'Vytvořeno','type'=>'datetime'],
                ['col'=>'total_price','title'=>'Celkem','type'=>'text'],
                ['col'=>'pay_cash','title'=>'Hotovost','type'=>'text'],
                ['col'=>'pay_card','title'=>'Karta','type'=>'text'],
                ['col'=>'pay_voucher','title'=>'Poukázka','type'=>'text'],
                ['col'=>'pay_discount','title'=>'Sleva','type'=>'text'],
                ['col'=>'pay_employee','title'=>'Zam','type'=>'text'],
                ['col'=>'pay_company','title'=>'Firma','type'=>'text'],
                ['col'=>'sum_storno','title'=>'Storna','type'=>'text'],
                ['col'=>'user_id','title'=>'Uživatel','type'=>'list','list_data'=>$select_list['delivery_list']],
                ['col'=>'order_id_from','title'=>'Objednávky od','type'=>'text'],
                ['col'=>'order_id_to','title'=>'Objednávky do','type'=>'text'],
                //3=>['col'=>'type_id','title'=>'Zdroj','type'=>'list','list_data'=>$select_list['source_list']],
            ];
        }

        $this->filtration_defined = [
            'id'=>['col'=>'id','title'=>'ID','type'=>'text'],
            //'order_id_from'=>['col'=>'order_id_from','title'=>'Jméno','type'=>'text_like'],
            'created'=>['col'=>'created','title'=>'Vytvořeno','type'=>'date_range'],
            //'modified'=>['col'=>'modified','title'=>'Upraveno','type'=>'date'],
            //'delivery_id'=>['col'=>'delivery_id','title'=>'Rozvozce','type'=>'select','list'=>$this->vI->filtrSelectList($select_list['delivery_list'])],
        ];
        
        
        if (isset($this->request->data['conditions'])){
            $conditions = $this->vI->convertConditions($conditions,$this->request->data['conditions'],$this->filtration_defined);
            //pr($conditions);die();
        }
        
       
        $posibility = [
            ['link'=>'/api/deadlines/close/show/x/','title'=>__('Zobrazit'),'class'=>'fa-search','type'=>'ajax','params'=>'showPrint'],
            //0=>['link'=>'/api/status/Orders/','title'=>__('Status'),'class'=>'fa-check-circle-o','type'=>'ajax','params'=>'status'],
            ['link'=>'/api/deadlines/close/show/x/','title'=>__('Tisk'),'class'=>'fa-print','type'=>'ajax','params'=>'print'],
            ['link'=>'#/orders/','title'=>__('Zobrazit'),'class'=>'fa-edit','type'=>'redirect'],
            //2=>['link'=>'/api/trash/Orders/','title'=>__('Smazat'),'class'=>'fa-trash','type'=>'ajax','params'=>'trash','confirm'=>__('Opravdu smazat?')],
        ];
        
        $top_actions = [
            //0=>['link'=>'./edit/','title'=>__('Nová položka'),'class'=>'fa-edit','type'=>'router','params'=>'edit'],
        ];

        $fields = $this->vI->fieldsConvert($fields_defined);
        //pr($fields);die();
        $mapper = function ($data, $key, $mapReduce) {
            $sum = $this->Orders->getOrdersListSum($data->order_id_from,$data->order_id_to);
            foreach($sum AS $k=>$value){
                if (isset($data->$k)){
                    $data->$k = $value;
                }
            }
            $mapReduce->emit($data);
        };
        $this->loadModel('Orders');
        $query = $this->Deadlines->find()
            //->contain(['Orders'])
            ->select($fields)
            ->where($conditions)
            ->mapReduce($mapper);

        
        $this->loadComponent('Paginator');
        $data_list = $this->paginate($query);
        
        
        
        $pagination = $this->vI->convertPagination();
        //pr($pagination['page']);
        $results = [
            'result'=>true,
            'data'=>$data_list->toArray(),
            'data_count'=>count($data_list->toArray()),
            'pagination'=>$pagination,
            'table_th'=>$fields_defined,
            'filtration'=>array_values($this->filtration_defined),
            'select_list'=>$select_list,
            'posibility'=>$posibility,
            'top_actions'=>$top_actions,
            'conditions'=>(object) $conditions,
        ];
        
        $this->setJsonResponse($results);
    }

    private function checkOpenOrder(){
        $this->loadModel('Orders');
        $count = $this->Orders->checkOpenOrder();
        if ($count > 0){
            die(json_encode(['result'=>false,'message'=>'Nejsou přiřazeny všechny objednávky na rozvozce']));
        }
    }

    
    // uzavreni smeny
    public function close($print_type,$deadline_type,$deadline_id=null,$type_delivery=null,$delivery_id=null){
    
        if ($deadline_id == 'null') $deadline_id = null; 
        //pr($deadline_id);
        
            
            $this->type_id = 1;
            $conditionsDeadline = [
                'type_id'=>$this->type_id,
            ];
    
        if ($deadline_id){
            unset($conditionsDeadline['type_id']);
            $conditionsDeadline['id'] = $deadline_id;
            
        }
        // pr($print_type);
        // pr($conditionsDeadline);die();
        
        
        if (!$deadline_id){
            // nalezeni posledni uzaverky
            $findLast = $this->Deadlines->find()->where($conditionsDeadline)->select(['order_id_from','order_id_to'])->order('id DESC')->first();    
            if (empty($findLast)){
                $lastIdFrom = 0;
            } else {
                $lastIdFrom = $findLast->order_id_to;
            } 
            
        } else {
            
            $findLast = $this->Deadlines->find()->where($conditionsDeadline)->select(['order_id_from','order_id_to','created','type_id','delivery_id'])->order('id DESC')->first();    
            if (!empty($findLast->delivery_id)){
                $this->type_id = $findLast->type_id;
                $this->delivery_id = $delivery_id = $findLast->delivery_id;
            }
            $lastIdFrom = $findLast->order_id_from;
            $lastIdTo = $findLast->order_id_to;
        }
        // vyhledani objednavek dle posledniho ID uzaverky
        $this->loadModel('Orders');
        if (!$deadline_id){
            $conditions = [
                'id >'=>$lastIdFrom,
            ];
        } else {
            $conditions = [
                'id >'=>$lastIdFrom,
                'id <='=>$lastIdTo,
            ];
        }

        //pr($conditions);die();
        $findOrders = $this->Orders->find()
        ->contain(['OrderItems'=>function($oitems){
            return $oitems->select([
                'id',
                'order_id',
                'name',
                'price',
                'count',
                'product_group_id',
                'code',
                'price_default',
                'free',
                'storno',
                'storno_type',
                'soft_storno_count',
                'product_id',
                'tax_id',
            ]);
        }])
        ->where($conditions)
        ->select([
            'Orders.id',
            'Orders.total_price',
            'Orders.payment_id',
            'Orders.storno',
            'Orders.delivery',
            'Orders.table_type_id'
        ])
        ->order('id DESC')
        ->toArray();

        if (!$findOrders){
            die(json_encode(['result'=>false,'message'=>'Nejsou žádné objednávky pro zobrazení a tisk']));
        }

        // nacteni skupin trzeb
        $this->loadModel('Products');
        $this->groupTrzbaListLoad = $this->Products->groupTrzbaList();


        // nacteni skupin
        $this->loadModel('ProductGroups');
        $group_list = $this->ProductGroups->groupListDeadline();
        $productsGroup = [];
        foreach($group_list AS $group_id=>$group_name){
            $productsGroup[$group_id] = [
                'name'=>$group_name,
                'total_price'=>0,
                'total_units'=>0,
                'items'=>[],
            ];
        }
        
        if($deadline_type == 'x'){
            $title = 'Uzávěrka náhled';
        }

        if($deadline_type == 'z'){
            $title = 'Uzávěrka';
            $this->closeDeadline = true;
        }

        if ($deadline_id){
            $titleDate = $findLast->created->format('d.m.Y H:i:s');
        } else {
            $titleDate = date('d.m.Y H:i:s');
        }

        // pr($deadline_type);die();

        $sum_group = [];
        foreach($this->group_trzba_list AS $group_id=>$group){
            $taxs = [];
            foreach($this->price_tax_list AS $tax_id=>$tax){
                $taxs[$tax_id] = 0; 
            }
            $sum_group[$group_id] = [
                'name'=>$group,
                'total_without_tax'=>0,
                'total_with_tax'=>0,
                'taxs'=>$taxs,
            ];
        }

        $this->resultDeadline = [
            'header' => [
                'title'                     => $title,
                'titleDate'                 => $titleDate,
            ],
            'orders_data' => [
                'order_id_from'             => $lastIdFrom,
                'order_id_to'               => $findOrders[0]->id,
            ],
            'sum_total_complete' => [
                'orders'                    => 0,
                'total_price'               => 0,
                'items'                     => 0,
                'units'                     => 0,
            ],
            'sum_total_storno' => [
                'orders'                    => 0,
                'total_price'               => 0,
                'items'                     => 0,
                'units'                     => 0,
            ],
            'soft_storno' => [
                'count'                     => 0,
                'price'                     => 0,
                'items'                     => []
            ],
            'sum_group'                 => $sum_group,
            'payed_cards' => [
                'orders'                    => 0,
                'total_price'               => 0,
                'order_items'               => [],
            ],
            'total_price_products' => [
                'products_count'            =>0,
                'total_price'               =>0,
            ],
            'products_list'             => $productsGroup,
            'products_list_delivery'    => $productsGroup,
            'products_list_employee'    => $productsGroup,
            'products_list_company'     => $productsGroup
        ];

        // pokud je rozvozce pridat do tisk
        if (isset($this->deliveryList[$delivery_id])){
            $this->resultDeadline['header']['delivery_name'] = $this->deliveryList[$delivery_id];
        }
        
        $stornoItems = [];
        if($findOrders){
            foreach($findOrders AS $order){
                // pokud je nedokoncena
                if ($order->close_order == 1){
                    $res = $this->getOrderData('sum_total_nocomplete',$order);
                // pokud je dokoncena
                } else {
                    // pokud je storno
                    if ($order->storno > 0){
                       $res = $this->getOrderData('sum_total_storno',$order);
                    // pokud je normalni objednavka    
                    } else {
                        $res = $this->getOrderData('sum_total_complete',$order);
                    }
                }
                
            }
        }
        //pr($this->resultDeadline);
         //pr($this->resultDeadline);die();
        $print_data = $this->printData($this->resultDeadline, $print_type);
        
        // ulozeni uzaverky Z
        $this->saveDeadline();

        // pr($print_data);
        // pr($this->resultDeadline);
        //die();

        $results = [
            'result'=>true,
            'message'=>__('Uzávěrka vygenerována'),
            'print_type'=>$print_type,
            'deadline_type'=>$deadline_type,
            'type_delivery'=>$type_delivery,
            'print_data'=>$print_data,
        ];
        
        $this->setJsonResponse($results);
        //$this->model->tra
    
    }

    private function saveDeadline(){
        // load settings
        if (!isset($this->settings)){
            $this->getSettings();
        }
        if (isset($this->closeDeadline)){
            $save = $this->Deadlines->newEntity([
                'user_id'=>(isset($this->loggedUser['id'])?$this->loggedUser['id']:''),
                'date_close'=>new Time(date('Y-m-d H:i:s')),
                'order_id_from'=>$this->resultDeadline['orders_data']['order_id_from'],
                'order_id_to'=>$this->resultDeadline['orders_data']['order_id_to'],
                'createdTime'=>strtotime(date('Y-m-d H:i:s')),
                'type_id'=>$this->type_id,
                'delivery_id'=>$this->delivery_id,
                'system_id' => $this->settings->data->system_id,
                'sended' => 0
            ]);
            //pr($save);
            if (!$result = $this->Deadlines->save($save)){
                die(json_encode(['result'=>false,'message'=>'Chyba uložení uzávěrky']));
            }
        }
        //pr($result);die();
        
    }

    private function printData($data,$print_type){
        if (isset($this->request->query['debug'])){
            pr($data);die();
        }
        $table_data = [];
        $colWidth = [
            1=>20,
            2=>5,
            3=>12,
        ];  
        $format = "%- ".$colWidth[1]."s % ".$colWidth[2].'.0f'.'% '.$colWidth[3].'.2f';
        $format2 = '%- '.$colWidth[1].'s % '.$colWidth[2].'s'.'% '.$colWidth[3].'.2f';
        $format3 = '%- '.$colWidth[1].'s % '.$colWidth[2].'s'.'% '.$colWidth[3].'s';
        $format4 = '%- '.$colWidth[1].'s % '.$colWidth[2].'s'.'% '.$colWidth[3].'.2f';
        $formatHeader = '%- '.$colWidth[1].'s % '.$colWidth[2].'s % '.$colWidth[3].'.s';

        // total price
        $table_data[] = ['formatTable'=>$format2,'format'=>'normal','data'=>['Produkt'=>' ','Ks'=> ' ','Cena'=>' ']];
        $table_data[] = ['formatTable'=>$format2,'format'=>'bold','data'=>['Produkt'=>'Tržba celkem','Ks'=> ' ','Cena'=>$data['sum_total_complete']['total_price']]];
        $table_data[] = ['formatTable'=>$format3,'format'=>'normal','data'=>['Produkt'=>'------------------------','Ks'=> ' ','Cena'=>' ']];
        // $table_data[] = ['formatTable'=>$format2,'format'=>'normal','data'=>['Produkt'=>'Celkem','Ks'=> ' ','Cena'=>$this->price_format($data['sum_total_complete']['total_price'])]];

        $table_data[] = ['formatTable'=>$format2,'format'=>'normal','data'=>['Produkt'=>'Učtenek','Ks'=> ' ','Cena'=>$data['sum_total_complete']['orders']]];
        $table_data[] = ['formatTable'=>$format2,'format'=>'normal','data'=>['Produkt'=>'Položek', 'Ks'=> ' ','Cena'=>$data['sum_total_complete']['items']]];
        $table_data[] = ['formatTable'=>$format2,'format'=>'normal','data'=>['Produkt'=>'Počet ks', 'Ks'=> ' ','Cena'=>$data['sum_total_complete']['units']]];
        
        // sum_total_storno
        $table_data[] = ['formatTable'=>$format2,'format'=>'normal','data'=>['Produkt'=>' ','Ks'=> ' ','Cena'=>' ']];
        $table_data[] = ['formatTable'=>$format3,'format'=>'normal','data'=>['Produkt'=>'------------------------','Ks'=> ' ','Cena'=>' ']];

        $table_data[] = ['formatTable'=>$format2,'format'=>'bold','data'=>['Produkt'=>'Storna','Ks'=> ' ','Cena'=>$data['sum_total_storno']['total_price']]];
        $table_data[] = ['formatTable'=>$format2,'format'=>'normal','data'=>['Produkt'=>'Celkem','Ks'=> ' ','Cena'=>$data['sum_total_storno']['total_price']]];
        $table_data[] = ['formatTable'=>$format2,'format'=>'normal','data'=>['Produkt'=>'Učtenek','Ks'=> ' ','Cena'=>$data['sum_total_storno']['orders']]];
        $table_data[] = ['formatTable'=>$format2,'format'=>'normal','data'=>['Produkt'=>'Položek', 'Ks'=> ' ','Cena'=>$data['sum_total_storno']['items']]];
        $table_data[] = ['formatTable'=>$format2,'format'=>'normal','data'=>['Produkt'=>'Počet ks', 'Ks'=> ' ','Cena'=>$data['sum_total_storno']['units']]];
         
        
        // pr($table_data);die();
        
        if (isset($data['sum_group'])){
            $table_data[] = ['formatTable'=>$format3,'format'=>'normal','data'=>['Produkt'=>'------------------------','Ks'=> ' ','Cena'=>' ']];
            // new line
            $table_data[] = [
                'format'=>'normal', 
                'formatTable'=>$format,
                'data'=>[
                    'Produkt'=>' ', 
                    'Ks'=> ' ',
                    'Cena'=>' ',
                ],
            ];
            
            $table_data[] = [
                'format'=>'bold',  
                'formatTable'=>$format3,
                'data'=>[
                    'Produkt'=>'Rozpis skupin', 
                    'Ks'=> ' ',
                    'Cena'=>'Cena',
                ],
            ];
            //pr($data['payed_cards']);die();
            foreach($data['sum_group'] AS $oi){
                $table_data[] = [
                    'format'=>'normal',  
                    'formatTable'=>$format4,
                    'data'=>[
                        'Produkt'=>$oi['name'], 
                        'Ks'=> ' ',
                        'Cena'=>$oi['total_without_tax'],
                    ],
                ];
                foreach($oi['taxs'] AS $tax_id=>$taxData){
                    $table_data[] = [
                        'format'=>'normal',  
                        'formatTable'=>$format4,
                        'data'=>[
                            'Produkt'=>$this->price_tax_list[$tax_id], 
                            'Ks'=> ' ',
                            'Cena'=>$taxData,
                        ],
                    ];  
                }
                $table_data[] = [
                    'format'=>'normal',  
                    'formatTable'=>$format4,
                    'data'=>[
                        'Produkt'=>'Celkem s DPH', 
                        'Ks'=> ' ',
                        'Cena'=>$oi['total_with_tax'],
                    ],
                ];
                $table_data[] = ['formatTable'=>$format3    ,'format'=>'normal','data'=>['Produkt'=>'------------------------','Ks'=> ' ','Cena'=>' ']];
           
                //pr($oi);die();
            }
           // pr($data['sum_total_storno']);
     
           if(isset($data['soft_storno']['items']) && !empty($data['soft_storno']['items'])){
                
                $table_data[] = [
                    'format'=>'bold',  
                    'formatTable'=>$format2,
                    'data'=>[
                        'Produkt'=>'Měkké storno', 
                        'Ks'=> $data['soft_storno']['count'],
                        'Cena'=> $data['soft_storno']['price'],
                    ],
                ];
                /*$table_data[] = [
                    'format'=>'bold',  
                    'formatTable'=>$format2,
                    'data'=>[
                        'Produkt'=>' Měk. stor. položek', 
                        'Ks'=> ' ',
                        'Cena'=>$data['soft_storno']['count'] ,
                    ],
                ];*/

                $table_data[] = ['formatTable'=> $format2,'format'=>'normal','data'=>['Produkt'=>' ','Ks'=> ' ','Cena'=>' ']];
                foreach($data['soft_storno']['items'] as $it){
                    $table_data[] = ['formatTable'=> $format2,'format'=>'normal','data'=>['Produkt'=>$it['name'], 'Ks'=> $it['units'], 'Cena'=>$it['price']]];
                }
            }

            // pr($data['sum_group']);
            // pr($table_data);
            // die('a');
        }
        
        
        // platba kartou
        if (isset($data['payed_cards']) && !empty($data['payed_cards']['order_items'])){
            $table_data[] = ['formatTable'=>$format3,'format'=>'normal','data'=>['Produkt'=>'------------------------','Ks'=> ' ','Cena'=>' ']];
        
            // new line
            $table_data[] = [
                'format'=>'normal', 
                'formatTable'=>$format,
                'data'=>[
                    'Produkt'=>' ', 
                    'Ks'=> ' ',
                    'Cena'=>' ',
                ],
            ];
            
            $table_data[] = [
                'format'=>'bold',  
                'formatTable'=>$format,
                'data'=>[
                    'Produkt'=>'Platba kartou', 
                    'Ks'=> ' ',
                    'Cena'=>'Cena',
                ],
            ];
            //pr($data['payed_cards']);die();
            foreach($data['payed_cards']['order_items'] AS $oi){
                $table_data[] = [
                    'format'=>'normal',  
                    'formatTable'=>$format,
                    'data'=>[
                        'Produkt'=>$oi['id'], 
                        'Ks'=> ' ',
                        'Cena'=>$oi['total_price'],
                    ],
                ];
                
                //pr($oi);die();
            }
            $table_data[] = [
                'format'=>'normal',  
                'formatTable'=>$format,
                'data'=>[
                    'Produkt'=>'Celkem', 
                    'Ks'=> ' ',
                    'Cena'=>$data['payed_cards']['total_price'],
                ],
            ];
           
        }
        // new line
        $table_data[] = [
            'format'=>'normal',  
            'formatTable'=>$format,
            'data'=>[
                'Produkt'=>' ', 
                'Ks'=> ' ',
                'Cena'=>' ',
            ],
        ];
            
        // produkty
        foreach($data['products_list'] AS $prodGroup){
            if ($prodGroup['total_units'] > 0){
                // separator line
                $table_data[] = [
                    'format'=>'normal',  
                    'formatTable'=>$format3,
                    'data'=>[
                        'Produkt'=>'--------------', 
                        'Ks'=> '----',
                        'Cena'=>'-------',
                    ],
                ];
                
                $table_data[] = ['format'=>'normal','data'=>['Produkt'=>' ','Ks'=> ' ','Cena'=>' ']];
                $table_data[] = [
                    'format'=>'bold',
                    'formatTable'=>$format,  
                    'data'=>[
                        'Produkt'=>$prodGroup['name'], 
                        'Ks'=> $prodGroup['total_units'],
                        'Cena'=>$prodGroup['total_price'],
                    ],
                ];

                
                foreach($prodGroup['items'] AS $items){
                    $table_data[] = [
                        'formatTable'=>$format,
                        'format'=>'normal',  
                        'data'=>[
                            'Produkt'=>$items['name'], 
                            'Ks'=> $items['units'],
                            'Cena'=>$items['price'],
                        ],
                    ];
                }
            }
        }

        // Products list delivery type.
        $table_data[] = [
            'format'        => 'normal',
            'formatTable'   => $format3,
            'data' => [
                'Produkt'       => '--------------------',
                'Ks'            => '----',
                'Cena'          => '-------'
            ]
        ];

        $table_data[] = [
            'format'        => 'bold',
            'formatTable'   => $format3,
            'data' => [
                'Produkt'       => 'Rozvoz',
                'Ks'            => 'Ks',
                'Cena'          => 'Cena'
            ]
        ];

        foreach ($data['products_list_delivery'] as $prodGroup) {
            if ($prodGroup['total_units'] > 0) {
                // Separator line.
                $table_data[] = [
                    'format'        => 'normal',
                    'formatTable'   => $format3,
                    'data' => [
                        'Produkt'       => '--------------',
                        'Ks'            => '----',
                        'Cena'          => '-------'
                    ]
                ];

                $table_data[] = [
                    'format'        => 'normal',
                    'data' => [
                        'Produkt'       => ' ',
                        'Ks'            => ' ',
                        'Cena'          => ' '
                    ]
                ];

                $table_data[] = [
                    'format'        => 'bold',
                    'formatTable'   => $format,
                    'data' => [
                        'Produkt'       => $prodGroup['name'],
                        'Ks'            => $prodGroup['total_units'],
                        'Cena'          => $prodGroup['total_price']
                    ]
                ];

                foreach ($prodGroup['items'] as $items) {
                    $table_data[] = [
                        'format'        => 'normal',
                        'formatTable'   => $format,
                        'data' => [
                            'Produkt'       => $items['name'],
                            'Ks'            => $items['units'],
                            'Cena'          => $items['price']
                        ]
                    ];
                }
            }
        }

        // Products list employee type.
        $table_data[] = [
            'format'        => 'normal',
            'formatTable'   => $format3,
            'data' => [
                'Produkt'       => '--------------------',
                'Ks'            => '----',
                'Cena'          => '-------'
            ]
        ];

        $table_data[] = [
            'format'        => 'bold',
            'formatTable'   => $format3,
            'data' => [
                'Produkt'       => 'Zaměstnanci',
                'Ks'            => 'Ks',
                'Cena'          => 'Cena'
            ]
        ];

        foreach ($data['products_list_employee'] as $prodGroup) {
            if ($prodGroup['total_units'] > 0) {
                // Separator line.
                $table_data[] = [
                    'format'        => 'normal',
                    'formatTable'   => $format3,
                    'data' => [
                        'Produkt'       => '--------------',
                        'Ks'            => '----',
                        'Cena'          => '-------'
                    ]
                ];

                $table_data[] = [
                    'format'        => 'normal',
                    'data' => [
                        'Produkt'       => ' ',
                        'Ks'            => ' ',
                        'Cena'          => ' '
                    ]
                ];

                $table_data[] = [
                    'format'        => 'bold',
                    'formatTable'   => $format,
                    'data' => [
                        'Produkt'       => $prodGroup['name'],
                        'Ks'            => $prodGroup['total_units'],
                        'Cena'          => $prodGroup['total_price']
                    ]
                ];

                foreach ($prodGroup['items'] as $items) {
                    $table_data[] = [
                        'format'        => 'normal',
                        'formatTable'   => $format,
                        'data' => [
                            'Produkt'       => $items['name'],
                            'Ks'            => $items['units'],
                            'Cena'          => $items['price']
                        ]
                    ];
                }
            }
        }

        // Products list company type.
        $table_data[] = [
            'format'        => 'normal',
            'formatTable'   => $format3,
            'data' => [
                'Produkt'       => '--------------------',
                'Ks'            => '----',
                'Cena'          => '-------'
            ]
        ];

        $table_data[] = [
            'format'        => 'bold',
            'formatTable'   => $format3,
            'data' => [
                'Produkt'       => 'Firma',
                'Ks'            => 'Ks',
                'Cena'          => 'Cena'
            ]
        ];

        foreach ($data['products_list_company'] as $prodGroup) {
            if ($prodGroup['total_units'] > 0) {
                // Separator line.
                $table_data[] = [
                    'format'        => 'normal',
                    'formatTable'   => $format3,
                    'data' => [
                        'Produkt'       => '--------------',
                        'Ks'            => '----',
                        'Cena'          => '-------'
                    ]
                ];

                $table_data[] = [
                    'format'        => 'normal',
                    'data' => [
                        'Produkt'       => ' ',
                        'Ks'            => ' ',
                        'Cena'          => ' '
                    ]
                ];

                $table_data[] = [
                    'format'        => 'bold',
                    'formatTable'   => $format,
                    'data' => [
                        'Produkt'       => $prodGroup['name'],
                        'Ks'            => $prodGroup['total_units'],
                        'Cena'          => $prodGroup['total_price']
                    ]
                ];

                foreach ($prodGroup['items'] as $items) {
                    $table_data[] = [
                        'format'        => 'normal',
                        'formatTable'   => $format,
                        'data' => [
                            'Produkt'       => $items['name'],
                            'Ks'            => $items['units'],
                            'Cena'          => $items['price']
                        ]
                    ];
                }
            }
        }
        
        $this->getSettings();
        //pr($this->settings);die();
        //pr($data);die();  
        
        $table_data = $this->tableGen($table_data,$formatHeader,null,$colWidth);
        // pr($table_data);
        $json_print_data = [
            'printer_name'=>$this->settings['printer'],
            'is_logo'=>false,
            'header'=>[
                [
                    'font'=>'B',
                    'align'=>'center',
                    'format'=>'bold',
                    'data'=>$data['header']['title'],
                ],
                [
                    'font'=>'A',
                    'align'=>'center',
                    'format'=>'',
                    'data'=>$data['header']['titleDate'],
                ],
                [
                    'font'=>'A',
                    'align'=>'center',
                    'format'=>'',
                    'data'=>$this->settings['name'],
                ],
                [
                    'font'=>'A',
                    'align'=>'left',
                    'format'=>'',
                    'data'=>' ',
                ],
                
            ],
            // 'footer'=>[
            //     [
            //         'align'=>'left',
            //         'format'=>'normal',
            //         'data'=>$this->settings->data->print_bottom,
            //     ],
            // ],
            'table_data'=>$table_data,
            'table_align'=>[
                'Produkt'=>'left',
                'Cena'=>'right',
                'Ks'=>'left',	
            ],
        ];
        if (isset($data['header']['delivery_name'])){
            $json_print_data['header'][] = [
                'font'=>'B',
                'align'=>'center',
                'format'=>'bold',
                'data'=>'Rozvozce: '.$data['header']['delivery_name']
            ];
            $json_print_data['header'][] = [
                'font'=>'A',
                'align'=>'left',
                'format'=>'',
                'data'=>' ',
            ];
        }
        return $json_print_data;
    }


    function price_format($price){
        // pr(number_format($price, 2, '.', '&nbsp;'));die();
        return number_format($price, 2, '.', '');
    }

    private function getOrderData($type,$order){
        $units = 0;
        $this->plusFreeOrder = false;

        foreach($order->order_items AS $oi){
            if($oi->storno_type == 2){
                $count = 0;
                if(isset($oi->count) && $oi->count > 0){
                    $count += $oi->count;
                    if($oi->soft_storno_count > 0){
                        $count += $oi->soft_storno_count;
                    }
                    $this->resultDeadline['soft_storno']['count'] += $count;
                
               
                    $price = round($oi->price * $count, 2);
                    if(isset($oi->price) && $oi->price > 0){
                        $this->resultDeadline['soft_storno']['price'] +=  $price;
                    }
                    
                    $this->resultDeadline['soft_storno']['items'][] = [
                        'name'=>mb_substr($oi->name,0,25),
                        'price'=>$price,
                        'units'=>$count,
                    ];
                }
            }else if($oi->soft_storno_count > 0){
                $price = round($oi->price * $oi->soft_storno_count, 2);
                $this->resultDeadline['soft_storno']['items'][] = [
                    'name'=>mb_substr($oi->name,0,25),
                    'price'=>$price,
                    'units'=>$oi->soft_storno_count,
                ];
            }
        }

        if ($type == 'sum_total_storno'){  
            $this->resultDeadline[$type]['orders'] += 1;
            $this->resultDeadline[$type]['total_price'] += $order->total_price;
            $this->resultDeadline[$type]['items'] += count($order->order_items);
         
            foreach($order->order_items AS $oi){
                $this->resultDeadline[$type]['units'] += $oi->count;
            }
            return false;
        }

        foreach($order->order_items AS $oi){
            
            $units += $oi->count;

            // sum group trzba
            if (isset($this->groupTrzbaListLoad[$oi->product_id])){
                if ($oi->storno == 0){
                    $trzba_id = $this->groupTrzbaListLoad[$oi->product_id];
                    $this->resultDeadline['sum_group'][$trzba_id]['total_with_tax'] += $oi->price * $oi->count;
                    if (!isset($oi->tax_id) || empty($oi->tax_id)){
                        $oi->tax_id = 1;
                    }
                    
                    $this->resultDeadline['sum_group'][$trzba_id]['total_without_tax'] += ($oi->price - $this->price_tax_list_con[$oi->tax_id]*$oi->price) * $oi->count;
                    $this->resultDeadline['sum_group'][$trzba_id]['taxs'][$oi->tax_id] += ($this->price_tax_list_con[$oi->tax_id]*$oi->price) * $oi->count;
                }
            }
    
            // pokud je doprava pripocti
            if ($oi->code =='dop'){
                $this->resultDeadline['sum_transport']['price'] += $oi->price;
                $this->resultDeadline['sum_transport']['orders'] += 1;
            }

            if ($order->delivery == 1) {
                $product_list = 'products_list_delivery';
            } elseif ($order->table_type_id == 2) {
                $product_list = 'products_list_employee';
            } elseif ($order->table_type_id == 3) {
                $product_list = 'products_list_company';
            } else {
                $product_list = 'products_list';
            }

            if (isset($this->resultDeadline[$product_list][$oi->product_group_id])  ){
                if ($oi->storno == 0){
                    $this->resultDeadline[$product_list][$oi->product_group_id]['total_price'] += ($oi->price * $oi->count);
                    $this->resultDeadline[$product_list][$oi->product_group_id]['total_units'] += $oi->count;
                }
                //sum_total_free
                if (isset($oi->free) && $oi->free == 1){
                    $this->plusFreeOrder = true;
                    $this->resultDeadline['sum_total_free']['total_price'] += $oi->price_default;
                    $this->resultDeadline['sum_total_free']['items'] += 1;
                }
                
                if (!isset($this->resultDeadline[$product_list][$oi->product_group_id]['items'][$oi->code])){
                    if ($oi->storno == 0){
                        $this->resultDeadline[$product_list][$oi->product_group_id]['items'][$oi->code] = [
                            'name'=>mb_substr($oi->name,0,25),
                            'price'=>($oi->price * $oi->count),
                            'units'=>$oi->count,
                        ];
                    } else {
                    }
                } else {
                    if ($oi->storno == 0){
                        $this->resultDeadline[$product_list][$oi->product_group_id]['items'][$oi->code]['price'] += ($oi->price * $oi->count);
                        $this->resultDeadline[$product_list][$oi->product_group_id]['items'][$oi->code]['units'] += $oi->count;
                    }
                }
            }

            if ($type == 'sum_total_storno'){
                $this->resultDeadline['sum_total_storno']['total_price'] += ($oi->price * $oi->count);
                $this->resultDeadline['sum_total_storno']['units'] += $oi->count;
            }

            if ($oi->storno == 0){
                $this->resultDeadline['total_price_products']['total_price'] += ( $oi->price * $oi->count);
                $this->resultDeadline['total_price_products']['products_count'] += $oi->count;
            }
            
        }
        //pr($this->resultDeadline); die();
        //pr($this->resultDeadline['total_price_products']); die();
        
        if (isset($this->plusFreeOrder)){
            //$this->resultDeadline['sum_total_free']['orders'] ++;
        }

        // platba kartou
        if ($order->payment_id == 2){
            $this->resultDeadline['payed_cards']['orders'] += 1;
            $this->resultDeadline['payed_cards']['total_price'] += $order->total_price;
            $this->resultDeadline['payed_cards']['order_items'][] = [
                'id'=>$order->id,
                'total_price'=>$order->total_price,
            ];
        }
        
            if ($type == 'sum_total_complete'){

            }
        if ($this->resultDeadline[$type]){
            if ($type == 'asum_total_complete'){   
                pr('id'.$order->id);    
                pr($order);
                pr('a'.count($order->order_items));
                die();    
            }
            if (empty($order->total_price)){
                $order->total_price = 0;
                foreach($order->order_items AS $oi){
                    if ($oi->storno == 0){
                        $order->total_price += $oi->price;
                    }
                }
            }
   
            if ($type == 'sum_total_complete'){

            }
            $this->resultDeadline[$type]['orders'] += 1;
            $this->resultDeadline[$type]['total_price'] += $order->total_price;
            if ($type == 'sum_total_complete'){
                $sumTotal = 0;
                foreach($order->order_items AS $oi){
                    
                    if ($oi->storno == 0){
                        $sumTotal ++;
                    }
                }
                $this->resultDeadline[$type]['items'] += $sumTotal;
            
            } else {
                $this->resultDeadline[$type]['items'] += count($order->order_items);
            
            }
            $this->resultDeadline[$type]['units'] += $units;
        }
        
        if ($type == 'sum_total_complete'){

        }
    }

    public function sendToCloud(){
        if (!isset($this->settings)){
            $this->getSettings();
        }

        $data = $this->Deadlines->find()
            ->select([])
            ->where(['type_id'=>1,'sended'=>0])
            ->order('id DESC')
            ->hydrate(false)
            ->limit(10)
            ->toArray();    
        ;
        //pr(count($data));
        if (!$data){
            die(json_encode($result = ['result'=>false,'message'=>'Není žádná uzávěrka']));
        }
        $this->sendedIds = [];
        foreach($data AS $k=>$d){
            // pr($this->settings->data->system_id);die();
            $data[$k]['branch_id'] = $this->settings->data->system_id;
            $data[$k]['terminal_id'] = $this->loggedUser['terminal_id'];
            $data[$k]['user_id'] = $this->loggedUser['id'];
            $data[$k]['pokladna_id'] = $data[$k]['id'];
            $data[$k]['created'] = $data[$k]['created']->format('Y-m-d H:i:s');
            $data[$k]['modified'] = $data[$k]['modified']->format('Y-m-d H:i:s');
            $this->sendedIds[] = $data[$k]['id']; 
            $deadline_id = $data[$k]['id'];
            $this->close('show','x',$deadline_id);
            $resultData = $this->resultDeadline;
            // pr($resultData);
            $data[$k]['data'] = json_encode($resultData);
            unset($data[$k]['id']);
            
        }
        
        // pr($data);die();
        
        $ch = curl_init();
        $post = $data;
        //pr($post);die();
        
		$curlUrl = API_URL.'api/deadlines/save/';
        curl_setopt($ch, CURLOPT_URL, $curlUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec ($ch);
		// pr($result);
		if (empty($result)){
			$result = [
				'result'=>false,
				'message'=>'Není spojení se serverem pro uložení uzávěrek, zkontrolujte připojení k internetu a dostupnost intranetu',
			];
		} else {
            if (!$this->isJson($result)){
                echo($result);
            } else {
                $result = json_decode($result,true);
                $this->Deadlines->updateAll(
                    ['sended' => 1,
                    ], // fields
                    ['id IN' => $this->sendedIds]
                );
                
                //pr($this->sendedIds);
                
            }
        }
        die(json_encode($result));
    }

    function isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    
    
}

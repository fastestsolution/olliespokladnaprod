<?php

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Core\Exception\Exception;
use Cake\I18n\I18n;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\Time;
use Cake\Database\Type;
use Cake\Core\Configure;
Time::$defaultLocale = 'cs-CZ';
Type::build('date')->setLocaleFormat('yyyy-MM-dd');
Type::build('datetime')->setLocaleFormat('yyyy-MM-dd HH:mm:ss'); 

class AppController extends Controller
{
    public $paginate = [
        'limit' => 50,
        'order' => [
        ]
    ];

    public $return = [
        'data' => [],
        'lists' => [],
        'result' => false,
        'count' => null,
        'message' => null,
       // 'validations' => [],
       // 'validation_errors' => false // if validation error is thrown... then reutrn array with errors
    ];
   
    public $languages = ['cz'];
    public $system_id;

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Paginator', [
            'limit' => 25
        ]);
        
        if(isset($_GET['per-page']) && $_GET['per-page'] > 0 && $_GET['per-page'] <= 60){
            $this->paginate['limit'] = $_GET['per-page'];
        }
    }

        
    public function beforeFilter( \Cake\Event\Event $event){
        $this->load_select_config();
        $this->getSettings();
         
        $this->checkServerType(); 
        I18n::locale('cz');
         $loginAction = [ 'controller' => 'users', 'action' => 'login', 'prefix'=>false];
         $this->loadComponent('Auth', [
             'loginRedirect' => ['controller' => 'users', 'action' => 'ng_layout'],
             'loginAction' => $loginAction,
             'authorize' => [
                 'Controller'
             ],
             'authenticate' => [
                    'Form' => [
                        'userModel' => 'Users',
                        'fields' => ['username' => 'username', 'password'=>'password'],
                    ]
              ],
              'checkAuthIn'=> 'Controller.initialize'
         ]);
         $this->checkLogged();


    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }


     // generovani tabulky
    public function tableGen($table_data,$formatHeader = '%-25.25s %3s %3s %5s %5s',$format = '%-25.25s %3.0f %3.0f %5.2f %5.2f',$colWidth=null){
        $table = [];
        // pr($table_data);die();
        foreach($table_data AS $k=>$row){
            $i = 1;
            foreach($row AS $rid=>$rr){
                if ($rid == 'data'){
                    foreach($rr AS $kk=>$r){
                        $table_data[$k][$rid][$kk] = mb_substr($r,0,$colWidth[$i]);
               
                    }
                $i++;
                }
            }
            //pr($k);
            $i++;
        }
        // pr($table_data);die();
        foreach($table_data AS $k=>$row){
            if (isset($row['formatTable'])){
                $format = $row['formatTable'];
                unset($table_data[$k]['formatTable']);
            }

            foreach($row AS $kr=>$drow){
                if ($kr == 'data'){
                    // hlavicka tabulky
                    if (!isset($header)){
                        // $row['data']['Kč jed.'] = $row['data']['Kč/jed.'];
                        // unset($row['data']['Kč/jed.']);
                        $lineHeader = vsprintf($formatHeader, array_keys($row['data']));
                        
                        // pr($lineHeader);die();
                        $table[-10] = [ 
                            'format'=>'',
                            'data' => $lineHeader,
                        ];
                        $len = strlen($lineHeader);
                        $sep = str_repeat("-", $len);
                        $table[-9] = [ 
                            'format'=>'',
                            'data' => $sep,
                        ];
                    
                    }
                    $first_key = key($drow);
                    //pr($first_key);
                    if ($row['data'][$first_key] == ' '){
                        $line = ' ';
                    } else {
                        $i = 1;
                        // pr($row['data']);
                        
                            //pr($k);
                        // pr($row['data']);
                        // $format = '%-.25s %5s %7.2f';
                        $line = vsprintf($format, $row['data']);
                        //  pr($row['data']);
                        //  pr($colWidth);
                        //  pr($format);
                        // pr($line);
                    }
                    $table[$k]['data'] = $line;
                    // pr($line);
                } else {
                    //pr($k);
                    unset($row['formatTable']);
                    $table[$k] = $row;
                }
            }
        }
        // die();
        ksort($table);
        $result = [];
        foreach($table AS $t){
            $result[] = $t;
        }
        // pr($result);die();
        /*
        $txt = '';
        foreach($result AS $r){
            $txt .= $r['data']."\n";
        }
        pr($txt);
        pr($result);die();
        /**/
        return $result;
        
    }
    /**
	 * poslani curl request
	 */
	public function conCurl($curlUrl,$post, $sendRawData = false){
    
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $curlUrl);
        curl_setopt($ch, CURLOPT_POST, 1);
        if(!$sendRawData){
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
        }else{
            curl_setopt($ch, CURLOPT_POSTFIELDS,     json_encode($post) ); 
            curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type: text/plain')); 
        }
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        // pr($post);
        $result = curl_exec ($ch);
       
        if (empty($result)){
            $result = [
				'result'=>false,
				'message'=>'Není spojení se serverem',
			];
        }
        curl_close($ch);
        return $result;
    }
    
    public function getData(){
        $data = file_get_contents('php://input');
        if (!empty($data)){
            return json_decode($data,true);
        } else {
            return false;
        }
    }
   
    public function checkServerType(){
        if (strncasecmp(PHP_OS, 'WIN', 3) == 0) {
            $this->serverType = 'win';
        } else {
            $this->serverType = 'linux';
        }
   }

    
    private function checkLogged(){
        if ($this->Auth->user()){
            $this->set('loggedUser',$this->Auth->user());
            $this->loggedUser = $this->Auth->user();
        }
    }

    /**
     * Nacteni nastaveni dane pokladny
     */
    
    public function getSettings(){
        if(!($this->settings = $this->request->session()->read('settings'))){
            $this->loadModel('Settings');
            $this->settings = $this->Settings->getSetting();
            $this->request->session()->write('settings', $this->settings);
        }
        $this->system_id = $this->settings->data->system_id;
    }

    /**
     * clear cache
     */
    public function clearCache($name){
        $files = scandir('../tmp/cache');
        foreach($files AS $f){
            if (strpos($f,'cake_'.$name) === 0){
                unlink('../tmp/cache/'.$f);
                //pr($f);
            }
        }
        return true;
    }

    

    
    protected function setJsonResponse($results){
        if (!isset($this->request->query['firstLoad'])){
            //unset($results['select_list']);
            unset($results['top_actions']);
            unset($results['filtration']);
            unset($results['table_th']);
        }
        //pr($this->request->query['firstLoad']);  
        
        $this->loadComponent('RequestHandler');
        $this->RequestHandler->renderAs($this, 'json');
        $this->response->type('application/json');
        $this->set([
			'results' => $results,
			'_serialize' => 'results'
        ]);
    }
        
    
    private function load_select_config(){
		$sc = Configure::read('select_config');
		foreach($sc AS $k=>$item){
			$this->$k = $item;
			$this->set($k,$item);
		}
    }
    
}

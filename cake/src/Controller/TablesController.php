<?php
namespace App\Controller;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;
use Cake\Cache\Cache;
use Cake\I18n\Time;
use App\Component\vIComponent;

class TablesController extends AppController{
    
    /**
     * vytvoreni select listu
     */
    public function getSelectList($check=true){
        if ($check == false || isset($this->request->query['firstLoad'])){
            return $select_list = [
                'no_yes'=>$this->no_yes,
                'tables_groups_list'=>$this->tables_groups_list,
                'tables_types'=>$this->tables_types,
            ];
        } else {
            return false;
        }  
        
        
    }

    /**
     * INDEX orders
     */
    public function index()
    {
        $this->loadComponent('vI');

        $select_list = $this->getSelectList();
        //pr($select_list);die();
        $conditions = [];
        // $conditions = ['id'=>9];
        $fields_defined = [
            0=>['col'=>'id','title'=>'ID','type'=>'text'],
            1=>['col'=>'name','title'=>'Název','type'=>'text'],
            2=>['col'=>'num','title'=>'Číslo','type'=>'text'],
            3=>['col'=>'group_id','title'=>'Skupina','type'=>'list','list_data'=>$select_list['tables_groups_list']],
            4=>['col'=>'table_type_id','title'=>'Typ','type'=>'list','list_data'=>$select_list['tables_types']],
            5=>['col'=>'created','title'=>'Vytvořeno','type'=>'datetime'],
            
        ];

        $this->filtration_defined = [
            'id'=>['col'=>'id','title'=>'ID','type'=>'text'],
            'name'=>['col'=>'name','title'=>'Jméno','type'=>'text_like'],
            'num'=>['col'=>'num','title'=>'Číslo','type'=>'text_like'],
            // 'num'=>['col'=>'num','title'=>'Číslo','type'=>'text_like'],
            // 'code'=>['col'=>'code','title'=>'Kod','type'=>'text_like'],
            'group_id'=>['col'=>'group_id','title'=>'Skupina','type'=>'select','list'=>$this->vI->filtrSelectList($select_list['tables_groups_list'])],
            'table_type_id'=>['col'=>'table_type_id','title'=>'Typ','type'=>'select','list'=>$this->vI->filtrSelectList($select_list['tables_types'])],
            
            //'created'=>['col'=>'created','title'=>'Vytvořeno','type'=>'date_range'],
            //'modified'=>['col'=>'modified','title'=>'Upraveno','type'=>'date'],
        ];
        
        if (isset($this->request->data['conditions'])){
            //pr($this->request->data['conditions']);
            $conditions = $this->vI->convertConditions($conditions,$this->request->data['conditions'],$this->filtration_defined);
            //pr($conditions);die();
        }
        
       
        $posibility = [
            //0=>['link'=>'/api/status/Tables/','title'=>__('Status'),'class'=>'fa-check-circle-o','type'=>'ajax','params'=>'status'],
            0=>['link'=>'./edit/','title'=>__('Editovat'),'class'=>'fa-edit','type'=>'router','params'=>'edit'],
            1=>['link'=>'/api/trash/Tables/','title'=>__('Smazat'),'class'=>'fa-trash','type'=>'ajax','params'=>'trash','confirm'=>__('Opravdu smazat?')],
        ];
        
        $top_actions = [
            0=>['link'=>'./edit/','title'=>__('Nová položka'),'class'=>'fa-edit','type'=>'router','params'=>'edit'],
        ];

        
        $fields = $this->vI->fieldsConvert($fields_defined,['status'=>false]);
        //pr($fields);die();
        
        $mapper = function ($data, $key, $mapReduce) {
            //$data->operating = (($data->operating == '')?0:1);
            //$data->problem = (($data->problem == '')?0:1);
            $mapReduce->emit($data);  
        };

        $query = $this->Tables->find()
            ->select($fields)
            ->where($conditions)
            ->cache(function ($query) {
				return 'table_data-' . md5(serialize($query->clause('where')));
            })
            ->mapReduce($mapper)
        ;

        
        //$data = $query;
        //pr($query->toArray());die();
        
        
        $this->loadComponent('Paginator');
        $data_list = $this->paginate($query);
        
        
        
        $pagination = $this->vI->convertPagination();
        //pr($pagination['page']);
        $results = [
            'result'=>true,
            'data'=>$data_list->toArray(),
            'data_count'=>count($data_list->toArray()),
            'pagination'=>$pagination,
            'table_th'=>$fields_defined,
            'filtration'=>array_values($this->filtration_defined),
            'select_list'=>$select_list,
            'posibility'=>$posibility,
            'top_actions'=>$top_actions,
            'conditions'=>(object) $conditions,
        ];
        
        $this->setJsonResponse($results);
    }


    /**
     * kontrola existence kodu
     */
    private function checkCode(){
        //pr($this->request);
        $con = [
            'code'=>$this->request->data['saveData']['code'],
        ];
        if (!empty($this->request->data['saveData']['id'])){
            $con['id !='] = $this->request->data['saveData']['id'];
        }

        $find = $this->Tables->find()
        ->select([])
        ->where($con)
        ->first();
        if ($find){
            $results = [
                'result'=>false,
                'message'=>__('Kód produktu je již použit'),
            ];
            die(json_encode($results));  
    
        }
    }

    /**
     * editace
     */
    public function edit($id=null, $return = false){
        $select_list = $this->getSelectList(false);
        $this->loadComponent('vI');
        //pr($select_list);die();
        
        // save data
        if (isset($this->request->data['saveData'])){
            $saveData = $this->request->data['saveData'];
            $saveData = $this->vI->convertTime($saveData);

            //$this->checkCode();
            // pr($saveData);
            
            $saveData['system_id'] = $this->system_id;

            $save_entity = $this->Tables->newEntity($saveData);
            //pr($save_entity); die();
            
            $this->vI->checkErrors($save_entity);

            if (!$resultDb = $this->Tables->save($save_entity)){
                $results = [
                    'result'=>false,
                    'message'=>__('Chyba uložení')
                ];    
            } else {

                $res = $this->curl_send(API_URL . '/api/saveTable' , $save_entity->toArray());
             
                $results = [
                    'result'=>true,
                    'message'=>__('Uloženo'),
                    'data'=>$resultDb
                ];  
            }
            if(!$return){
                $this->setJsonResponse($results);
            }
        // load data
        } else {
            $defaultValues = [
                'nodelete'=>0,
            ];
            if ($id != null){
                // pokud je edit
                $mapper = function ($data, $key, $mapReduce) {
                    
                    $mapReduce->emit($data);  
                };
        
                if ($id != 'new'){
                    $conditions = ['id'=>$id];
                    $data = $this->Tables->find()
                    ->select()
                    ->where($conditions)
                    ->mapReduce($mapper)
                    ->first()
                ;
                // pokud je nova polozka
                } else {
                   
                    $data = $this->vI->emptyEntity('Tables'); 
                    
                }
                $data = $this->vI->convertLoadData($data,$defaultValues);        
                $data->addons = json_decode($data->addons);
                $validations = $this->vI->getValidations('Tables');
            } else {

            }
            $results = [
                'result'=>true,
                'validations'=>(isset($validations)?$validations:null),
                'data'=>(isset($data)?$data:''),
                'select_list'=>$this->vI->SelectList($select_list),
            ];  
            //pr($results);die();  
            $this->setJsonResponse($results);
        }
        $this->clearCache('table_data');
    }

    public function exportAll(){
        $this->loadModel('Tables');
        $ok = $err = 0;
        foreach( $this->Tables->find() as $table){
            $table->system_id = $this->system_id;
            //pr($table->toArray());
            $res = $this->curl_send(API_URL . '/api/saveTable' , $table->toArray());
            if($res == 'ok'){
                $ok++;
            }else{
                pr($res);
                $err++;
            }
        }
        die(json_encode(['ok'=>$ok, 'err'=>$err]));
    }

    public function saveAddon(){
        $conditions = [];
        $data = $this->Tables->find()
            ->select()
            ->where($conditions)
            ->toArray();
            
        $this->loadModel('TableConnAddons');    
        $list_load = $this->TableConnAddons->find()
            ->select()
            ->where()
            ->toArray();
        $list = [];
        foreach($list_load AS $l ){
            $list[$l->table_id][] = $l->table_addon_id;
        }
        //pr($list);
        foreach($data AS $k=>$d){
            if (isset($list[$d->id])){
            $resultDb = $this->Tables->updateAll(
                ['addons'=>json_encode($list[$d->id])], // fields
                ['id' => $d->id]
            );
            }
        }
        pr($data);
        die('a');
    }

    protected function curl_send($url, $data){
        $this->sended = null;
		$ch =  curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
                curl_setopt($ch, CURLOPT_TIMEOUT, 10);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $result = curl_exec ($ch);
     
                if($result === false){
                    $result = 'error: '. curl_error($ch) . ' ' . $url;
                }else{
                    $json = json_decode($result, true);
                    if(!$json){
                        $result = $result;
                    }else if($json['result'] == true){
                        $result = 'ok'; 
                    }else{
                        $result = $json['message'];
                    }
                }
        curl_close($ch);
        
        return $result;
    }
}
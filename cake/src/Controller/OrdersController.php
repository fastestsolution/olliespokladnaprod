<?php
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;
use Cake\Cache\Cache;
use Cake\I18n\Time;
use App\Component\vIComponent;
use App\Component\WebsocketClient;
use Cake\Utility\Hash;

ini_set('max_execution_time', 180);

class OrdersController extends AppController{
    
    public function beforeFilter( \Cake\Event\Event $event){
        parent::beforeFilter($event);
        $this->getSettings();
    }
    /**
     * vytvoreni select listu
     */
    public function getSelectList($check=true){
        if ($check == false || isset($this->request->query['firstLoad'])){
            $this->loadModel('Deliverys');
            $this->loadModel('Settings');
            $this->loadModel('Tables');
            return $select_list = [
                'price_tax_list'=>$this->price_tax_list,
                'price_tax_list_con'=>$this->price_tax_list_con,
                'close_order_list'=>$this->close_order_list,
                'no_yes'=>$this->no_yes,
                'area_list'=>$this->area_list = [],
                'source_list'=>$this->source_list,
                'payment_list'=>$this->payment_list,
                'storno_type_list'=>$this->storno_type_list,
                'tables_types'=>$this->tables_types,
                'distributor_list'=>$this->Deliverys->deliveryList(),
                'operations_list'=>$this->Settings->operationsList(),
                'table_list'=>$this->Tables->tableList(),
                'table_list_all'=>$this->Tables->tableListAll(),
            ];
        } else {
            return false;
        }  
    }

    /**
     * INDEX orders
     */
    public function index($deadline_id=null)
    {
        $this->loadComponent('vI');
        
        $select_list = $this->getSelectList();

        $conditions = [];

        $fields_defined = [
            ['col'=>'table_open','title'=>'Otevření stolu','type'=>'datetime'],
            ['col'=>'created','title'=>'Vytvořeno','type'=>'datetime'],
            ['col'=>'table_id','title'=>'Stůl','type'=>'list','list_data'=>$select_list['table_list']],
            ['col'=>'total_price','title'=>'Tržba','type'=>'text'],
            ['col'=>'pay_casch','title'=>'Hotovost','type'=>'text'],
            ['col'=>'pay_card','title'=>'Karta','type'=>'text'],
            ['col'=>'pay_voucher','title'=>'Poukázka','type'=>'text'],
            ['col'=>'pay_discount','title'=>'Sleva','type'=>'text'],
            ['col'=>'pay_employee','title'=>'Zam.','type'=>'text'],
            ['col'=>'pay_company','title'=>'Firma','type'=>'text'],
            ['col'=>'payment_id','title'=>'Platba','type'=>'list','list_data'=>$select_list['payment_list']],
            ['col'=>'storno','title'=>'Storno','type'=>'list','list_data'=>$select_list['no_yes']],
            ['col'=>'table_type_id','title'=>'Typ stolu','type'=>'list','list_data'=>$select_list['tables_types']],
            ['col'=>'id','title'=>'ID','type'=>'text'],
        ];

        $this->filtration_defined = [
            'id'=>['col'=>'id','title'=>'ID','type'=>'text'],
            'created'=>['col'=>'created','title'=>'Vytvořeno','type'=>'date_range'],
            //'modified'=>['col'=>'modified','title'=>'Upraveno','type'=>'date'],
            'payment_id'=>['col'=>'payment_id','title'=>'Platba','type'=>'select','list'=>$this->vI->filtrSelectList($select_list['payment_list'])],
            'storno'=>['col'=>'storno','title'=>'Storno','type'=>'select','list'=>$this->vI->filtrSelectList($select_list['no_yes'])],
            'table_id'=>['col'=>'table_id','title'=>'Stůl','type'=>'select','list'=>$this->vI->filtrSelectList($select_list['table_list'])],
            'table_type_id'=>['col'=>'table_type_id','title'=>'Typ stolu','type'=>'select','list'=>$this->vI->filtrSelectList($select_list['tables_types'])],
        ];
        
        
        if (isset($this->request->data['conditions'])){
            $conditions = $this->vI->convertConditions($conditions,$this->request->data['conditions'],$this->filtration_defined);
            // pr($conditions);die();
        }

        $posibility = [
            ['link'=>'/api/orders/show/','title'=>__('Zobrazit'),'class'=>'fa-search','type'=>'ajax','params'=>'showPrint'],
            ['link'=>'/api/orders/printOrder/','title'=>__('Tisk'),'class'=>'fa-print','type'=>'ajax','params'=>'print'],
            //2=>['link'=>'/api/orders/storno/','title'=>__('Storno'),'class'=>'fa-close','type'=>'ajax','params'=>'storno'],
            ['link'=>'./storno/','title'=>__('Stornovat'),'class'=>'fa-close','type'=>'router','params'=>'storno'],
            //2=>['link'=>'/api/orders/storno_nobake/','title'=>__('Storno neupečeno'),'class'=>'fa-battery-quarter','type'=>'ajax','params'=>'storno'],
            
            //0=>['link'=>'/api/status/Orders/','title'=>__('Status'),'class'=>'fa-check-circle-o','type'=>'ajax','params'=>'status'],
            //1=>['link'=>'./edit/','title'=>__('Editovat'),'class'=>'fa-edit','type'=>'router','params'=>'edit'],
            //2=>['link'=>'/api/trash/Orders/','title'=>__('Smazat'),'class'=>'fa-trash','type'=>'ajax','params'=>'trash','confirm'=>__('Opravdu smazat?')],
        ];
        
        $top_actions = [
            ['link'=>'/api/orders/storno/','title'=>__('Stornovat účet'),'class'=>'fa-edit','type'=>'ajax','params'=>'storno'],
           // 1=>['link'=>'#showStornoButton','title'=>__('Zobrazit storno'),'class'=>'fa-edit','type'=>'fce','params'=>'showStornoButton'],
        ];

        
        $fields = $this->vI->fieldsConvert($fields_defined);
        $mapper = function ($data, $key, $mapReduce) {
            $data->close_order = (($data->close_order)?1:0);
            $data->storno = (($data->storno)?1:0);
            $data->operating = (($data->operating)?1:0);
            $data->total_price = $this->price($data->total_price);
            $mapReduce->emit($data);
        };


        $this->loadModel('Deadlines');
        if ($deadline_id){
            $deadline = $this->Deadlines->find()
            ->select(['order_id_from','order_id_to'])
            ->where(['id'=>$deadline_id])
            ->first();
            if ($deadline){
                $conditions = [
                    'id >'=>$deadline->order_id_from,
                    'id <='=>$deadline->order_id_to,
                ];
            }
            // pr($deadline);die();
        } else {
            $from = $this->Deadlines->findLast();
            
            $conditions['id >'] = $from;
        }
        // pr($conditions);die();
        $query = $this->Orders->find()
            ->select($fields)
            ->where($conditions)
            ->mapReduce($mapper)
            ->cache(function ($query) {
				return 'orders_data-' . md5(serialize($query->clause('where')));
			})
        ;
        //$data = $query;
        
        $this->loadComponent('Paginator');
        $dataClone = clone $query;
        $data_list_sum = $dataClone->toArray();
        // pr(count($data_list_sum));
        $data_list = $this->paginate($query);
        // pr(count($data_list));die();
        
        
        $this->sum_list_name = [
            'pay_total'=> 'Tržba celkem',
           /* 'price_delivery'=> 'Rozvoz',
            'price_push'=> 'Vklad / výběr',*/
            'price_storno'=> 'Storno',
          /*  'price_discount'=> 'Sleva',
            'price_back'=> 'Vratka',
            'price_stock'=> 'Vráceno na sklad',
            'price_tables'=> 'Přesun mezi stoly',*/
        ]; 
        $this->sum_list_load = [
            'pay_total'=>0,
           /* 'price_delivery'=>0,
            'price_push'=>0,*/
            'price_storno'=>0,
            /*'price_discount'=>0,
            'price_back'=>0,
            'price_stock'=>0,
            'price_tables'=>0,*/
        ];
        $this->sum_list_col = [
            'pay_total'=>'total_price',
           /* 'price_delivery'=>0,
            'price_push'=> 0,*/
            'price_storno'=>'total_price',
          /*  'price_discount'=>0,
            'price_back'=>0,
            'price_stock'=>0,
            'price_tables'=>0,*/
        ];

        $sumRow = (object)[
            'table_open'=>'',
            'created'=>'',
            'table_id'=>'',
            'total_price'=>0,
            'pay_casch'=>0,
            'pay_card'=>0,
            'pay_voucher'=>0,
            'pay_discount'=>0,
            'pay_employee'=>0,
            'pay_company'=>0,
            'payment_id'=>0,
            'storno'=>0,
            'table_type_id'=>'',
            'id'=>'-1000',
        ];
        $data_list = $data_list->toArray();
        foreach($data_list AS $d){
            if ($d->storno == 1){
                $this->sum_list_load['price_storno'] += $d->total_price;
            }
            if ($d->storno != 1){
                $this->sum_list_load['pay_total'] += $d->total_price;
                $sumRow->total_price += $d->total_price;
                $sumRow->pay_casch += $d->pay_casch;
                $sumRow->pay_card += $d->pay_card;
                $sumRow->pay_voucher += $d->pay_voucher;
                $sumRow->pay_discount += $d->pay_discount;
                $sumRow->pay_employee += $d->pay_employee;
                $sumRow->pay_company += $d->pay_company;
            }
        }
        $this->sum_list = [];
        foreach($this->sum_list_load AS $key=>$sl){
            $this->sum_list[] = [
                'name'=>$this->sum_list_name[$key],
                'col'=>$key,
                // 'count'=>$sl,
                'count'=>0,
            ];
        }

        foreach($data_list_sum AS $d){
            if ($d->storno == 1){
                $this->sum_list[1]['count'] += $d->total_price*1;
            } else {
                $this->sum_list[0]['count'] += $d->total_price*1;
                
            }
        }

        $data_list[] = $sumRow;

        $pagination = $this->vI->convertPagination();

        $results = [
            'result'=>true,
            'data'=>$data_list,
            'data_count'=>count($data_list),
            'pagination'=>$pagination,
            'table_th'=>$fields_defined,
            'filtration'=>array_values($this->filtration_defined),
            'select_list'=>$select_list,
            'sum_list'=>$this->sum_list,
            'sum_list_col'=>$this->sum_list_col,
            'posibility'=>$posibility,
            'top_actions'=>$top_actions,
            'conditions'=>(object) $conditions,
        ];
        
        $this->setJsonResponse($results);
    }

    /**
     * poslani dat na cloud objednvaky
     */
    public function sendOrders(){
        $data = $this->getData();
        if (!isset($data)){
            die(json_encode(['result'=>true,'message'=>'Chyba odeslani objednavky, nejsou data']));
        }
        foreach($data AS $post){
            $curlUrl =  API_URL.'api/receiveOrders/'.$this->settings->data->system_id;
            
            $post['user_name'] = $this->loggedUser['name'];
            $post['user_id'] = $this->loggedUser['id'];
            $post['terminal_id'] = $this->loggedUser['terminal_id'];
            $post['system_id'] = $this->settings->data->system_id;
            // pr($post);die();
            $result = $this->conCurl($curlUrl,$post);
    
            $res = @json_decode($result,true);
            if (!empty($res)){
                die(json_encode($res));
            }
        }

        if (!empty($emptyRecipies)){
            die(json_encode(['result'=>false]));
    
        } else {
            die(json_encode(['result'=>true]));
    
        }
    }

    /**
     * poslani dat na cloud sklad
     */
    public function sendStocks(){
        $data = $this->getData();

        $this->getSettings();
        $emptyRecipies = [];

        if (!isset($data)){
            die(json_encode(['result'=>true,'message'=>'Chyba odeslani na sklad, nejsou data']));
        }

        foreach($data AS $k=>$post){
            $curlUrl =  API_URL.'api/receiveStocks/'.$this->settings->data->system_id;
            foreach($post AS $kp=>$p){
                $post[$kp]['stockList']['user_name'] = $this->loggedUser['name'];
                $post[$kp]['stockList']['user_id'] = $this->loggedUser['id'];
                $post[$kp]['stockList']['terminal_id'] = $this->loggedUser['terminal_id'];
                $post[$kp]['stockList']['system_id'] = $this->settings->data->system_id;
            }    

            $result = $this->conCurl($curlUrl,$post);
  
            $res = @json_decode($result,true);

            if ($res['result'] == false){
                die(json_encode($res));
            }
            //  pr($res);
            if (!empty($res['emptyRecepies'])){
                // pr('a');
                $emptyRecipies[] = $res['emptyRecepies'];
            }
            // pr($result);
        }
        // pr($emptyRecipies);
        if (!empty($emptyRecipies)){
            die(json_encode(['result'=>false,'message'=>'Není receptura: '.implode(',',$emptyRecipies)]));
    
        } else {
            die(json_encode(['result'=>true,'ids'=>$res['ids']]));
    
        }
    }
    
    /**
     * show nahled objednavky
     */
    public function show($id){
        $data = $this->Orders->find()
        ->contain(['OrderItems'])
        ->where(['id'=>$id])
        ->first();
        //pr($data);
        $print_data = $this->jsonToPrinter($data);
        $results = [
            'result'=>true,
            'print_type'=>'show',
            'print_data'=>$print_data,
        ];
        die(json_encode($results));
    }
    
    /**
     * print objednavky
     */
    public function printOrder($id,$result=true){
        $data = $this->Orders->find()
            ->contain(['OrderItems', 'EetStorno'])
            ->where(['Orders.id'=>$id])
            ->first();
        if($data && isset($data->eet_storno)){
            $data->eet_queue = $data->eet_storno;
            unset($data->eet_storno);
        }
            $print_data = $this->jsonToPrinter($data);
            $results = [
                'result'=>true,
                // 'print_type'=>'show',
                'print_data'=>$print_data,
            ];
        if (!$result){
            return $results;

        } else {
            die(json_encode($results));

        }
    }


    /**
     * kontrola zda neni bon pro stornovan v nejake uzaverce
     */
    private function checkStornoDeadlines($id){
        $this->loadModel('Deadlines');
        $deadline = $this->Deadlines->find()->where(['type_id'=>1])->select(['id','order_id_to'])->order('id DESC')->first();
        if ($deadline && $id < $deadline->order_id_to){
            die(json_encode(['result'=>false,'message'=>'Stornovat lze jen BON, který není ještě v uzávrce']));
        }
    }

    /**
     * storno objednavky
     */
    public function storno($id=null){
        $this->autoRender = false;
        $cloudUrl = API_URL . 'api/stornoOrder';
        if ($id > 0){
            $updateData = ['storno'=>1,'storno_user_id'=>$this->loggedUser['id']];
            
            if(isset($this->request->data['saveData'])){
                $saveData = $this->request->data['saveData'];
                if(isset($saveData['storno_reason']) && trim($saveData['storno_reason']) != ''){
                        $updateData['storno_reason'] = $saveData['storno_reason'];
                }
            } 

            //UPDATE LOCAL ORDER
            $resultDb = $this->Orders->updateAll(
                $updateData, // fields
                ['id' => $id]
            );

            //STORE LOCAL STOCK
            $this->loadModel('Stocks');
            $stockItems = $this->Stocks->find()->where([
                'order_id' => $id	//na lokalnim skladu neni local_id takze se musi vyhledavat podle order_id
            ])->toArray();
            if($stockItems){
                $this->Stocks->setAutoSend(false); //Neodesilat na cloud automaticky... je nasledne provedeno v ramci funkce 'api/stornoOrder'
                foreach($stockItems as $sItem){
                    unset($sItem->id);
                    $newStock = $this->Stocks->newEntity($sItem->toArray());
                    $newStock->storno_user_id = $this->loggedUser['id']; 
                    $newStock->storno_type = 1; 
                    $newStock->storno_reason = $saveData['storno_reason']; 
                    $newStock->storno_date = new Time(); 
                    $newStock->stock_type_id = 12;  //TYP STORNO
                    $saveStornoStock[] = $newStock; 
                    $this->Stocks->save($newStock); 
                } 
                $this->Stocks->setAutoSend(true);
            } 


            $order = $this->Orders->find()->where(['id'=>$id])->first();
          
            //PRVNI SE MUSI ODESLAT NA EET ABY PO ODESLANI NA SKLAD BYLO I EET ODESLANO
            $msg = 'Účtenka stornována úspěšně.';
            if($id){ 
                $this->loadModel('EetQueues'); 
                $order->eet_queue = $this->EetQueues->save($this->EetQueues->newEntity([
                    'order_id' => $id,
                    'price' => $order->total_price,
                    'storno' => 1,
                    'created' => date('Y-m-d H:i:s')
                ]));
                if($this->EetQueues->error){
                    $msg .= '<br/> EET chyba: '. $this->EetQueues->error;
                }
            }

            //SEND TO CLOUD 
            $cloudResult = $this->conCurl($cloudUrl, [
                'order_id' => $id, 
                'storno_fik'=> (isset($order->eet_queue->fik) ? $order->eet_queue->fik : null),  
                'storno_bkp'=> (isset($order->eet_queue->bkp) ? $order->eet_queue->bkp : null),
                'system_id' => $this->settings->data->system_id,
                'storno_user_id' => $this->loggedUser['id'], 
                'storno_type' => 1,
                'storno_reason' => $saveData['storno_reason']
            ]);
            $cloudResult = json_decode($cloudResult , true);
            if(isset($cloudResult['sended'])){
                $resultDb = $this->Orders->updateAll(
                    ['storno_sended' => new Time($cloudResult['sended'])], // fields
                    ['id' => $id]
                );
            }

            $printData = $this->printOrder($id,false);
            
            $results = [
                'result'=>true,
                'message'=>$msg,
                'data'=>$order,
                'print_data'=>$printData['print_data'],
            ];
        } else {
            $ids = $this->request->data;
            $indexList = [];
            foreach($this->request->data AS $d){
                $indexList[] = $d;
            }
            $ids = array_flip($ids);
            $resultDb = $this->Orders->updateAll(
                ['storno'=>1,'storno_user_id'=>$this->loggedUser['id'],'storno_type'=>1], // fields
                ['id IN' => $ids]
            );

            $this->conCurl($cloudUrl, [
                'order_ids' => $ids, 
                'system_id' => $this->settings->data->system_id,
                'storno_user_id' => $this->loggedUser['id'], 
                'storno_type' => 1 
            ]);


            $orders = $this->Orders->find()->where(['id IN'=>$ids])->toArray();
            foreach($ids AS $oid){
                $printData = $this->printOrder($oid,false);
            
            }
            $results = [
                'result'=>true,
                'message'=>'Účtenky stornovány',
                'indexList'=>$indexList,
                'printData'=>$printData,
            ];
        }
        
        die(json_encode($results));
    }
    
    /**
     * storno neupeceno objednavky
     */
    public function stornoNobake($id){
        $this->autoRender = false;
        $this->checkStornoDeadlines($id);
        $resultDb = $this->Orders->updateAll(
			['storno'=>1,'storno_type'=>2], // fields
			['id' => $id]
        );
        $order = $this->Orders->find()->where(['id'=>$id])->first();
        $results = [
            'result'=>true,
            'message'=>'Objednávka stornována jako neupečeno',
            'data'=>$order,
        ];
        die(json_encode($results));
    }

    /**
     * 
     */
    function tst(){
        $this->loadModel('ProductGroups');
        $data = $this->ProductGroups->find('threaded')->toArray();
        pr($data);
        die();
    }

    /**
     * editace
     */
    public function edit($id=null){
        $select_list = $this->getSelectList(false);
        $this->loadComponent('vI');

        // save data
        if (isset($this->request->data['saveData'])){ 

            $saveData = $this->request->data['saveData'];
            $saveData = $this->vI->convertTime($saveData);

            if (empty($saveData['billItems'])) {
                if ($saveData['close_order'] == 1){
                    die(json_encode(['result'=>true]));
                } else {
                    die(json_encode(['result'=>false,'message'=>'Nejsou vybrány produkty']));
                }
            }

            // load settings
            if (!isset($this->settings)){
                $this->getSettings();
            }
            $saveData['order_items'] = $saveData['billItems'];
            unset($saveData['billItems']);

            $stock_list = [ 
                'order_id'=>'',
                'data'=>[],
            ];

            if(isset($saveData['order_items'])){
                foreach($saveData['order_items'] AS $k=>$i){
                    $saveData['order_items'][$k]['system_id'] = $this->settings->data->system_id;
                    $saveData['order_items'][$k]['product_id'] = $saveData['order_items'][$k]['id'];
                    $saveData['order_items'][$k]['name'] = strtr($saveData['order_items'][$k]['name'],['ó'=>'o']);
                    if (isset($saveData['order_items'][$k]['addonAdd'])){
                        $saveData['order_items'][$k]['addonAdd'] = json_encode($saveData['order_items'][$k]['addonAdd']);
                    }
                    unset($saveData['order_items'][$k]['id']);
                    $saveData['order_items'][$k]['user_id'] = $saveData['order_items'][$k]['user_id']['id'];
                    $saveData['order_items'][$k]['createdTime'] = strtotime(date('Y-m-d H:i:s'));

                    // stock list
                    if (!isset($stock_list['data'][$saveData['order_items'][$k]['code']])){
                        $stock_list['data'][$saveData['order_items'][$k]['code']]['count'] = $saveData['order_items'][$k]['count'];
                        $stock_list['data'][$saveData['order_items'][$k]['code']]['product_group_id'] = $saveData['order_items'][$k]['product_group_id'];
                    } else {
                        $stock_list['data'][$saveData['order_items'][$k]['code']]['count'] += $saveData['order_items'][$k]['count'];
                
                    }
                }
            }

            // storno ulozeni
            if (isset($saveData['storno_list']) && !empty($saveData['storno_list'])){
                foreach($saveData['storno_list'] AS $k=>$i){
                    $i['storno'] = 1;
                    $i['storno_type'] = 2;
                    $i['storno_user_id'] = $saveData['storno_list'][$k]['user_id']['id'];
                    $i['system_id'] = $this->settings->data->system_id;
                    $i['product_id'] = $saveData['storno_list'][$k]['id'];
                    $i['name'] = strtr($saveData['storno_list'][$k]['name'],['ó'=>'o']);
                    if (isset($saveData['storno'][$k]['addonAdd'])){
                        $i['addonAdd'] = json_encode($saveData['storno_list'][$k]['addonAdd']);
                    }
                    unset($saveData['storno_liststorno'][$k]['id']);
                    $i['user_id']= $saveData['storno_list'][$k]['user_id']['id'];
                    unset($i['id']);
                    $saveData['order_items'][] = $i;
                }
            }

            $saveData['total_price'] = $this->getTotalPrice($saveData['order_items']);
            $saveData['total_price_without_tax'] = $this->getTotalPriceWithOutTax($saveData['order_items']);
            $saveData[$this->payment_list_save[$saveData['payment_id']]] = $saveData['total_price'];

            if ($saveData['table_type_id'] === 3) {
                $saveData['pay_company'] = $saveData['total_price'];
            } else {
                $saveData['pay_company'] = null;
            }

            if ($saveData['table_type_id'] === 2) {
                $saveData['pay_employee'] = $saveData['total_price'];
            } else {
                $saveData['pay_employee'] = null;
            }
            
            $saveData['createdTime'] = strtotime(date('Y-m-d H:i:s'));
            $saveData['system_id'] = $this->settings->data->system_id;
            $saveData['table_open'] = new Time($this->convertTime($saveData['table_open']));
            $saveData['table_close'] = new Time(date('Y-m-d H:i:s'));
            $saveData['name'] = '';
            $saveData['print_user_id'] = $saveData['user_id']['id'];
            $saveData['source_id'] = 1;
            
            if (!isset($this->settings)){
                //$this->getSettings();
            }
            $save_entity = $this->Orders->newEntity($saveData,[
                'associated' => [
                    'OrderItems' => ['validate' => false],
                ]
            ]);
      
            $this->vI->checkErrors($save_entity);

            $this->Orders->setAutoSend(false);

            if (!$resultDb = $this->Orders->save($save_entity)){
                $results = [
                    'result'=>false,
                    'message'=>__('Chyba uložení')
                ];    
            } else {
                $this->Orders->setAutoSend(true);
                $msg = null;

                if(!isset($saveData['id']) || !$saveData['id']){
                    
                    $this->loadModel('EetQueues');
                    $resultDb->eet_queue = $this->EetQueues->save($this->EetQueues->newEntity([
                        'order_id' => $save_entity->id,
                        'price' => $save_entity->total_price,
                        'storno'=> 0,
                        'created' => date('Y-m-d H:i:s')
                    ]));
                    if($this->EetQueues->error){
                        $msg = 'Uložení proběhlo úspěšně.<br/> EET chyba: '. $this->EetQueues->error;
                    }
                }
                
                if (isset($resultDb->eet_queue->fik, $resultDb->eet_queue->bkp)) {
                    $resultDb->fik = $resultDb->eet_queue->fik;
                    $resultDb->bkp = $resultDb->eet_queue->bkp;
                }
                //Nacteni nazvu stolu
                $this->loadModel("Tables");
                $table = $this->Tables->find()->select(["name", "table_type_id"])->where(["id" => $save_entity->table_id])->first();
                if ($table) {
                    $resultDb->table_name = $table->name;
                }
                $this->Orders->sendToCloud($resultDb);
                

                //Ulozeni do skladu
                $this->saveStock($resultDb);         
                $this->jsonToPrinter($resultDb);

                $results = [
                    'result'=>true,
                    'order_id'=>$resultDb->id,
                    'stock_list'=>$stock_list,
                    'sendOrders'=>$resultDb,
                    'message'=> $msg,
                    'data'=>$resultDb,
                    'print_data'=>$this->json_print_data,
                ];
                
                //Pro stoly zamestnancu a firemni tiskneme dve uctenky
                if($table){
                    if(in_array($table->table_type_id, [2,3])){
                        $results['secondPrint'] = true;
                    }
                }
                die(json_encode($results));
            }            

            $this->setJsonResponse($results);
        
        // load data
        } else {
            $defaultValues = [
                'source_id' => 1,
                'web_id'=>'',
                'client_addresses_id'=>'',
                'client_id'=>'',
                'note'=>'',
                'area_id'=>'',
                'send_time_id'=>'',
                'pay_points'=>0,
                'close_order'=>0,
                'payment_id'=>1,
                'storno'=>0,
                'operating'=>0,
                'later'=>0,
            ];
            if ($id != null){
                // pokud je edit
                if ($id != 'new'){
                    $conditions = ['id'=>$id];
                    $data = $this->Orders->find()
                    ->contain('Clients')
                    ->select()
                    ->where($conditions)
                    ->first()
                ;
                // pokud je nova polozka
                } else {
                    $data = $this->vI->emptyEntity('Orders',['belongsTo'=>'Clients']); 
                    
                }
            
                //pr($data);die();
                $this->loadModel('ProductGroups');
                $this->productGroups = $this->ProductGroups->loadGroupProducts();
                $this->loadModel('Tables');
                $this->tableGroups = $this->Tables->loadTableGroups();
                $this->loadModel('ProductConnects');
                $this->productConnects = $this->ProductConnects->connectList();
                $this->loadModel('Products');
                $this->productList = $this->Products->productList();
                $this->loadModel('ProductAddons');
                $this->addonList = $this->ProductAddons->addonList();
                //pr($this->productList);die();
                $data->client_addresses = '';
                $data = $this->vI->convertLoadData($data,$defaultValues);
                //pr($data);die();
                $validations = $this->vI->getValidations('Orders',['belongsTo'=>'Clients']);
            } else {

            }

            $results = [
                'result'=>true,
                'validations'=>(isset($validations)?$validations:null),
                'data'=>(isset($data)?$data:''),
                'others' => [
                    'table_groups'=>$this->tableGroups,
                    'tables_groups_parent'=>$this->tables_groups,
                    'product_groups'=>$this->productGroups,
                    'productConnects'=>$this->productConnects,
                    'product_list'=>$this->productList,
                    'addons_list'=>$this->addonList,
                    
                ],
                'select_list'=>$this->vI->SelectList($select_list),
                'select_list2'=>[
                    'price_tax_list_con'=>$this->price_tax_list_con,   
                    'table_list_all'=>$select_list['table_list_all'],
                ],
              
            ];  

            //pr($results);die();  
            $this->setJsonResponse($results);
        }
        $this->clearCache('orders_data');
    }

    /**
     * Ulozeni do lokalniho skladu - v modelu je automaticke odeslani na cloud
     */
    private function saveStock($orderData){

             // $product_codes = Hash::combine($orderData->order_items, '{n}.code', '{n}');
            $product_codes = [];
            if(isset($orderData->order_items) && !empty($orderData->order_items)){
                foreach($orderData->order_items as $oItem){
                    if($oItem->storno < 1){ //Vkladame pouze nestornovane polozky
                        $product_codes[$oItem->code] = $oItem;
                    }
                }
            } 
            $conditions = [ 'code IN'=> array_keys($product_codes) ];
        
            $this->loadModel('Products');
            $data = $this->Products->find()
                        ->contain([ 
                            'ProductRecipes' => function($q){
                                return $q->select([
                                    'id',
                                    'product_id',
                                    'stock_item_global_id',
                                    'value',
                                    'loss',
                                    'unit_id',
                                ]);
                            },
                        ])
                        ->where($conditions)
                        ->select([
                            'id',
                            'name',
                            'code',
                            'price',
                            'price_without_tax',
                            'tax_id',
                        ])
                        ->toArray();
            if (!$data){
                return false;
            }
            
            $saveStocks = [];
            $emptyRecepies = [];
            foreach($data AS $d){
                if (empty($d['product_recipes'])){
                    $emptyRecepies[] = $d->code .' - '.$d->name;
                }
                foreach($d['product_recipes'] AS $recepies){
                    $saveStocks[] = [
                        'order_id'=> $orderData->id,
                        'stock_item_id'=>$recepies->stock_item_global_id,
                        'stock_item_product_id'=>$recepies->product_id,
                        'value'=> $recepies->value * $product_codes[$d->code]->count,
                        'product_group_id'=> $product_codes[$d->code]->product_group_id,
                        'stock_type_id'=>4,
                        'system_id'=> $this->settings->data->system_id,
                        'tax_id'=> $d->tax_id,
                        'unit_id'=>$recepies->unit_id,
                        'user_name'=> $this->loggedUser['name'],
                        'user_id'=> $this->loggedUser['id'],
                        'terminal_id'=> $this->loggedUser['terminal_id'],
                        'createdTime'=>time(),
                    ];
                }
            }
            //pr($saveStocks);
            $this->loadModel('Stocks');
            $entities = $this->Stocks->newEntities($saveStocks);
            
            foreach($entities AS $entity){
                try { 
                    $this->Stocks->save($entity);
                } catch (\Exception $e) {
                    return false;
                    //die(json_encode(['result'=>false,'message'=>$e]));
                }
            }
    }

    /**
     * Returns stocks data to work with.
     * 
     * @since 1.71
     * @return void
     */
    public function getStocksData()
    {
        $this->loadModel('Products');
        $this->loadModel('ProductRecipes');
        $this->loadModel('Stocks');
        
        $products = $this->Products->find()
        ->select([
            'id'
        ])
        ->all();

        $data = [];

        foreach ($products as $pr) {
            $recipes = $this->ProductRecipes->find()
            ->select([
                'id',
                'product_id',
                'stock_item_global_id',
                'value'
            ])
            ->where(['product_id' => $pr->id])
            ->all();

            $pr_recipes = [];

            foreach ($recipes as $re) {
                $pr_recipes = $re->stock_item_global_id;
            }

            $stocks = $this->Stocks->find()
            ->select([
                'id',
                'stock_item_id',
                'stock_type_id',
                'nakup_price'
            ])
            ->where(['stock_type_id' => 1, 'stock_item_id IN' => $pr_recipes, 'nakup_price IS NOT NULL'])
            ->all();

            foreach ($stocks as $st) {
                $data[$pr->id] = [
                    'nakup_price'   => $st->nakup_price
                ];
            }
        }
        
        if (!empty($data)) {
            die(json_encode(['result' => true, 'stocksData' => $data]));
        } else {
            die(json_encode(['result' => false]));
        }
    }

    /**
     * 
     */
    private function convertTime($date){
        return date( "Y-m-d H:i:s", strtotime($date));
    }

    /**
     * 
     */
    public function loadTables(){
        
        $this->loadModel('Settings');
        $data = $this->Settings->find()
            ->select(['data'])
            ->where(['id'=>2])
            ->first()
        ;
        $data = json_decode($data->data);
        if ($data->billItems && !empty($data->billItems)){
            die(json_encode(['result'=>true,'billItems'=>$data->billItems,'tableData'=>$data->tableData]));

        } else {
            die(json_encode(['result'=>false]));

        }
    }

    /**
     * 
     */
    public function closeTables(){
        if (!empty($this->request->data)){
            $this->loadModel('Settings');
            $resultDb = $this->Settings->updateAll(
                ['data'=>json_encode($this->request->data)], // fields
                ['id' => 2]
            );
            die(json_encode(['result'=>true]));
        } else {
            
            die(json_encode(['result'=>true]));
        }
    }

    	
    /**
     * 
     */
	public function price($price, $mena_suf = array()){
		$symbol_before = '';
		$kurz = null;
		$count = 1;
		$decimal = 2;
		$symbol_after = '';
		extract($mena_suf);
		
    	$white = array(" " => "");
		if ($kurz!=null)
			$price = ($price/strtr($kurz,',','.'))*$count;
		if ($price != null)
			return $symbol_before.number_format(strtr($price, $white), $decimal, '.', '').$symbol_after;        //NESMI CHODIT MEZERA V DECIMAL PROTOZE JS TO VYHODNOCUJE JAKO STRING!!!
		else
			return $symbol_before.number_format(strtr(0.00,$white), $decimal, '.', '').$symbol_after;	  //NESMI CHODIT MEZERA V DECIMAL PROTOZE JS TO VYHODNOCUJE JAKO STRING!!!
	}

    /**
     * get total price
     */
    private function getTotalPrice($items){
        $total_price = 0;
        //pr($items);
        foreach($items AS $item){
            if (!isset($item['storno']) || $item['storno'] == 0){
                $total_price += $item['price']*$item['count'];
       
            }
        }
        return $total_price;
    }

    /**
     * get total price
     */
    private function getTotalPriceWithOutTax($items){
        $total_price = 0;
        foreach($items AS $item){
            $total_price += $item['price_without_tax'] * $item['count'];
        }
        return $total_price;
    }

    /**
     * nacteni klienta dle telefoniho cisla
     */
    public function loadClient($phonePrefix,$phone,$client_id=null){
        $this->loadModel('Clients');
        $data = $this->Clients->getClient($phonePrefix,$phone,$client_id);
        if ($data){
            $results = [
                'result'=>true,
                'data'=>$data,
            ];
        } else {
            $results = [
                'result'=>false,
                'data'=>[
                    'phone_pref'=>$phonePrefix,
                    'phone'=>$phone,
                ],
            ];
        }
        
        $this->setJsonResponse($results);
    }

    /**
     * nacteni klienta dle telefoniho cisla
     */
    public function autocomplete($type,$value){
        $this->loadModel('Clients');
        $data = $this->Clients->getClientAutocomplete($type,$value);
        
        if ($data){
            $results = [
                'result'=>true,
                'type'=>$type,
                'data'=>$data,
            ];
        } else {
            $results = [
                'result'=>false,
                
            ];
        }
        
        $this->setJsonResponse($results);
    }

    public function saveUser()
    {
		$save = $this->Users->newEntity([
            'name'=>'test',
            'username'=>'test',
            'password'=>'testt',
        ]);
        $result = $this->Users->save($save);
        die('save user');
    }

    /**
     * get printer from cookue
     */
    private function getPrinters(){
		/*
		// get printer list from cookie
		if (isset($_COOKIE['printers_setting'])){
			$printer_list = json_decode($_COOKIE['printers_setting'],true);
			$this->printer_list = $printer_list = json_decode($printer_list['data'],true);
		}
		
		// get default printer from cookie
		if (isset($_COOKIE['printer_default'])){
			$printer_default = json_decode($_COOKIE['printer_default'],true);
			$this->printer_default = $printer_default = json_decode($printer_default['data'],true);
        }
        */
        //pr($this->settings['printer']);
    }
    
    
    /**
     * send client end to socket
     */
    public function sendClientEnd(){
        die('end');
    }
    
    /**
     * send client call to socket
     */
    public function sendClientCall(){
        die('call');
    }

    /**
     * send client hang to socket
     */
    public function sendClientHang($phoneAdd=null){
        
        if (isset($_GET["phoneNumber"]))
			$phone = $_GET["phoneNumber"];
		else 
			$phone = (isset($_POST["phoneNumber"])?$_POST["phoneNumber"]:'');
		
		$phone = trim($phone);
        if ($phoneAdd){
            $phone = $phoneAdd;
        }
        
        $this->autoRender = false;
        $this->pokladna_url = 'tcp://'.$_SERVER['SERVER_NAME'];		

        $local = $this->pokladna_url.'/api/hang/';
        
        $setting = array(
            'host'=>$this->pokladna_url,
            'port'=>8080,
            'local'=>$local,
        );
        $message = $phone;
        
        $this->loadComponent('WebsocketClient');
        $result = $this->WebsocketClient->sendCall($setting,$message,'hang');
    }
   

    public function saveDistributor(){
        $this->autoRender = false;
        if (empty($this->request->data)){
            die(json_encode(['result'=>false,'message'=>'Nejsou data']));
        } else {
            // pr($this->request->data);
            $this->loadModel('Deliverys');
            $find = $this->Deliverys->getDistributor($this->request->data['distributor_id']);
            if ($find){
                
                $this->request->data['distributor_id'] = $find->id;
                $this->loadModel('Deadlines');
                $findOrder = $this->Deadlines->getDistributorOrders($this->request->data['distributor_id'],$this->request->data['id']);
                if (!$findOrder){
                    die(json_encode(['result'=>false,'message'=>'Nelze uložit rozvoce, již máte uloženou uzávěrku pro tento BON']));
            
                } else {
                            
                    $save = $this->Orders->newEntity($this->request->data,['validate'=>false]);
                    //pr($save);die();
                    if (!$result = $this->Orders->save($save)){
                        die(json_encode(['result'=>false,'message'=>'Chyba uložení rozvozce']));
                    } else {
                        $this->sendCloudOrder($result->id,$this->request->data['distributor_id']);
                        die(json_encode(['result'=>true]));
                    }
                }
            } else { 
                die(json_encode(['result'=>false,'message'=>'Nelze uložit rozvoce']));
                
            }
        }
    }

    /**
     * odeslani objedavky do cloud pro trackovani
     */
    public function sendCloudOrder($id,$distributor_id){
        $this->getSettings();
        $this->autoRender = false;
		$this->settings->data->domain = strtr($this->settings->data->domain,['http'=>'https']);
        $find_order = $this->Orders->find()
            ->contain(['OrderItems'])
            ->select([])
            ->where(['id'=>$id])
            ->first();
        
        $products = array();
        
        if (!empty($find_order->order_items))
			foreach($find_order->order_items AS $pro){
				$products[] = array(
					'code'=>$pro->code,
					'name'=>$pro->name,
					'price_vat_per_item'=>$pro->price,
					'count'=>$pro->count,
				);
			}
			$order_data = array(
				'order_id'=>$find_order->id,
				'doba_id'=>$find_order->send_time_id,
				'order_web_id'=>$find_order->web_id,
				'shop_provoz_id'=>$this->settings->data->system_id,
				'zdroj_id'=>$find_order->source_id,
				'price_vat'=>$find_order->total_price,
				'created_pokladna'=>$find_order->created->format('Y-m-d H:i:s' ),
				'created_time'=>strtotime($find_order->created->format('Y-m-d H:i:s')),
				'shop_payment_id'=>$find_order->payment_id,
				'note'=>$find_order->note,
				'rozvozce_id'=>$distributor_id,
		    );
            //pr($order_data);die();
        $ch = curl_init();
		$post = array(
			'order_data'=>$order_data,
			'order_id'=>$find_order->id,
			'order_web_id'=>$find_order->web_id,
			'rozvozce_id'=>$distributor_id,
			'pokladna_id'=>$this->settings->data->system_id,
		);
        curl_setopt($ch, CURLOPT_URL, $this->settings->data->domain.'/rozvozce_add/');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
        curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
	
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec ($ch);
        //pr($result);
        $result = (json_decode($result,true));
        //pr($result);
        if ($result['r'] != true){
            die(json_encode(array('result'=>false,'message'=>'Chyba uložení do aplikace')));
        }
        curl_close ($ch);
    }

    /**
     * get OrderList
     */
    public function getOrdersList(){
        $OrdersList = $this->Orders->getOrdersList();
        if ($OrdersList){
            foreach($OrdersList AS $k=>$data){
                $this->escPosSetting = ['is_logo'=>false];
                $this->jsonToPrinter($data);
                $OrdersList[$k]['print_data'] = $this->json_print_data;
            }
        }

        if (isset($this->request->query['debug'])){
            pr($OrdersList);die();
        }

        if ($OrdersList){
            die(json_encode(['result'=>true,'data'=>$OrdersList]));
        } else {
            die(json_encode(['result'=>false,'message'=>__('Nenalezeny zadne objednavky')])); 
        }
    }

    /**
     * 
     */
    function price_format($price){
        return number_format($price, 2, '.', '');
    }

    /**
     * 
     */
    private function formatPhone($num){
        if (substr($num,0,2) == '42'){
            
            $result = substr($num,0,3)." ".substr($num,3,3)." ".substr($num,6,3)." ".substr($num,9,3);
            
        } else {
            $result = substr($num,0,3)." ".substr($num,3,3)." ".substr($num,6);
            
        }
        return $result;
    }

    /**
     * print json data
     */
    public function jsonToPrinter($data=null,$part_print=false){
  
        if (!isset($this->settings)){
            $this->getSettings();
        }
        $this->loadModel('Settings');
        $this->transfer_list = $this->Settings->operationsList(true);

        if (empty($this->settings)){
            die(json_encode(['result'=>false, 'message'=>'Neni definovano nastaveni']));
        }
        $this->autoRender = false;
        if ($data == 'null') $data = null;
        if ($data == null){
            $showResult = true;
            $data = $this->Orders->find()
                                ->contain(['OrderItems', 'EetQueues'])
                                ->where(['id'=>149359])
                                ->first();
        }

        $table_data = [];
        $total_price = 0;
        
        $dphSum = [
            1=>['value1'=>0,'value2'=>0], //15%',
            2=>['value1'=>0,'value2'=>0], //21%',
            3=>['value1'=>0,'value2'=>0], //0%',
            4=>['value1'=>0,'value2'=>0], //10%',
        ];

        $colWidth = [
            1=>17,
            2=>3,
            3=>4,
            4=>8,
            5=>8,
        ];
        $format = '%-'.$colWidth[1].'s %'.$colWidth[2].'.0f %'.$colWidth[3].'.0f%% %'.$colWidth[4].'.2f %'.$colWidth[5].'.2f';
        $format2 = '%-'.$colWidth[1].'s %'.$colWidth[2].'.0s %'.$colWidth[3].'.0s  %'.$colWidth[4].'.2s %'.$colWidth[5].'.2f';
        $formatHeader = '%-'.$colWidth[1].'s %'.$colWidth[2].'s %'.$colWidth[3].'.s  %'.$colWidth[4].'s %'.$colWidth[5].'s';

        foreach($data['order_items'] AS $item){
            if ($item['storno'] == 1){
                continue;
            }
            $total_price += $item['price']*$item['count'];
            
            $pref = '';

            $price = $item['price'] * $item['count'];
            if($item['tax_id'] && isset($this->price_tax_list_con[$item['tax_id']])){
                $dphSum[$item['tax_id']]['value1'] += $price * $this->price_tax_list_con[$item['tax_id']];
                $dphSum[$item['tax_id']]['value2'] += $price - $price * $this->price_tax_list_con[$item['tax_id']];
            }

            $table_data[] = [
                'format'=>'',
                'formatTable'=>$format,
                'data'=>[
                    //'Položka'=> $item['num'], 
                    'Položka'=> $pref.mb_substr(strtr($item['name'],[]),0,14), 
                    'Mn.'=> $item['count'], 
                    'DPH'=> ($item['tax_id'] && isset($this->price_tax_list[$item['tax_id']]) ? $this->price_tax_list[$item['tax_id']] : ''),
                    'Kč/jed.'=>$this->price_format($item['price']),
                    'Cena'=>$this->price_format($item['price'] * $item['count']), 
                ],
            ];
        }
        $table_data[] = [
            'format'=>'',
            'data' => [
                'Položka'=>' ', 
                'Mn.'=>' ', 
                'DPH'=> ' ',
                'Kč/jed.'=> ' ',
                'Cena'=>' ',
            ]
        ];

        // pr($dphSum);die();
        $table_data[] = [
            'font'=>'B', 
            'format'=>'bold',  
            'formatTable'=>$format2,
            'data'=>[
                'Položka'=>'Celkem s DPH', 
                'Mn.'=>' ', 
                'DPH'=> ' ',
                'Kč/jed.'=> ' ',
                'Cena'=>$this->price_format($total_price),
            ],
        ];
        
        $table_data[] = [
            'format'=>'',
            'formatTable'=>$formatHeader,
            'data' => [
                'Položka'=>' ', 
                'Mn.'=>' ', 
                'DPH'=> ' ',
                'Kč/jed.'=> ' ',
                'Cena'=>' ',
            ]
        ];
        

        $table_data[] = [
            'format'=>'',
            'formatTable'=>$formatHeader,
            'data' => [
                'Položka'=>'Platba: '. $this->payment_list[$data['payment_id']] , 
                'Mn.'=>' ', 
                'DPH'=> ' ',
                'Kč/jed.'=> ' ',
                // 'Cena'=> $this->price_format($total_price)
                'Cena'=> ' '
            ]
        ];
        
        $table_data[] = [
            'format'=>'',
            'formatTable'=>$formatHeader,
            'data' => [
                'Položka'=>' ', 
                'Mn.'=>' ', 
                'DPH'=> ' ',
                'Kč/jed.'=> ' ',
                'Cena'=>' ',
            ]
        ];
        

        foreach($dphSum AS $tax_id=>$tax){
            if ($tax['value1'] > 0){
                $table_data[] = [
                    'format'=>'',
                    'formatTable'=>$format2,
                    'data' => [
                        'Položka'=>'DPH '.$this->price_tax_list[$tax_id], 
                        'Mn.'=>' ', 
                        'DPH'=> ' ',
                        'Kč/jed.'=> ' ',
                        'Cena'=>round($tax['value1'],2),
                    ]
                ];

                $table_data[] = [
                    'format'=>'',
                    'formatTable'=>$format2,
                    'data' => [
                        'Položka'=>'Zaklad DPH '.$this->price_tax_list[$tax_id], 
                        'Mn.'=>' ', 
                        'DPH'=> ' ',
                        'Kč/jed.'=> ' ',
                        'Cena'=>round($tax['value2'],2),
                    ]
                ];
            }
        }
       
        //pr($table_data);die();

        $path_logo_domain = 'http://localhost'.(($_SERVER['SERVER_PORT']>100)?':'.$_SERVER['SERVER_PORT']:'');
        if (strpos($_SERVER['SCRIPT_NAME'],'/src/') !== false){
            $path_logo = '/src/cake/webroot/uploaded/';
        } else {
            $path_logo = '/cake/webroot/uploaded/';
        }
        
        
       
        //pr($table_data);die();
        // gemerovani tabulky array to string
        $table_data = $this->tableGen($table_data,$formatHeader);
        // pr($table_data);die();

        $this->loadModel('Tables');
        $this->table_list = $this->Tables->tableList();

        $printSetting = [];
        if(isset($this->settings['data'])){
            $printSetting = (array) $this->settings['data'];
        }
        $json_print_data = [
            'printer_name'=>$this->settings['printer'],
            'is_logo'=>true,
            'path_logo' => $path_logo_domain . $path_logo . (isset($printSetting['print_logo']) && $printSetting['print_logo'] != '' ? $printSetting['print_logo'] : 'logo_uctenka.png'),
            'header'=>[
                
                [
                    'font'=>'A',
                    'align'=>'center',
                    'format'=>'',
                    'data'=> (isset($printSetting['print_heading']) && $printSetting['print_heading'] != '' ? $printSetting['print_heading'] : 'Cukrárna OLLIES')
                ],
                [
                    'font'=>'A',
                    'align'=>'center',
                    'format'=>'',
                    'data'=> (isset($printSetting['print_branch']) && $printSetting['print_branch'] != '' ? $printSetting['print_branch'] : 'Ollies dorty s.r.o.')
                ],
                [
                    'font'=>'A',
                    'align'=>'center',
                    'format'=>'',
                    'data'=> (isset($printSetting['print_street']) && $printSetting['print_street'] != '' ? $printSetting['print_street'] : 'Výstavní 2968/108')
                ],
                [
                    'font'=>'A',
                    'align'=>'center',
                    'format'=>'',
                    'data'=>(isset($printSetting['print_city']) && $printSetting['print_city'] != '' ? $printSetting['print_city'] : 'Ostrava-Vítkovice, 703 00')
                ],
                [
                    'font'=>'A',
                    'align'=>'center',
                    'format'=>'',
                    'data'=> 'IČ: '. (isset($printSetting['print_ic']) && $printSetting['print_ic'] != '' ? $printSetting['print_ic'] : '28596030') . ' DIČ: '. (isset($printSetting['print_dic']) && $printSetting['print_dic'] != '' ? $printSetting['print_dic'] : 'CZ28596030')
                ],
                [
                    'font'=>'A',
                    'align'=>'center',
                    'format'=>'',
                    'data'=>'-----------------------------------',
                    
                ],
                [
                    'font'=>'A',
                    'align'=>'center',
                    'format'=>'',
                    'data'=>'OBJEDNÁVKOVÝ TELEFON: 603 424 268'
                ],
                [
                    'font'=>'A',
                    'align'=>'center',
                    'format'=>'',
                    'data'=>'OBJEDNÁVKY ONLINE: www.ollies.cz'
                ],
                [
                    'font'=>'A',
                    'align'=>'center',
                    'format'=>'',
                    'data'=>' '
                ],
                [
                    'font'=>'B',
                    'align'=>'left',
                    'format'=>'bold',
                    'data'=>'Název: '. (isset( $this->table_list[$data->table_id]) ? $this->table_list[$data->table_id] : ''), // TODO logged user name
                ],
                [
                    'font'=>'A',
                    'align'=>'center',
                    'format'=>'',
                    'data'=>' '
                ],
                [
                    'font'=>'A',
                    'align'=>'left',
                    'format'=>'',
                    'data'=>'Daňový doklad: '.$data->id //TODO var sym of order
                ],
                [
                    'font'=>'A',
                    'align'=>'right',
                    'format'=>'',
                    'data'=>'' . date('d.m.Y H:i:s')
                ],
                [
                    'font'=>'A',
                    'align'=>'left',
                    'format'=>'',
                    'data'=>'Tisk provedl: '.$this->loggedUser['name'] // TODO logged user name ?
                ],
                /*[
                    'font'=>'A',
                    'align'=>'left',
                    'format'=>'',
                    'data'=>'Placeno:||'.$this->payment_list[$data['payment_id']],
                ],*/
                /*[
                    'font'=>'A',
                    'align'=>'left',
                    'format'=>'',
                    'data'=>'----------------------------------------------',
                    
                ],*/
            ],
            'footer'=>[
                
                [
                    'align'=>'left',
                    'format'=>'normal',
                    'data'=> ' ',
                ],
                [
                    'font'=>'A',
                    'align'=>'center',
                    'format'=>'',
                    'data'=>'Děkujeme za návštěvu'
                ],
                [
                    'align'=>'left',
                    'format'=>'normal',
                    'data'=> ' ',
                ],
                [
                    'font'=>'A',
                    'align'=>'center',
                    'format'=>'',
                    'data'=>'a'
                ],
                [
                    'align'=>'left',
                    'format'=>'normal',
                    'data'=> ' ',
                ],
                [
                    'font'=>'A',
                    'align'=>'center',
                    'format'=>'',
                    'data'=>'Mějte sladký život'
                ],
                [
                    'align'=>'left',
                    'format'=>'normal',
                    'data'=> ' ',
                ],
                [
                    'font'=>'A',
                    'align'=>'center',
                    'format'=>'',
                    'data'=>'Všechny nepečené výrobky jsou určeny k okamžité spotřebě, '
                ],
                [
                    'font'=>'A',
                    'align'=>'center',
                    'format'=>'',
                    'data'=>' uchovejte při teplotě +4°C do +8°C. '
                ],
                [
                    'align'=>'left',
                    'format'=>'normal',
                    'data'=> ' ',
                ],
              
                [
                    'align'=>'left',
                    'format'=>'normal',
                    'data'=> ' ',
                ],
                [
                    'font'=>'A',
                    'align'=>'center',
                    'format'=>'',
                    'data'=>'Všechny pečené výrobky (Záviny, Muffiny, Koláče, Ricotty, Jahelníky, Sacher, Brownies) jsou '
                ],
                [
                    'font'=>'A',
                    'align'=>'center',
                    'format'=>'',
                    'data'=>'určeny k okamžité spotřebě,  '
                ],
                [
                    'font'=>'A',
                    'align'=>'center',
                    'format'=>'',
                    'data'=>'uchovejte při teplotě +4°C do +15°C.   '
                ],
               
                [
                    'align'=>'left',
                    'format'=>'normal',
                    'data'=> ' ',
                ],
                [
                    'font'=>'A',
                    'align'=>'center',
                    'format'=>'',
                    'data'=>'Zmrzlina je určena k okamžité spotřebě, '
                ],
                [
                    'font'=>'A',
                    'align'=>'center',
                    'format'=>'',
                    'data'=>'uchovávejte při teplotě -17°C až -19°C. '
                ],
                [
                    'align'=>'left',
                    'format'=>'normal',
                    'data'=> ' ',
                ],
                [
                    'font'=>'A',
                    'align'=>'center',
                    'format'=>'',
                    'data'=>'*Tímto symbolem jsou označeny pečené výrobky.'
                ],
                [
                    'align'=>'left',
                    'format'=>'normal',
                    'data'=> ' ',
                ],
                [
                    'font'=>'B',
                    'align'=>'center',
                    'format'=>'',
                    'data'=>'JAK SE VÁM LÍBILO?'
                ],
                [
                    'align'=>'left',
                    'format'=>'normal',
                    'data'=> ' ',
                ],
                [
                    'font'=>'A',
                    'align'=>'center',
                    'format'=>'',
                    'data'=>'Podělte se s ostatními a napište nám recenzi na Google.'
                ],
                /**/
            ],
            'table_data'=>$table_data,
            'table_align'=>[
                'Položka'=>'Zaklad DPH '.$this->price_tax_list[$tax_id], 
                'Mn.'=>'left', 
                'DPH'=> 'right',
                'Kč/jed.'=> 'right',
                'Cena'=>'right',
            ],
        ];
        if(isset($data->eet_queue) && $data->eet_queue){

            array_unshift($json_print_data['footer'], [
                'font'=>'A',
                'align'=>'center',
                'format'=>'',
                'data'=> 'BKP: ' . $data->eet_queue->bkp
            ]);          
            array_unshift($json_print_data['footer'], [
                'font'=>'A',
                'align'=>'center',
                'format'=>'',
                'data'=> 'FIK: ' . $data->eet_queue->fik
            ]);
            array_unshift($json_print_data['footer'] , [  
                'align'=>'left',
                'format'=>'normal',
                'data'=> ' ',
            ]);
        }
        if(isset($data->storno, $data->storno_reason, $data->storno_user_id) && $data->storno == 1){
            $json_print_data['header'][] = [
                'font'=>'A',
                'align'=>'left',
                'format'=>'',
                'data'=> 'Stornováno (' . $data->storno_user_id . '): ' . $data->storno_reason 
            ];
        }

        $json_print_data['header'][] = [
            'font'=>'A',
            'align'=>'center',
            'format'=>'',
            'data'=>'================================================',
        ];
        
        // parsovani hlavicka pokud je na 2 radky
        if (strpos($this->settings->data->print_top,'<br>') == true){
            $top = explode('<br>',$this->settings->data->print_top);
            $top = array_reverse($top);
            foreach($top AS $k=>$t){
                $topArray = [
                    'font'=>'A',
                    'align'=>'center',
                    'format'=>'',
                    'data'=>$t,
                ];
                array_unshift($json_print_data['header'],$topArray);
            }
        } else {
            $topArray = [
                'font'=>'A',
                'align'=>'center',
                'format'=>'',
                'data'=>$this->settings->data->print_top,
            ];
            array_unshift($json_print_data['header'],$topArray);
        
        }

        // parsovani hlavicka pokud je na 2 radky
        if (strpos($this->settings->data->print_bottom,'<br>') == true){
            $foot = explode('<br>',$this->settings->data->print_bottom);
            foreach($foot AS $k=>$t){
                $footArray = [
                    'font'=>'A',
                    'align'=>'center',
                    'format'=>'',
                    'data'=>$t,
                ];
                array_push($json_print_data['footer'],$footArray);
            }
        } else {
            $footArray = [
                'font'=>'A',
                'align'=>'center',
                'format'=>'',
                'data'=>$this->settings->data->print_bottom,
            ];
            array_push($json_print_data['footer'],$footArray);
        
        }

        @file_put_contents( ROOT . '/tmp/logs/receipts.log', date('Y-m-d H:i:s') . ' - ' .  json_encode($json_print_data) . PHP_EOL, FILE_APPEND );

        if (isset($this->request->query['debug'])){
            pr($json_print_data);
            die();
        }else if (isset($this->request->query['json']) || isset($showResult)){
            //$convert = iconv("UTF-8", "ISO-8859-1", json_encode($json_print_data));
            //pr($convert);
            // die(json_encode(['result'=>true,'data'=>json_decode($convert)]));
            die(json_encode(['result'=>true,'data'=>$json_print_data]));
            die();
        } else {
            //pr($json_print_data);
            $this->json_print_data = $json_print_data;
            return $json_print_data; 
        }
    }

    /**
     * print json data
     */
    public function jsonToPrinter2($data=null,$part_print=false){
            if (!isset($this->settings)){
            $this->getSettings();
            }
            $this->transfer_list = $this->Settings->operationsList(true);

            if (empty($this->settings)){
                die(json_encode(['result'=>false,'message'=>'Neni definovano nastaveni']));
            }
            $this->autoRender = false;
            if ($data == 'null') $data = null;
            if ($data == null){
                $showResult = true;
                $data = $this->Orders->find()
                            ->contain(['OrderItems'])
                            ->where(['id'=>149359])
                            ->first();
            }
            $table_data = [];
            $total_price = 0;
            
            foreach($data['order_items'] AS $item){
                $total_price += $item['price']*$item['count'];
                
                $pref = '';
                if($item['addon'] == 1) $pref = '+ ';
                if($item['addon'] == 2) $pref = '- ';
                $item['addonAdd'] = json_decode($item['addonAdd'],true);

                //pr($item);
                $table_data[] = [
                    'format'=>'',
                    'data'=>[
                        'Kod'=> $item['num'], 
                        'Produkt'=> $pref.mb_substr($item['name'],0,18), 
                        'Ks'=> $item['count'], 
                        'Cena'=>$this->price_format($item['price']),
                        //'Celkem'=>$this->price_format($item['price']*$item['count']), 
                    ],
                ];

                if (!empty($item['addonAdd'])){
                    foreach($item['addonAdd'] AS $ad){
                        $table_data[] = [
                            'format'=>'',
                            'data'=>[
                                'Kod'=> ' ', 
                                'Produkt'=> '+ '.mb_substr($ad['name'],0,18), 
                                'Ks'=> ' ', 
                                'Cena'=>$this->price_format($ad['price']),
                                //'Celkem'=>$this->price_format($item['price']*$item['count']), 
                            ],
                        ];
                    }
                }
            }
            $table_data[] = [
                'format'=>'bold',  
                'data'=>[
                    'Kod'=>'Celkem', 
                    'Produkt'=>' ', 
                    'Ks'=> ' ',
                    'Cena'=>$this->price_format($total_price),
                ],
            ];
            $path_logo_domain = 'http://localhost'.(($_SERVER['SERVER_PORT']>100)?':'.$_SERVER['SERVER_PORT']:'');
            if (strpos($_SERVER['SCRIPT_NAME'],'/src/') !== false){
                $path_logo = '/src/cake/webroot/uploaded/';
            } else {
                $path_logo = '/cake/webroot/uploaded/';
            }

            $json_print_data = [
                'printer_name'=>$this->settings['printer'],
                // 'is_logo'=>(isset($this->escPosSetting['is_logo'])?$this->escPosSetting['is_logo']:true),
                'is_logo'=>true,
                'path_logo' => $path_logo_domain.$path_logo.'logo_uctenka.png',
                'header'=>[
                    // [
                    //     'font'=>'A',
                    //     'align'=>'center',
                    //     'format'=>'',
                    //     'data'=>$this->settings->data->print_top,
                    // ],
                    [
                        'font'=>'A',
                        'align'=>'left',
                        'format'=>'',
                        'data'=>' '
                    ],
                    [
                        'font'=>'A',
                        'align'=>'left',
                        'format'=>'',
                        'data'=>'Placeno:||'.$this->payment_list[$data['payment_id']],
                    ],
                    [
                        'font'=>'A',
                        'align'=>'left',
                        'format'=>'',
                        'data'=>'-----------------------------------------',
                    ],
                    
                ],
                'footer'=>[
                    [
                        'align'=>'left',
                        'format'=>'normal',
                        'data'=> ' ',
                    ],
                ],
                'table_data'=>$table_data,
                'table_align'=>[
                    'Produkt'=>'left',
                    'Ks'=>'right',
                    'Cena'=>'right',	
                ],
            ];
            // parsovani hlavicka pokud je na 2 radky
            if (strpos($this->settings->data->print_top,'<br>') == true){
                $top = explode('<br>',$this->settings->data->print_top);
                $top = array_reverse($top);
                foreach($top AS $k=>$t){
                    $topArray = [
                        'font'=>'A',
                        'align'=>'center',
                        'format'=>'',
                        'data'=>$t,
                    ];
                    array_unshift($json_print_data['header'],$topArray);
                }
            } else {
                $topArray = [
                    'font'=>'A',
                    'align'=>'center',
                    'format'=>'',
                    'data'=>$this->settings->data->print_top,
                ];
                array_unshift($json_print_data['header'],$topArray);
            
            }

            // parsovani hlavicka pokud je na 2 radky
            if (strpos($this->settings->data->print_bottom,'<br>') == true){
                $foot = explode('<br>',$this->settings->data->print_bottom);
                foreach($foot AS $k=>$t){
                    $footArray = [
                        'font'=>'A',
                        'align'=>'center',
                        'format'=>'',
                        'data'=>$t,
                    ];
                    array_push($json_print_data['footer'],$footArray);
                }
            } else {
                $footArray = [
                    'font'=>'A',
                    'align'=>'center',
                    'format'=>'',
                    'data'=>$this->settings->data->print_bottom,
                ];
                array_push($json_print_data['footer'],$footArray);
            
            }

            if (isset($this->request->query['debug'])){
                pr($json_print_data);
                die();
            }else if (isset($this->request->query['json']) || isset($showResult)){
                //$convert = iconv("UTF-8", "ISO-8859-1", json_encode($json_print_data));
                //pr($convert);
                // die(json_encode(['result'=>true,'data'=>json_decode($convert)]));
                die(json_encode(['result'=>true,'data'=>$json_print_data]));
                die();
            } else {
                //pr($json_print_data);
                $this->json_print_data = $json_print_data;
                return $json_print_data;
                
            }
        
    }


    public function sendToCloud(){
        $this->loadModel('Deadlines');
        $deadline = $this->Deadlines->find()
            ->select(['id','order_id_from','order_id_to'])
            ->where(['type_id'=>1])
            ->order('id DESC')
            ->first();    
        ;
        if (!$deadline){
            die(json_encode($result = ['result'=>false,'message'=>'Není žádná uzávěrka']));
        }
        //pr($deadline);

        $conditions = [
            'id > '=>$deadline->order_id_from,
            'id <= '=>$deadline->order_id_to,
        ];
        $query = $this->Orders->find()
            ->contain('OrderItems')
            ->select()
            ->where($conditions) 
            ->hydrate(false)   
        ;
        $data = $query->toArray();
        if (!$data){
            
            die(json_encode($result = ['result'=>false,'message'=>'Nejsou žádné objednávky']));
        }
        //pr($data);

        foreach($data AS $k=>$d){
            
            $data[$k]['pokladna_id'] = $data[$k]['id'];
            $data[$k]['created'] = $data[$k]['created']->format('Y-m-d H:i:s');
            $data[$k]['modified'] = $data[$k]['modified']->format('Y-m-d H:i:s');
            unset($data[$k]['id']);
            
            if (isset($d['order_items'])){
                foreach($d['order_items'] AS $ok=>$oi){
                    
                    $data[$k]['order_items'][$ok]['created'] = $data[$k]['order_items'][$ok]['created']->format('Y-m-d H:i:s');
                    $data[$k]['order_items'][$ok]['modified'] = $data[$k]['order_items'][$ok]['modified']->format('Y-m-d H:i:s');
                    $data[$k]['order_items'][$ok]['pokladna_id'] = $data[$k]['order_items'][$ok]['id'];
                    $data[$k]['order_items'][$ok]['pokladna_order_id'] = $data[$k]['order_items'][$ok]['order_id'];
                    unset($data[$k]['order_items'][$ok]['id']);
                    unset($data[$k]['order_items'][$ok]['order_id']);
                }
            }
        }


        $this->loadModel('ProductGroups');
        $product_groups = $this->ProductGroups->groupList();
        $data['product_groups'] = $product_groups;
        //pr($product_groups);die();
        
        
        $ch = curl_init();
        $post = $data;
        //pr($post);
		$curlUrl = API_URL.'api/orders/save/';
        curl_setopt($ch, CURLOPT_URL, $curlUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec ($ch);
		//pr('curl result');
		if (empty($result)){
			$result = [
				'result'=>false,
				'message'=>'Není spojení se serverem pro uložení objednávek, zkontrolujte připojení k internetu a dostupnost www.chachar.cz',
			];
		} else {
            //pr($result);
			$result = json_decode($result,true);
            if (json_encode($result) == null){
                pr($result);
            }
        }
        die(json_encode($result));
    }

    /**
     * 
     */
    public function changeOperation($order_id,$current,$new){
        
        $ch = curl_init();
        $post = [];
        //pr($post);
        // load settings
        if (!isset($this->settings)){
            $this->getSettings();
        }
        //pr($this->settings->data->domain);
        $curlUrl = $this->settings->data->domain.'/change_order/'.$order_id.'/'.$current.'/'.$new.'/';
        //pr($curlUrl);die();
        curl_setopt($ch, CURLOPT_URL, $curlUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
        curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec ($ch);
		//pr('curl result');
		if (empty($result)){
			$result = [
				'result'=>false,
				'message'=>'Není spojení se serverem pro změnu provozu, zkontrolujte připojení k internetu a dostupnost www.chachar.cz',
			];
		} else {
            //pr($result);
			$result = json_decode($result,true);
            if (json_encode($result) == null){
                pr($result);
            }
        }
        die(json_encode($result));
    }


    /**
     * send doba rozvozu
     */
    public function sendTime($time_id,$web_id,$later=null){
        
        $ch = curl_init();
        $post = [];
        //pr($post);
        // load settings
        if (!isset($this->settings)){
            $this->getSettings();
        }
        //pr($this->settings->data->domain);
        $curlUrl = $this->settings->data->domain.'/confirm_email/'.$time_id.'/'.$web_id.'/'.$later;
        //pr($curlUrl);die();
        curl_setopt($ch, CURLOPT_URL, $curlUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post)); 
        curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec ($ch);
		//pr('curl result');
		if (empty($result)){
			$result = [
				'result'=>false,
				'message'=>'Není spojení se serverem pro poslání doby rozvozu, zkontrolujte připojení k internetu a dostupnost www.chachar.cz',
			];
		} else {
            //pr($result);
			$result = json_decode($result,true);
            if (json_encode($result) == null){
                pr($result);
            }
        }
        die(json_encode($result));
    }
    
    /**
     * Funkce ktera kontrolujte frontu EET a pozadavky ktere nebyly vyrizeny odesle znovu
     */
    public function checkEetQueue(){
        $this->loadModel('EetQueues');
        $queue = $this->EetQueues->find()->where(['sended IS NULL'])->limit(20)->toArray();
        
        $c = 0;
        foreach($queue as $ent){
             $this->EetQueues->sendToEetServer($ent);
             $c++;
        }
        die($c==0 ? 'Nenalezeny zadne nezpracovane polozky' : 'Zpracovano '. $c . ' polozek');
    } 

    /**
     * Funkce pro odeslani zaznamu ktere se drive nepodarilo odeslat na cloud a nemaji tedy nastaveno sended
     */
    public function clearLocalQueues(){
        $limit = 40;
        
        $resultEet = $this->sendWaitingEetQueue($limit ,true);

        $this->loadModel('Stocks');
        $resultStock = $this->Stocks->sendQueue($limit);

        $this->loadModel('Orders');
        $resultOrder = $this->Orders->sendQueue($limit);

        die(json_encode([ 'result'=> true, 'message'=>'Bylo odeslano: ' . ( $resultOrder['ok'] > 0 ? $resultOrder['ok'] . ' objednavek, ' : '') . ($resultStock['ok'] > 0 ? $resultStock['ok'] .' skladu' : ''), 'data'=>[
            'processed_stocks' => $resultStock['ok'],
            'processed_orders' => $resultOrder['ok'],
            'eet' => $resultEet,
        ]])); 
    }
     
    /**
     * Stahovani aktualnich novych objednavek z eshopu, bude volano automaticky pravidelne 
     */
    public function getWebOrders(){
        $this->loadModel('WebOrders'); 
        $lastId = $this->WebOrders->find()->select(['lastId'=>'MAX(web_id)'])->first();

        //Pokud je nalezeno posledni ID webove objednavky, tak se posila do feedu pro stazeni
        $data = @file_get_contents( API_URL . '/get_web_orders/' . $this->system_id . '/' . ($lastId ? $lastId->lastId : 0) , false , stream_context_create(['http'=>[
            'timeout' => 8,
        ]]));

       if($data === false || ($data = json_decode($data, true)) === false){
            die(json_encode(['result'=>false, 'message'=>'Empty data']));
       }else{
            if(isset($data['result']) && $data['result'] == true){
                if(isset($data['data']) && !empty($data['data'])){
                    $data = $data['data'];
                }else{
                    die(json_encode(['result'=>true, 'message'=>'Zadne nove objednavky']));
                }
            }else{
                die(json_encode(['result'=>false, 'message'=>'No data to process']));
            }
        }

       $ids = Hash::extract($data, '{n}.web_id');
      
       $existIds = $this->WebOrders->find('list', ['keyField'=>'web_id', 'valueField'=>'id'])->where(['web_id IN'=>$ids])->toArray();

       $ok = $err = $exist = 0;
       $founded = Count($data);
       $errors = [];
       $printOrders = [];
       if(!empty($data)){
            foreach($data as $item){
                //Vkladame pouze ty objednavky ktere zatim nejsou vlozeny
                if(!isset($existIds[$item['web_id']])){
                        $order = $this->WebOrders->newEntity($item);
                        try{
                            $order->doprava_zdarma = (!isset($order->doprava_zdarma) || $order->doprava_zdarma === null ? 0 : $order->doprava_zdarma);
                            if($this->WebOrders->save($order)){
                                $printOrders[] = $this->orderDataForPrint($order);
                                $ok++;
                            }
                        }catch(\PDOException $e){
                            $errors[$item['web_id']] = $e->getMessage();
                            $err++;
                        }
                }else{
                    $exist++;
                }
            }
        }
        die(json_encode(['result'=> true, 'ok'=>$ok, 'err'=> $err, 'founded'=> $founded, 'exist'=>$exist, 'errors'=> $errors, 'printOrders'=> $printOrders]));
    }

    /**
     * Pomocna funkce pro nachystani dat nove objednavky pro tisk
     */
    private function orderDataForPrint($order){
        $table_data = [];
        if(isset($order['web_order_items'])){
            foreach($order['web_order_items'] AS $item){
                $table_data[] = [
                    'format'=>'',
                    'data'=>[
                       // 'Kod'=> $item['num'], 
                        'Produkt'=> mb_substr($item['name'],0,18), 
                        'Ks'=> $item['ks'], 
                        'Cena'=>$this->price_format($item['price_with_tax']),
                    ],
                ];
            }
        }
        $json_print_data = [
            'printer_name'=>$this->settings['printer'],
            'is_logo'=>true,
           // 'path_logo' => $path_logo_domain.$path_logo.'logo_uctenka.png',
            'header'=>[
                [
                    'font'=>'A',
                    'align'=>'left',
                    'format'=>'',
                    'data'=>' '
                ],
                [
                    'font'=>'A',
                    'align'=>'center',
                    'format'=>'',
                    'data'=>'Nová objednávka:',
                ],
                [
                    'font'=>'A',
                    'align'=>'left',
                    'format'=>'',
                    'data'=>' '
                ],
                [
                    'font'=>'A',
                    'align'=>'left',
                    'format'=>'',
                    'data'=> 'Klient: ' . $order['client_name'],
                ],
                [
                    'font'=>'A',
                    'align'=>'left',
                    'format'=>'',
                    'data'=> 'Adresa: ' . $order['client_address'],
                ],
                [
                    'font'=>'A',
                    'align'=>'left',
                    'format'=>'',
                    'data'=> 'Telefon: ' . $order['client_phone'],
                ],
                [
                    'font'=>'A',
                    'align'=>'left',
                    'format'=>'',
                    'data'=> 'Email: ' . $order['client_email'],
                ],
                [
                    'font'=>'A',
                    'align'=>'left',
                    'format'=>'',
                    'data'=>'-----------------------------------------',
                ],
                
            ],
            'footer'=>[
                [
                    'align'=>'left',
                    'format'=>'normal',
                    'data'=> ' ',
                ],
            ],
            'table_data'=> $table_data,
            'table_align'=>[
                'Produkt'=>'left',
                'Ks'=>'right',
                'Cena'=>'right',	
            ],
        ];

        return $json_print_data;
    }

    /**
     * Opetovne Odeslani uctenek ktere se neposlaly na EET z fronty
     */
    public function sendWaitingEetQueue($limit = 20, $return = false){
        $result = ['count'=>0, 'done'=>0, 'errors' => [], 'ids'=> [] ];
        $this->loadModel('Branches');
        $branch = $this->Branches->find()->where(['id'=> $this->settings->data->system_id])->first();
        //Pouze pokud ma pobocka aktivni EET Certifikat
        if($branch && $branch->eet_active){
            $this->loadModel('EetQueues');
            $eetQueue = $this->EetQueues->find()->where([
                'sended IS NULL', 
                'DATE(created) >'=> (new \Cake\I18n\Date('2018-07-29'))->format('Y-m-d')])->limit($limit)->toArray();
           
            //$result['done'] = Count($eetQueue);
            if($eetQueue){
                $result['count'] = Count($eetQueue);
                foreach($eetQueue as $item){
                    $res = $this->EetQueues->sendToEetServer($item);
                    if($res !== false && $res->sended){
                        $result['done']++;
                        $result['ids'][] = $item->id;
                    }
                    if($this->EetQueues->error){
                        $result['errors'][$item->id] =  $this->EetQueues->error;
                    }
                }
            }
        }
        if($return){
            return $result;
        }else{
            die(json_encode($result));
        }
    }
}

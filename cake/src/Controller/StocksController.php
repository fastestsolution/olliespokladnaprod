<?php
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;
use Cake\Cache\Cache;
use Cake\I18n\Time;
use App\Component\vIComponent;

class StocksController extends AppController{

    private $select_list;
    public $paginate = [
        'limit' => 50,
        'order' => [
        ]
    ];

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
    }

    public function index()
    {
        $this->loadComponent('vI');
        $this->vI->disable_status = true;
        $this->getSelectList(true, 'index');

        $conditions = [];

        $fields_defined = [
            ['col'=>'id','title'=>'ID','type'=>'text'],
            ['col'=>'stock_type_id','title'=>'Typ','type'=>'list','list_data'=>$this->stock_type_list],
            ['col'=>'StockGlobalItems.name','title'=>'Sklad položka','type'=>'text','col_names'=> ['stock_global_item','name']],
            ['col'=>'Products.name','title'=>'Produkt','type'=>'text', 'col_names'=> ['product','name'] /*,'list'=>$this->select_list->product_list*/],
            ['col'=>'value','title'=>'Množství','type'=>'text'],
            ['col'=>'nakup_price_total','title'=>'Nákupní cena celkem','type'=>'price'],
            ['col'=>'nakup_price_total_vat','title'=>'Nákupní cena celkem s DPH','type'=>'price'],
            ['col'=>'unit_id','title'=>'Jednotka','type'=>'list', 'list_data'=>$this->stock_unit_list],
            ['col'=>'invoice','title'=>'Faktura','type'=>'text'],
            ['col'=>'created','title'=>'Vytvořeno','type'=>'datetime'],
            ['col'=>'user_name','title'=>'Uživatel'],
            ['col'=>'Branches.name','title'=>'Pobočka','type'=>'text', 'col_names'=> ['branch','name']],
            ['col'=>'FromBranches.name','title'=>'Pobočka na/z','type'=>'text', 'col_names'=> ['from_branch','name']],
            ['col'=>'product_group_id','title'=>'Skupina','type'=>'list','list_data'=>$this->select_list->category_list],
        ];

        $this->filtration_defined = [
            'id'=>['col'=>'id','title'=>'ID','type'=>'text'],
            'name'=>['col'=>'name','title'=>'Jméno','type'=>'text_like'],
            'code'=>['col'=>'code','title'=>'Kod','type'=>'text'],
        ];
        
        if (isset($this->request->data['conditions'])){
            $conditions = $this->vI->convertConditions($conditions,$this->request->data['conditions'],$this->filtration_defined);
        }
        
        $posibility = [
           // ['link'=>'./edit/','title'=>__('Editovat'),'class'=>'fa-edit','type'=>'router','params'=>'edit'],
            //['link'=>'/api/trash/Stocks/','title'=>__('Smazat'),'class'=>'fa-trash','type'=>'ajax','params'=>'trash','confirm'=>__('Opravdu smazat?')],
        ];
        
        $top_actions = [
            ['link'=>'./add','title'=>__('Příjem'),'icon'=>'fa-plus','type'=>'link','params'=>'add_stock'],
            ['link'=>'./move','title'=>__('Převodka'),'icon'=>'fa-paper-plane-o','type'=>'link','params'=>'move_stock'],
            ['link'=>'./remove','title'=>__('Odpis'),'icon'=>'fa-minus','type'=>'link','params'=>'remove_stock'],
            ['link'=>'./mirrors','title'=>__('Zrcadla'),'icon'=>'fa-clone', 'class'=>'btn-info','type'=>'link','params'=>'mirrors'],
        ];
  
        $fields = $this->vI->fieldsConvert($fields_defined);

        $mapper = function ($data, $key, $mapReduce) {            
            $mapReduce->emit($data);  
        };
                
        $query = $this->Stocks->find()
            ->select($fields)
            ->where($conditions)
            ->contain(['Products', 'StockGlobalItems', 'Branches', 'FromBranches', 'ProductGroups'])
           /* ->cache(function ($query) {
				return 'deliverys_data-' . md5(serialize($query->clause('where')));
            })*/
            ->mapReduce($mapper);

        
        if (isset($this->request->query['firstLoad'])){
            $this->request->query['sort'] = 'created';
            $this->request->query['direction'] = 'DESC';
        }
        
        $this->loadComponent('Paginator');
        
        $data_list = $this->paginate($query);
        
        $pagination = $this->vI->convertPagination();

        $results = [
            'result'=>true,
            'data'=>$data_list->toArray(),
            'data_count'=>count($data_list->toArray()),
            'pagination'=>$pagination,
            'table_th'=>$fields_defined,
            'filtration'=>array_values($this->filtration_defined),
            'select_list'=>$this->select_list,
            'posibility'=>$posibility,
            'top_actions'=>$top_actions,
            'conditions'=>(object) $conditions,
        ];
        
        $this->setJsonResponse($results);
    }

    /**
     * Prijemka
     */
    public function add(){
        $this->loadComponent('vI');

        if(isset($this->request->data['saveData'])){
            $result = ['result'=> false, 'error'=>[], 'saved'=>[], 'message'=>''];
            foreach($this->request->data['saveData'] as $i => $stock){ 
                $stock['createdTime'] = time();  
                $stock['stock_type_id'] = 1;  
                if($stock['note'] == ''){
                    $stock['note'] = null;  
                }
                $entity = $this->Stocks->newEntity($stock /*, ['validate'=> false]*/);
                if($entity->invoice == ''){
                    $entity->invoice = null;
                }
                if(!empty($entity->errors()) || !$this->Stocks->save($entity)){
                    $result['errors'][$i] = 'Chyba uložení: '.  $entity->errors();
                }else{
                    $result['ok'][$i] = $entity->id; 
                }
            }
            if(empty($result['errors'])){
                $result['result'] =  true;
                $result['message'] =  'Příjemka byla uložena';
            }else{
                $result['result'] =  false;
                $result['message'] =  'Během ukládání došlo k chybám. Uloženo bylo '. Count($result['ok']).' záznamů. U '.Count($result['errors']).' záznamů došlo k chybám';
            }
            die(json_encode($result));
        }else{
                $this->getSelectList(true, 'add');
            
                $data = $this->vI->emptyEntity('Stocks',[/*'belongsTo'=>'Clients'*/]); 
                $defaultValues = [
                    'system_id'=> $this->settings->data->system_id,
                    'value'=> 1,
                    'tax_id'=> 1,
                    'unit_id'=> 1,
                    'user_id'=> $this->loggedUser['id'],
                    'user_name'=> $this->loggedUser['name'],
                    'invoice'=> null,
                    'note'=> null,
                    'stock_type_id'=>1, //PRIJEM
                    'sended'=> null
                ];  
                $data = $this->vI->convertLoadData($data,$defaultValues);
                $validations = $this->vI->getValidations('Stocks',[/*'belongsTo'=>'Clients'*/]);

                $results = [
                    'result'=>true,
                    'data' => $data,
                    'validations'=> $validations,
                    'select_list'=>$this->select_list,
                ];

                $this->setJsonResponse($results);
        }
    }

    /**
     * Odpis
     */
    public function remove(){
        $this->loadComponent('vI');

        if(isset($this->request->data['saveData'])){
            $result = ['result'=> false, 'error'=>[], 'saved'=>[], 'message'=>''];
            foreach($this->request->data['saveData'] as $i => $stock){ 
                $stock['createdTime'] = time();  
                $stock['stock_type_id'] = 3;  
                if($stock['note'] == ''){
                    $stock['note'] = null;  
                }
                $entity = $this->Stocks->newEntity($stock /*, ['validate'=> false]*/);
                if($entity->invoice == ''){
                    $entity->invoice = null;
                }
                if(!empty($entity->errors()) || !$this->Stocks->save($entity)){
                    $result['errors'][$i] = 'Chyba uložení: '.  $entity->errors();
                }else{
                    $result['ok'][$i] = $entity->id; 
                }
            }
            if(empty($result['errors'])){
                $result['result'] =  true;
                $result['message'] =  'Odpis byl uložen';
            }else{
                $result['result'] =  false;
                $result['message'] =  'Během ukládání došlo k chybám. Uloženo bylo '. Count($result['ok']). ' záznamů. U '. Count($result['errors']) .' záznamů došlo k chybám';
            }
            die(json_encode($result));
        }else{
                $this->getSelectList(true, 'remove');
            
                $data = $this->vI->emptyEntity('Stocks',[/*'belongsTo'=>'Clients'*/]); 
                $defaultValues = [
                    'system_id'=> $this->settings->data->system_id,
                    'value'=> 1,
                    'tax_id'=> 1,
                    'unit_id'=> 1,
                    'user_id'=> $this->loggedUser['id'],
                    'user_name'=> $this->loggedUser['name'],
                    'invoice'=> null,
                    'note'=> null, 
                    'stock_type_id'=> 3, //ODPIS
                    'sended'=> null
                ];  

                $data = $this->vI->convertLoadData($data,$defaultValues);

                $validations = $this->vI->getValidations('Stocks',[/*'belongsTo'=>'Clients'*/]);
    
                $results = [
                    'result'=>true,
                    'data' => $data,
                    'validations'=> $validations,
                    'select_list'=>$this->select_list,
                ];

                $this->setJsonResponse($results);
        }
    }

    /**
     * Prevod
     */
    public function move(){
        $this->loadComponent('vI');

        if(isset($this->request->data['saveData'])){
            $result = ['result'=> false, 'error'=>[], 'saved'=>[], 'message'=>''];
            foreach($this->request->data['saveData'] as $i => $stock){ 
                $stock['createdTime'] = time();  
                $stock['stock_type_id'] = 2;  
                if($stock['note'] == ''){
                    $stock['note'] = null;  
                }
                $entity = $this->Stocks->newEntity($stock /*, ['validate'=> false]*/);
                if($entity->invoice == ''){
                    $entity->invoice = null;
                }
                if(!empty($entity->errors()) || !$this->Stocks->save($entity)){
                    $result['errors'][$i] = 'Chyba uložení: '.  $entity->errors();
                }else{
                    $result['ok'][$i] = $entity->id; 
                }
            }
            if(empty($result['errors'])){
                $result['result'] =  true;
                $result['message'] =  'Převod byl uložen';
            }else{
                $result['result'] =  false;
                $result['message'] =  'Během ukládání došlo k chybám. Uloženo bylo '. Count($result['ok']). ' záznamů. U '. Count($result['errors']) .' záznamů došlo k chybám';
            }
            die(json_encode($result));
        }else{
                $this->getSelectList(true, 'move');
            
                $data = $this->vI->emptyEntity('Stocks',[/*'belongsTo'=>'Clients'*/]); 
                $defaultValues = [
                    'system_id'=> $this->settings->data->system_id,
                    'system_id_to'=> null,
                    'value'=> 1,
                    'tax_id'=> 1,
                    'unit_id'=> 1,
                    'user_id'=> $this->loggedUser['id'],
                    'user_name'=> $this->loggedUser['name'],
                    'invoice'=> null,
                    'note'=> null, 
                    'stock_type_id'=> 2, //PREVOD MINUS -> PRI ODESLANI NA CLOUD JE NUTNE VYTVORIT NA DANE PROVOZOVNE PREVOD PLUS?
                    'sended'=> null
                ];  

                $data = $this->vI->convertLoadData($data,$defaultValues);

                $validations = $this->vI->getValidations('Stocks',[/*'belongsTo'=>'Clients'*/]);
    
                $results = [
                    'result'=>true,
                    'data' => $data,
                    'validations'=> $validations,
                    'select_list'=>$this->select_list,
                ];

                $this->setJsonResponse($results);
        }
    }


    /**
     * Zrcadla
     */
    public function mirrors(){
        if (!isset($this->settings)){
            $this->getSettings();
        }

        $this->paginate = [
            'limit' => 20,
            'order' => [
            ]
        ];

        $this->loadComponent('vI');

        if(isset($this->request->data['saveData'])){
            $result = ['result'=> false, 'error'=>[], 'saved'=>[], 'message'=>''];
            /*foreach($this->request->data['saveData'] as $i => $stock){ 
                $stock['createdTime'] = time();  
                $stock['stock_type_id'] = 2;  
                if($stock['note'] == ''){
                    $stock['note'] = null;  
                }
                $entity = $this->Stocks->newEntity($stock);
                if($entity->invoice == ''){
                    $entity->invoice = null;
                }
                if(!empty($entity->errors()) || !$this->Stocks->save($entity)){
                    $result['errors'][$i] = 'Chyba uložení: '.  $entity->errors();
                }else{
                    $result['ok'][$i] = $entity->id; 
                }
            }
            if(empty($result['errors'])){
                $result['result'] =  true;
                $result['message'] =  'Převod byl uložen';
            }else{
                $result['result'] =  false;
                $result['message'] =  'Během ukládání došlo k chybám. Uloženo bylo '. Count($result['ok']). ' záznamů. U '. Count($result['errors']) .' záznamů došlo k chybám';
            }*/
            die(json_encode($result));
        }else{
                $this->getSelectList(true, 'mirrors');
                
                $this->loadModel('StockMirrors');
                $query = $this->StockMirrors->find()->where(['system_id' => $this->settings->data->system_id])->order(['id'=>'DESC']);
                
                if (isset($this->request->data['pagination'])){
                    foreach($this->request->data['pagination'] AS $pag_key=>$pag_value){
                        if (in_array($pag_key,['page','sort','direction'])){
                            $this->request->query[$pag_key] = $pag_value;
                        }
                        
                    }
                }    
        
                if (!isset($this->request->query['sort']) || empty($this->request->query['sort'])){
                    $this->request->query['sort'] = 'id';
                    $this->request->query['direction'] = 'DESC';
                }
                
                //$data = $this->vI->convertLoadData($data,$defaultValues);
                $data = $this->paginate($query);
        
                $pagination = $this->vI->convertPagination('StockMirrors');

                $validations = $this->vI->getValidations('StockMirrors',[/*'belongsTo'=>'Clients'*/]);
    
                $results = [
                    'result'=>true,
                    'data' => $data->toArray(),
                    'pagination' => $pagination,
                    'validations'=> $validations,
                    'select_list'=>$this->select_list,
                ];

                $this->setJsonResponse($results);
        }
    }

    public function mirror($id = null, $step = null){
        $result = [];
        $this->loadComponent('vI');
        $this->loadModel('StockMirrors');

        //ULOZENI ZAZNAMU
        if (isset($this->request->data['saveData']) && $saveData = $this->request->data['saveData']){
            if($id == 'new'){
                $entity = $this->StockMirrors->newEntity();
            }else{
                $entity = $this->StockMirrors->find()->where(['id' => $id])->first();
                unset($saveData['created']);
                unset($saveData['modified']);
            }
            
            if($step == 1){
                $saveData['date_step1'] = new Time();
                $saveData['date_step2'] = null;
                $saveData['data_step2'] = $saveData['data_step1'];
                $saveData['step_id'] = 2;
            }else if($step == 2){
                $saveData['date_step2'] = new Time();
                $saveData['step_id'] = 3;
            }else if($step == 3){
                $saveData['date_confirm'] = new Time();
                $saveData['is_confirm'] = 1;
            }else{
                $result['result'] = false;
                $result['message'] = 'Step not defined';
                return $this->setJsonResponse($result);
            }
            if(isset($saveData['data_step1']) && is_array($saveData['data_step1'])){
                $saveData['data_step1'] = json_encode($saveData['data_step1']);
            }
            if(isset($saveData['data_step2']) && is_array($saveData['data_step2'])){

                $saveData['data_step2'] = json_encode($saveData['data_step2']);
            }

            $entity = $this->StockMirrors->patchEntity($entity, $saveData);

            $this->vI->checkErrors($entity);

            if($entity = $this->StockMirrors->save($entity)){
               
                $entity->data_step1 = json_decode($entity->data_step1, true);
                $entity->data_step2 = json_decode($entity->data_step2, true);
                $entity->data_diff = json_decode($entity->data_diff, true);

                //POKUD SE JEDNA O SCHVALENI ZRCADLA TAK PROVEDEME ZAPIS ROZDILU DO SKLADOVYCH ZAZNAMU...
                if($step == 3){
                    $this->mirrorDiffToStock($entity);
                }
                
                if(isset($entity->date_step1->timezone)){
                    $entity->date_step1 = $entity->date_step1->format('d.m. H:i');
                }
                if(isset($entity->date_step2->timezone)){
                    $entity->date_step2 = $entity->date_step2->format('d.m. H:i');
                }
                $validations = $this->vI->getValidations('StockMirrors');
                $result = [
                    'result'=>true,
                    'data' => $entity,
                    'validations'=> $validations,
                    'select_list'=> []
                ];
            }else{
                $result['result'] = false;
                $result['message'] = 'Chyba ulozeni';
            }
            return $this->setJsonResponse($result);
        }else{
                if($id){
                    $data = $this->StockMirrors->find()->where(['id'=>$id])->first();
                    if($data){
                        $data->data_step1 = json_decode($data->data_step1, true);
                        $data->data_step2 = json_decode($data->data_step2, true);
                        $data->data_diff = json_decode($data->data_diff, true);
                        $data->date_step1 = null;
                        $data->date_step2 = null;
                        $data->date_confirm = null;
                    }else{
                        $result['result'] =  false;
                        $result['message'] =  'Zaznam nenalezen';
                        return $this->setJsonResponse($result);
                    }
                }else{
                    $data = $this->vI->emptyEntity('StockMirrors'); 
                    $data->data_step1 = $this->loadMirrorData();
                    $data->system_id = $this->system_id;
                } 

                if(!$data->data_step1){
                    $data->data_step1 = [];
                }else{
                    if(isset($data->data_step1) && !empty($data->data_step1)){
                        foreach($data->data_step1 as &$item){
                            if(!isset($item['repair'])){
                                $item['repair'] = null;
                            }
                            if(!isset($item['note'])){
                                $item['note'] = null;
                            }
                        }
                    }
                    if(isset($data->data_step2) && !empty($data->data_step2)){
                        foreach($data->data_step2 as &$item){
                            if(!isset($item['repair'])){
                                $item['repair'] = null;
                            }
                            if(!isset($item['note'])){
                                $item['note'] = null;
                            }
                        }
                    }
                }
                if(!$data->data_step2){
                    $data->data_step2 = [];
                }
                if(!$data->data_diff){
                    $data->data_diff = new \stdClass();
                }

               /* $this->mirrorDiffToStock($data);
                die();*/
                if(isset($data->date_step1->timezone)){
                    $data->date_step1 = $data->date_step1->format('d.m. H:i');
                }
                if(isset($data->date_step2->timezone)){
                    $data->date_step2 = $data->date_step2->format('d.m. H:i');
                }

                $validations = $this->vI->getValidations('StockMirrors');
                $results = [
                    'result'=>true,
                    'data' => $data,
                    'validations'=> $validations,
                    'select_list'=> [] //$this->select_list,
                ];
                $this->setJsonResponse($results);
        }
       
    }

    /**
     * Zaneseni uprav jednotlivych skladovych polozek v ramci zrcadla do skladovych polozek
            // 1=>'Příjem',
            // 2=>'Převodka minus',
            // 11=>'Převodka plus',
            // 12=>'Storno objednavky',
            // 3=>'Odpis',
            // 4=>'Prodej',
            // 5=>'Zrcadlo plus',
            // 6=>'Zrcadlo minus',
            // 9=>'Zrcadlo plus - oprava',
            // 10=>'Zrcadlo minus - oprava',
            // 7=>'Příjem Makro',
            // 8=>'Příjem BidFood',
    */
    private function mirrorDiffToStock($mirror){
        $mirror->data_diff = [];
        $mirror->data_step_change = [];
        /**
         * Puvodne se ukladaly zvlast opravy, nyni se vse zapocitava rovnou do atributu date_diff a jiz se nevytvari opravene typy doprav 9 a 10
         */
        if(isset($mirror->data_step2) && !empty($mirror->data_step2)){
                foreach($mirror->data_step2 as $item){
                    if(isset($item['repair']) && (float) $item['repair'] !== null){
                        $mirror->data_step_change[$item['stock_item_id']] = ((float) $item['sum'] - (float) $item['repair'])  * -1;
                    }
                    $diff =  ((float) $item['sum'] - (float) $item['real'])  * -1;
                    
                    if((float) $diff != 0){
                        $mirror->data_diff[$item['stock_item_id']] = $diff;
                    }
                }
        }

        if(!empty($mirror->data_diff) && $mirror->id){
            $this->StockMirrors->updateAll(['data_diff' => json_encode($mirror->data_diff), 'data_step_change' => json_encode($mirror->data_step_change)], ['id'=> $mirror->id]);

            $this->loadModel('StocksItems');
            $this->loadModel('StockGlobalItems');
            $sklad_items = $this->StocksItems->skladItems();
            $sklad_items_list = $this->StockGlobalItems->skladItems();
            foreach($mirror->data_diff AS $stock_item_id =>$value){
                //Pokud je nastavena oprava, tak se bere hodnota opravy
                if(isset($mirror->data_step_change[$stock_item_id])){
                    $value = $mirror->data_step_change[$stock_item_id];
                }
                $saveStocks = [
                    'order_id'=> null,
                    'stock_item_id'=>$stock_item_id,
                    'stock_item_product_id'=> null,
                    'value'=>(( $value < 0 ) ? $value * -1 : $value ),
                    'stock_type_id'=>(( $value >= 0 ) ? 5 : 6 ),
                    'system_id'=> $mirror->system_id,
                    'tax_id'=> (isset($sklad_items_list[$stock_item_id])? $sklad_items_list[$stock_item_id]->tax_id: null),
                    'unit_id'=> (isset($sklad_items_list[$stock_item_id]) ? $sklad_items_list[$stock_item_id]->jednotka_id: null),
                    'user_id'=>$this->loggedUser['id'],
                    'user_name'=>$this->loggedUser['name'],
                    'terminal_id'=> null,
                    'product_group_id'=> null,
                    'createdTime'=>time(),
                ];
                 //pr($saveStocks);
                $entity = $this->Stocks->newEntity($saveStocks);
                $this->Stocks->save($entity);
        
            }
        }
    }

    /**
     * Nacteni aktualni sumace skladu pro vytvorteni noveho zracadla
     */
    private function loadMirrorData(){
        $this->loadModel('Stocks');
        $mirrorList = $this->Stocks->stockMirrorList($this->system_id);

        foreach($mirrorList AS $k=>$m){
            $mirrorList[$k]->unit = (isset($this->stock_unit_list[$m->stock_global_item->jednotka_id])?$this->stock_unit_list[$m->stock_global_item->jednotka_id]:'');
            $mirrorList[$k]->real = $mirrorList[$k]->sum;
        }
       
        return $mirrorList;
    }

    /**
     * vytvoreni select listu
     */
    public function getSelectList($check=true, $type = ''){
        $this->select_list = new \StdClass();
       
        if($type == 'index' || $type == 'add' || $type == 'remove' || $type == 'move'){
            $this->select_list->price_tax_list = $this->price_tax_list;
            $this->select_list->stock_unit_list = $this->stock_unit_list;

          

            $this->transformLists();

            $this->loadModel('ProductGroups');
            $this->select_list->category_list =  $this->ProductGroups->fullpathList();
            
            $this->select_list->stock_type_list = $this->stock_type_list;

            if($type == 'move'){
                $this->loadModel('Branches');
                $this->select_list->branch_list = $this->Branches->find()->select(['id','value'=>'name'])->where(['id !='=> $this->system_id])->toArray();
            }  

            $this->loadModel('Products');
            $this->select_list->product_list = $this->Products->productList2();

         
    
            $this->loadModel('StockGlobalItems');
            $this->select_list->stock_item_list = $this->StockGlobalItems->skladItemsList();
            $this->select_list->group_list = $this->StockGlobalItems->groupList();

            $this->select_list->tax_coeficients = $this->price_tax_list_con;
            $this->select_list->tax_rates = $this->price_tax_list_rates;
           
        } else if($type == 'mirrors' ){
            $this->select_list->yn_list = [ 0 => 'Ne', 1 => 'Ano'];
        } else if ($check == false || isset($this->request->query['firstLoad'])){

            return $this->select_list;
        } else {
            return false;
        }  
    }

    private function transformLists(){
        $tmp = new \stdCLass();
        foreach($this->select_list as $model => $list){
            $tmp->{$model} = [];
            foreach($list as $atr => $val){
                $tmp->{$model}[] = ['id'=>$atr,'value'=>$val];
            }
        }
        $this->select_list = $tmp;
    }


    /**
     * editace
     */
    public function edit($id=null){
        $select_list = $this->getSelectList(false);
        $this->loadComponent('vI');
        //pr($select_list);die();
        
        // save data
        if (isset($this->request->data['saveData'])){
            $saveData = $this->request->data['saveData'];
            $saveData = $this->vI->convertTime($saveData);
            
            $this->checkCode();
            
            $save_entity = $this->Stocks->newEntity($saveData);
            //pr($save_entity); die();
            
            $this->vI->checkErrors($save_entity);

            if (!$resultDb = $this->Stocks->save($save_entity)){
                $results = [
                    'result'=>false,
                    'message'=>__('Chyba uložení')
                ];    
            } else {
                $results = [
                    'result'=>true,
                    'message'=>__('Uloženo'),
                    'data'=>$resultDb
                ];  
            }
            
            $this->setJsonResponse($results);
        
        // load data
        } else {
            $defaultValues = [
                'nodelete'=>0,
            ];
            if ($id != null){
                // pokud je edit
                if ($id != 'new'){
                    $conditions = ['id'=>$id];
                    $data = $this->Stocks->find()
                        ->select()
                        ->where($conditions)
                        ->first();
                // pokud je nova polozka
                } else {
                   
                    $data = $this->vI->emptyEntity('Stocks'); 
                    
                }
                $data = $this->vI->convertLoadData($data,$defaultValues);        
                //pr($data);die();
                $validations = $this->vI->getValidations('Stocks');
            } else {

            }

            $results = [
                'result'=>true,
                'validations'=>(isset($validations)?$validations:null),
                'data'=>(isset($data)?$data:''),
                'select_list'=>$this->vI->SelectList($select_list),
            ];  
            //pr($results);die();  
            $this->setJsonResponse($results);
        }
        $this->clearCache('delivery_data');
    }
    

    /**
     * kontrola existence kodu
     */
    private function checkCode(){
        //pr($this->request);
        $con = [
            'code'=>$this->request->data['saveData']['code'],
        ];
        if (!empty($this->request->data['saveData']['id'])){
            $con['id !='] = $this->request->data['saveData']['id'];
        }

        $find = $this->Stocks->find()
        ->select([])
        ->where($con)
        ->first();
        if ($find){
            $results = [
                'result'=>false,
                'message'=>__('Kód produktu je již použit'),
            ];
            die(json_encode($results));  
    
        }
    }

    function sendApp(){
		$this->autoRender = false;
		$this->getSettings();
        $rozvozce_list_load = $this->Stocks->find()
        ->toArray();
        $rozvozce_list = [];
        foreach($rozvozce_list_load AS $r){
            $rozvozce_list[] = [
                'Rozvozce'=>[
                    'id'=> $r->id,
                    'name'=> $r->name,
                    'code'=> $r->code,
                ]
            ];
        }
		
		$ch = curl_init();
		$post = array(
			'list'=>$rozvozce_list,
			'pokladna_id'=>$this->settings->data->system_id,
        );
        //pr($post);die();
		
		curl_setopt($ch, CURLOPT_URL, CLOUD_URL.'/mobile_add/');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec ($ch);
		//pr($result);
		$result = (json_decode($result,true));
		curl_close ($ch);
		if ($result['r'] == true){
			die(json_encode(array('result'=>true,'message'=>'Odesláno do aplikace')));
			
		} else {
			die(json_encode(array('result'=>false,'message'=>'Chyba odeslání')));
	
		}
	}
}
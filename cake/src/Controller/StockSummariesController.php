<?php
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;
use Cake\Cache\Cache;
use Cake\I18n\Time;
use Cake\I18n\Date;
use App\Component\vIComponent;

class StockSummariesController extends AppController{

    private $select_list;
    public $paginate = [
        'limit' => 50,
        'order' => [
            
        ]
    ];

    public function index()
    {
        $this->loadComponent('vI');
        $this->vI->disable_status = true;
        $this->vI->model_name = 'Stocks';

        $this->getSelectList();
        $this->loadModel('Stocks');
        $conditions = [
            'Stocks.stock_item_id >' => 0,
            'StockGlobalItems.id IS NOT NULL'
        ];

        if(isset($this->request->data['conditions']['date']) && is_array($this->request->data['conditions']['date'])){
            $d = $this->request->data['conditions']['date'];
            $date = new Date($d['year'].'-'.$d['month'].'-'.$d['day']);
            unset($this->request->data['conditions']['date']);
            $conditions['Stocks.created <='] = $date->format('Y-m-d').' 23:59:59';
        }else{
            $date = new Date();
        }

        
        $minus_without_sell = $this->Stocks->minus_types;
        if (($key = array_search(4, $minus_without_sell)) !== false) {
            unset($minus_without_sell[$key]);
        }
        $fields_defined = [
            ['col'=>'stock_item_id','title'=>'ID','type'=>'text'],
            ['col'=> 'StockGlobalItems.name','title'=>'Sklad položka','type'=>'text', 'col_names'=> ['stock_global_item', 'name']],
           // ['col'=> 'Products.name','title'=>'Produkt','type'=>'text', 'col_names'=> ['product', 'name']],
           // ['col'=>['item_name'=>'IF(StockGlobalItems.name != "", StockGlobalItems.name, CONCAT("__ ", Products.name))'],'title'=>'Sklad položka','type'=>'text', 'col_names'=> ['item_name']],
            ['col'=>['sum_value'=>'SUM(IF(Stocks.stock_type_id IN ('.implode(',', $this->Stocks->minus_types).'), (value * -1), value))'] ,'title'=>'Aktuální stav','type'=>'number', 'col_names'=> ['sum_value']],
            ['col'=>['start_value'=>'SUM(IF( Stocks.created < "'. $date->format('Y-m-d') .' 00:00:00", IF(Stocks.stock_type_id IN ('.implode(',',$this->Stocks->minus_types).'), (value * -1), value), 0))'] ,'title'=>'Počáteční stav','type'=>'number', 'col_names'=> ['start_value']],
            ['col'=>['item_plus'=>'SUM(IF(Stocks.stock_type_id IN ('.implode(',',$this->Stocks->plus_types).') AND Stocks.created LIKE "'. $date->format('Y-m-d') .'%", value, 0))'],'title'=>'Příjem','type'=>'number', 'col_names'=> ['item_plus']],
            ['col'=>['item_minus'=>'SUM(IF(Stocks.stock_type_id IN ('.implode(',', $minus_without_sell).') AND Stocks.created LIKE "'. $date->format('Y-m-d') .'%", value, 0))'],'title'=>'Odpis','type'=>'number', 'col_names'=> ['item_minus']],
            ['col'=>['item_sell'=>'SUM(IF(Stocks.stock_type_id = 4 AND Stocks.created LIKE "'. $date->format('Y-m-d') .'%", value, 0))'],'title'=>'Prodej','type'=>'number', 'col_names'=> ['item_sell']],
          /*  ['col'=>'Products.name','title'=>'Produkt','type'=>'text', 'col_names'=> ['product','name']],
            ['col'=>'value','title'=>'Množství','type'=>'text'],
            ['col'=>'nakup_price_total','title'=>'Nákupní cena celkem','type'=>'price'],
            ['col'=>'nakup_price_total_vat','title'=>'Nákupní cena celkem s DPH','type'=>'price'],
            ['col'=>'unit_id','title'=>'Jednotka','type'=>'list', 'list'=>$this->stock_unit_list],
            ['col'=>'invoice','title'=>'Faktura','type'=>'text'],
            ['col'=>'created','title'=>'Vytvořeno','type'=>'datetime'],
            ['col'=>'user_name','title'=>'Uživatel'],
            ['col'=>'Branches.name','title'=>'Pobočka','type'=>'text', 'col_names'=> ['branch','name']],
            ['col'=>'FromBranches.name','title'=>'Pobočka na/z','type'=>'text', 'col_names'=> ['from_branch','name']],
            ['col'=>'ProductGroups.name','title'=>'Skupina','type'=>'text', 'col_names'=> ['product_group','name']],*/
        ];

        $this->filtration_defined = [
            'stock_item_id'=>['col'=>'stock_item_id','title'=>'ID','type'=>'text'],
            'StockGlobalItems.name'=>['col'=>'StockGlobalItems.name','title'=>'Nazev','type'=>'text_like'],
            'date'=>['col'=>'date','title'=>'Datum','type'=>'date'],
           // 'code'=>['col'=>'code','title'=>'Kod','type'=>'text'],
        ];

        if (isset($this->request->data['conditions'])){
            $conditions = $this->vI->convertConditions($conditions,$this->request->data['conditions'],$this->filtration_defined);
        }

        $posibility = [
            //['link'=>'./edit/','title'=>__('Editovat'),'class'=>'fa-edit','type'=>'router','params'=>'edit'],
            //['link'=>'/api/trash/Stocks/','title'=>__('Smazat'),'class'=>'fa-trash','type'=>'ajax','params'=>'trash','confirm'=>__('Opravdu smazat?')],
        ];
        
        $top_actions = [
           /* ['link'=>'./add','title'=>__('Příjem'),'icon'=>'fa-plus','type'=>'link','params'=>'add_stock'],
            ['link'=>'./move','title'=>__('Převodka'),'icon'=>'fa-paper-plane-o','type'=>'link','params'=>'move_stock'],
            ['link'=>'./remove','title'=>__('Odpis'),'icon'=>'fa-minus','type'=>'link','params'=>'remove_stock'],
            ['link'=>'./mirrors','title'=>__('Zrcadla'),'icon'=>'fa-clone', 'class'=>'btn-info','type'=>'link','params'=>'mirrors'],*/
        ];
            
        $fields = $this->vI->fieldsConvert($fields_defined);

        //$fields['item_name'] = 'IF(StockGlobalItems.name AND Products.name, CONCAT(StockGlobalItems.name, " - ", Products.name), StockGlobalItems.name)';


        //pr($fields); die();
        $mapper = function ($data, $key, $mapReduce) {            
            $mapReduce->emit($data);  
        };
                
        
        $query = $this->Stocks->find()
            ->select($fields)
            ->where($conditions)
            ->contain(['Products', 'StockGlobalItems', 'Branches', 'FromBranches', 'ProductGroups'])
            ->group(['Stocks.stock_item_id'/*,'Stocks.stock_item_product_id'*/])
            ->mapReduce($mapper);

        
        if (isset($this->request->query['firstLoad'])){
            $this->request->query['sort'] = 'created';
            $this->request->query['direction'] = 'DESC';
        }
      
        $this->loadComponent('Paginator');

        $data_list = $this->paginate($query);
        
        $pagination = $this->vI->convertPagination();

        $results = [
            'result'=>true,
            'data'=>$data_list->toArray(),
            'data_count'=>count($data_list->toArray()),
            'pagination'=>$pagination,
            'table_th'=>$fields_defined,
            'filtration'=>array_values($this->filtration_defined),
            'select_list'=>$this->select_list,
            'posibility'=>$posibility,
            'top_actions'=>$top_actions,
            'conditions'=>(object) $conditions,
        ];
        $this->setJsonResponse($results);
    }



    /**
     * vytvoreni select listu
     */
    public function getSelectList($check=true, $type = ''){
        $this->select_list = new \StdClass();
       
        if($type == 'add' || $type == 'remove' || $type == 'move'){
            $this->select_list->price_tax_list = $this->price_tax_list;
            $this->select_list->stock_unit_list = $this->stock_unit_list;
            $this->transformLists();

            if($type == 'move'){
                $this->loadModel('Branches');
                $this->select_list->branch_list = $this->Branches->find()->select(['id','value'=>'name'])->toArray();
            }  

            $this->loadModel('Products');
            $this->select_list->product_list = $this->Products->productList2();
    
            $this->loadModel('StockGlobalItems');
            $this->select_list->stock_item_list = $this->StockGlobalItems->skladItemsList();
            $this->select_list->group_list = $this->StockGlobalItems->groupList();

            $this->select_list->tax_coeficients = $this->price_tax_list_con;
            $this->select_list->tax_rates = $this->price_tax_list_rates;
           
        } else if($type == 'mirrors' ){
            $this->select_list->yn_list = [ 0 => 'Ne', 1 => 'Ano'];
        } else if ($check == false || isset($this->request->query['firstLoad'])){

            return $this->select_list;
        } else {
            return false;
        }  
    }

    private function transformLists(){
        $tmp = new \stdCLass();
        foreach($this->select_list as $model => $list){
            $tmp->{$model} = [];
            foreach($list as $atr => $val){
                $tmp->{$model}[] = ['id'=>$atr,'value'=>$val];
            }
        }
        $this->select_list = $tmp;
    }


    /**
     * editace
     */
    public function edit($id=null){
        $select_list = $this->getSelectList(false);
        $this->loadComponent('vI');
        //pr($select_list);die();
        
        // save data
        if (isset($this->request->data['saveData'])){
            $saveData = $this->request->data['saveData'];
            $saveData = $this->vI->convertTime($saveData);
            
            $this->checkCode();
            
            $save_entity = $this->Stocks->newEntity($saveData);
            //pr($save_entity); die();
            
            $this->vI->checkErrors($save_entity);

            if (!$resultDb = $this->Stocks->save($save_entity)){
                $results = [
                    'result'=>false,
                    'message'=>__('Chyba uložení')
                ];    
            } else {
                $results = [
                    'result'=>true,
                    'message'=>__('Uloženo'),
                    'data'=>$resultDb
                ];  
            }
            
            $this->setJsonResponse($results);
        
        // load data
        } else {
            $defaultValues = [
                'nodelete'=>0,
            ];
            if ($id != null){
                // pokud je edit
                if ($id != 'new'){
                    $conditions = ['id'=>$id];
                    $data = $this->Stocks->find()
                        ->select()
                        ->where($conditions)
                        ->first();
                // pokud je nova polozka
                } else {
                   
                    $data = $this->vI->emptyEntity('Stocks'); 
                    
                }
                $data = $this->vI->convertLoadData($data,$defaultValues);        
                //pr($data);die();
                $validations = $this->vI->getValidations('Stocks');
            } else {

            }

            $results = [
                'result'=>true,
                'validations'=>(isset($validations)?$validations:null),
                'data'=>(isset($data)?$data:''),
                'select_list'=>$this->vI->SelectList($select_list),
            ];  
            //pr($results);die();  
            $this->setJsonResponse($results);
        }
        $this->clearCache('delivery_data');
    }
    
}
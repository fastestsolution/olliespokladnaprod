<?php
namespace App\Controller;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;
use Cake\Cache\Cache;
use Cake\I18n\Time;
use App\Component\vIComponent;

class StatisticsController extends AppController{
    
    /**
     * vytvoreni select listu
     */
    public function getSelectList($check=true){
        if ($check == false || isset($this->request->query['firstLoad'])){
            $this->loadModel('Users');
            $this->loadModel('ProductGroups');
            $this->loadModel('Deadlines');
            $this->deadline_list = $this->Deadlines->deadlineList();
            $this->product_groups_list = $this->ProductGroups->groupListTree();
            $this->users_list = $this->Users->userList();
            $this->groups_list = $this->ProductGroups->groupList();
            $this->groups_list_tree = $this->ProductGroups->groupTreeList();
            //pr($this->product_groups_list);die();
            return $select_list = [
                'no_yes'=>$this->no_yes,
                //'product_groups_list'=>$this->product_groups_list,
               // 'groups_list'=>$this->groups_list,
                'deadline_list'=>$this->deadline_list['list'],
                'price_tax_list'=>$this->price_tax_list,
                'users_list'=>$this->users_list,
                'groups_list_tree'=>$this->groups_list_tree,
            ];
        } else {
            return false;
        }  
        
        
    }

    /**
     * INDEX orders
     */
    public function index()
    {
        $this->loadComponent('vI');
        $this->vI->disable_status = true;
        
        $select_list = $this->getSelectList();
        //pr($select_list);die();
        $conditions = ['id'=>-1];
        $fields_defined = [
            ['col'=>'id','title'=>'ID','type'=>'text'],
            ['col'=>'name','title'=>'Název','type'=>'text'],
            ['col'=>'storno','title'=>'Storno','type'=>'list','list_data'=>$select_list['no_yes']],
            ['col'=>'count','title'=>'Množství','type'=>'text'],
            ['col'=>'price','title'=>'Cena s DPH','type'=>'text'],
            ['col'=>'price_without_tax','title'=>'Cena bez DPH','type'=>'text'],
            ['col'=>'tax_id','title'=>'DPH','type'=>'list','list_data'=>$select_list['price_tax_list']],
            ['col'=>'price_total','title'=>'Celkem s DPH','type'=>'text'],
            ['col'=>'price_total_without_tax','title'=>'Celkem bez DPH','type'=>'text'],
            ['col'=>'user_id','title'=>'Pracovník','type'=>'list','list_data'=>$select_list['users_list']],
            ['col'=>'created','title'=>'Naposledny prodáno','type'=>'datetime'],
            /*
            2=>['col'=>'','title'=>'Skupina','type'=>'list','list_data'=>$select_list['product_groups_list']],
            3=>['col'=>'price','title'=>'Cena','type'=>'text'],
            4=>['col'=>'amount','title'=>'Gramáž','type'=>'text'],
            5=>['col'=>'code','title'=>'Kod','type'=>'text'],
            6=>['col'=>'num','title'=>'Číslo','type'=>'text'],
            7=>['col'=>'created','title'=>'Vytvořeno','type'=>'datetime'],
            */
            //3=>['col'=>'zdroj_id','title'=>'Zdroj','type'=>'list','list_data'=>'zdroj_list'],
        ];

        $this->filtration_defined = [
            //'id'=>['col'=>'id','title'=>'ID','type'=>'text'],
           // 'name'=>['col'=>'name','title'=>'Jméno','type'=>'text_like'],
            'created'=>['col'=>'created','title'=>'Datum','type'=>'date_range'],
            'order_id'=>['col'=>'order_id','title'=>'Uzávěrka','type'=>'select','list'=>$this->vI->filtrSelectList($this->deadline_list['list'])],
            'storno'=>['col'=>'storno','title'=>'Storno','type'=>'select','list'=>$this->vI->filtrSelectList($select_list['no_yes'])],
            'tax_id'=>['col'=>'tax_id','title'=>'DPH','type'=>'select','list'=>$this->vI->filtrSelectList($select_list['price_tax_list'])],
            'user_id'=>['col'=>'user_id','title'=>'Pracovník','type'=>'select','list'=>$this->vI->filtrSelectList($select_list['users_list'])],
            'product_group_id'=>['col'=>'product_group_id','title'=>'Skupina','type'=>'select','list'=>$this->vI->filtrSelectList($select_list['groups_list_tree'])],
            
        
    //        'num'=>['col'=>'num','title'=>'Číslo','type'=>'text_like'],
  //          'code'=>['col'=>'code','title'=>'Kod','type'=>'text_like'],
//            'product_group_id'=>['col'=>'product_group_id','title'=>'Skupina','type'=>'select','list'=>$this->vI->filtrSelectList($select_list['product_groups_list'])],
            
            //'modified'=>['col'=>'modified','title'=>'Upraveno','type'=>'date'],
        ];
        //pr($this->request->data);
            
        if (isset($this->request->data['conditions'])){
            // pr($this->request->data['conditions']);die();
            $conditions = $this->vI->convertConditions($conditions,$this->request->data['conditions'],$this->filtration_defined);
            if (isset($this->request->data['conditions']['product_group_id']) && $this->request->data['conditions']['product_group_id'] > 0){
                //pr($this->request->data['conditions']['product_group_id']);die();
                $this->loadModel('ProductGroups');
                $group = $this->ProductGroups->get($this->request->data['conditions']['product_group_id']);
                //  pr($group);die();
                unset($this->request->data['conditions']['product_group_id']);
                unset($conditions['product_group_id']);
                $groupList = $this->ProductGroups->find('list',['keyField' => 'id', 'valueField' => 'id'])->where(['lft >='=>$group->lft,'rght <='=>$group->rght])->toArray();
                // pr($groupList);die();
                $conditions['product_group_id IN'] = $groupList;     
                 
            
            }

            if (isset($this->request->data['conditions']['order_id'])){
                $this->loadModel('Deadlines');
                $this->deadline_list = $this->Deadlines->deadlineList();
                $con = [
                    'order_id >='=>$this->deadline_list['all'][$this->request->data['conditions']['order_id']][0],
                    'order_id <='=>$this->deadline_list['all'][$this->request->data['conditions']['order_id']][1],

                ];
                $conditions = array_merge($conditions,$con);
                // unset($conditions['order_id >=']);
                unset($conditions['order_id']);
                //pr($this->deadline_list['all'][$this->request->data['conditions']['order_id']]);
            }
            // pr($conditions);die();
            
        }
        
        // $group = $this->ProductGroups->get(2);
        // pr($group);
        // pr($conditions);die();
        
       
        $posibility = [
            //0=>['link'=>'/api/status/Products/','title'=>__('Status'),'class'=>'fa-check-circle-o','type'=>'ajax','params'=>'status'],
            //1=>['link'=>'./edit/','title'=>__('Editovat'),'class'=>'fa-edit','type'=>'router','params'=>'edit'],
            //2=>['link'=>'/api/trash/Products/','title'=>__('Smazat'),'class'=>'fa-trash','type'=>'ajax','params'=>'trash','confirm'=>__('Opravdu smazat?')],
        ];
        
        $top_actions = [
            //0=>['link'=>'./edit/','title'=>__('Nová položka'),'class'=>'fa-edit','type'=>'router','params'=>'edit'],
        ];

        
        $fields = $this->vI->fieldsConvert($fields_defined);
        $fields[] = 'product_id';
        //pr($fields);die();
        //pr($conditions);
        
        $mapper = function ($data, $key, $mapReduce) {
            //$data->operating = (($data->operating == '')?0:1);
            //$data->problem = (($data->problem == '')?0:1);
            //$data->price_total = 0;
            
            $mapReduce->emit($data);  
        };

        $query = $this->Statistics->find()
            ->select($fields)
            ->where($conditions)
            
            ->mapReduce($mapper)
        ;

        //pr($conditions);
        //$data = $query;
        $data_list = $query->toArray();
        $result = [];
        $sumValue = [
            'count'=>0,
            'price'=>0,
            'price_without_tax'=>0,
            'price_total'=>0,
            'price_total_without_tax'=>0,
            
        ];
        $sum = [];
        foreach($fields AS $f){
            if (isset($sumValue[$f])){
                $sum[$f] = 0;
            } else {
                $sum[$f] = '';
            }
            
        }
        $sum['last_sum']=true;
        
        
        // pr($sum);die();
        foreach($data_list AS $k=>$d){
            if (!isset($result[$d->product_id])){
                $result[$d->product_id] = $d;
            } else {
                $result[$d->product_id]->count += $d->count;
                $result[$d->product_id]->price_total += $d->price_total;
                $result[$d->product_id]->price_total_without_tax += $d->price_total_without_tax;
                $result[$d->product_id]->created += $d->created;
            }
            $sumRow = $d;
            $item = json_decode(json_encode($d),true);
            foreach($item AS $kk=>$dd){
                if (isset($sum[$kk]) && is_integer($sum[$kk])){
                    $sum[$kk] += $d->$kk;
                } else {
                    // $sum[$kk] = '';
                }
            }
        }
        if (count($data_list)>0){
            $result[] = $sum;
        }
        // pr($result);die();
        $tmp = [];
        foreach($result AS $r){
            $tmp[] = $r;
        }
        $result = $tmp;
        //sort($result);
        //  pr($tmp);die();
        //pr($result);die(); 
        //pr($query->toArray());die();
        
        
        //$this->loadComponent('Paginator');
        //$data_list = $this->paginate($query);
        
        
        
        //$pagination = $this->vI->convertPagination();
        //pr($pagination['page']);
        $results = [
            'result'=>true,
            'data'=>$result,
            'data_count'=>count($result),
            'pagination'=>[],
            'table_th'=>$fields_defined,
            'filtration'=>array_values($this->filtration_defined),
            'select_list'=>$select_list,
            'posibility'=>$posibility,
            'top_actions'=>$top_actions,
            'conditions'=>(object) $conditions,
        ];
        
        $this->setJsonResponse($results);
    }



}
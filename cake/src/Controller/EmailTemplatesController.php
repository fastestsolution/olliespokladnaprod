<?php

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;

class EmailTemplatesController extends AppController{
    private $uses = ['EmailTemplates'];
    public $paginate = [
        'fields' => ['EmailTemplates.id', 'EmailTemplates.name', 'EmailTemplates.subject'],
        'order' => [
            'EmailTemplates.id' => 'asc'
        ]
    ];
    private $filtration = ['name'=>'text','subject'=>'text'];  
    
    public function initialize() {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Paginator');
    
    }
    
    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->loadModel($this->uses[0]);
    }

    public function rest( $action , $id = null){
         parent::rest($action, $id);
    }
    
    protected function index($settingsLoad = false){
        if($settingsLoad == true){
            parent::loadModuleSettings('email_templates');
        }
        
        $query = $this->{$this->uses[0]}->find();
        $query = parent::prepareFiltration($query, $this->filtration);

        $query->hydrate(false);
        $this->paginate($query);
        return $query ? $query->toArray() : []; //Execute query
    }

    protected function edit($id = null){
        if((int) $id){
            $item = $this->{$this->uses[0]}->find()->where(['id'=>$id])->first();
        }else{
            $item = $this->{$this->uses[0]}->newEntity($this->{$this->uses[0]}->getEmptyColumns());
        }
        
        $this->return['validations'] = $this->{$this->uses[0]}->getValidations();

        if(!empty($data = $this->getData())){
            if($this->{$this->uses[0]}->patchEntity($item, $data)){
                if(!empty($item->errors())){
                    throw new ValidationException(__('Zaslaná data nejsou validní'), $item->errors());
                }else{
                    $this->{$this->uses[0]}->save($item);
                }
            }else{
                throw new Exception(__('Error while saving'));
            }
        }
        
        return $item; //Execute query
    }
    
    protected function delete($id){
        $item = $this->{$this->uses[0]}->find()->where(['id'=>$id])->first();
       
        if($item){
            if($this->{$this->uses[0]}->delete($item)){
                return $id;
            }else{
                throw new Exception('Data errors');
            }
        }else{
            throw new Exception('Item not found');
        }
    }

}

<?php
namespace App\Controller;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
use Cake\Utility\Hash;



class ApisController  extends AppController
{

    public function index(){


	}

	public function node(){
		// pr($_SERVER);
		
		
		
		$ip = '127.0.0.1';
		$port = '8005';
		$fp = @fsockopen($ip, $port, $errno, $errstr, 0.1);
		if (!$fp) {
			$user_agent = $_SERVER['HTTP_USER_AGENT'];
			if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
				$nodeJsPath = 'websocket.bat';
			} else {
				$nodeJsPath = 'websocket.sh';
			}
			$ret = exec($nodeJsPath.' 2>&1', $out, $err);
			die(json_encode(['result'=>true,'data'=>$out]));
		
			//return false;
		} else {
			@fclose($fp);
			die(json_encode(['result'=>false]));
		}
		
	}
	/**
	 * Ověření zda jsou produkty pokladny akutální vzhledem k posledním úpravám produktů v Intranetu
	 */
	public function checkProducts(){
		$res = [];
		$res['result'] = false;
		$res['apiCount'] = 0;
		$this->getSettings();
		// pr($this->settings->data->system_id);
		$domain = API_URL.'uploaded/updated.txt';
		$domain2 = API_URL.'api/getStockQueues/'.$this->settings->data->system_id;
		// pr($domain2);
		$stockQueues = @file_get_contents($domain2, false, stream_context_create(array('http'=>
			array(
				'timeout' => 3,  //3 sekundy timeout požadavku
			)
		)));
		//pr($domain2);
		if($stockQueues){
			$sQueues = json_decode($stockQueues,true);
			// pr($sQueues);
			if ($sQueues['result'] == true){
				//$res['stocks']['result'] = true;
				foreach($sQueues['data'] AS $data){
					
					$res['dataList'][] = [
						'message' => 'Příjmout převodku',
						'function' => 'this.doConfirmStock('.$data['id'].')',
						'data' => $data,
						'icon' => 'fa-bell',
						'from_name' => $data['from_name'],
						'created' => date('d.m.Y H:s',strtotime($data['created'])),
					];
				}
				// $res['stocks']['data'] = $sQueues['data'];
				$res['apiCount'] += $sQueues['count'];
				$res['result'] = true;
			}
		}


		
		$lastUpdate = @file_get_contents($domain, false, stream_context_create(array('http'=>
			array(
				'timeout' => 3,  //3 sekundy timeout požadavku
			)
		)));
		if($lastUpdate !== false){
			$cashdeskLastProdUpdate = @file_get_contents( LOGS . 'last_products_update.txt', date('Y-m-d H:i:s')); //date('Y-m-d H:i:s', strtotime('2018-05-25 06:44:14'));
			if($cashdeskLastProdUpdate === false || $cashdeskLastProdUpdate < $lastUpdate){
				$res['dataList'][] = [
					'created' => date('d.m.Y H:s',strtotime($lastUpdate)),
					'message' => 'Import produktů',
					'icon' => 'fa-product-hunt',
					'function' => 'this.doImportProducts()',
				];
				$res['apiCount'] += 1;
				$res['result'] = true;
			}
		}
		if (isset($this->request->query['debug'])){
			pr($res);
		
		}

		$this->loadModel('Stocks');
		$this->loadModel('Orders');
        $res['queues'] = [
            'stocks' => $this->Stocks->getQueueSize(),
            'orders' => $this->Orders->getQueueSize()
        ];

		die(json_encode($res));
	}


	public function confirmStockPrevodka($id){
		//$url = API_URL.'api/confirmPrevodka/'.$id;
		$url = API_URL.'api/confirmStockTransfer/'.$id;
		$result = @file_get_contents($url, false, stream_context_create(array('http'=>
			array(
				'timeout' => 3,  //3 sekundy timeout požadavku
			)
		)));
		
		if ($result){
			//Vytvoreni noveho zaznamu do skladovych pohybu
			$result = json_decode($result, true);

			if(isset($result['result']) && $result['result'] && isset($result['data'])){
				$this->loadModel('Stocks');
				$stockEntity = $this->Stocks->newEntity($result['data']);
				$stockEntity->stock_type_id = 11; // Prevodka Plus
				$stockEntity->system_id = $this->system_id;
				$stockEntity->user_id = $this->loggedUser['id'];
				$stockEntity->user_name = $this->loggedUser['name'];
				unset($stockEntity->id);

				if($this->Stocks->save($stockEntity)){
					$res = true;
				}
			} 
		}
		die(json_encode(['result'=>$res,'message'=>($res ? 'Převodka byla přijmuta':'Chyba potvrzení')]));
	}


	public function checkPing(){
		
		function pingDomain($domain){
			// $ping = shell_exec('ping www.google.com');
			// pr($ping);
		
			$starttime = microtime(true);
			$file      = @fsockopen ($domain, 80, $errno, $errstr, 1);
			$stoptime  = microtime(true);
			$status    = 0;
			
			if (!$file) {
				// pr("ERROR: $errno - $errstr");
				$status = -1;  // Site is down
			} else {
				fclose($file);
				$status = ($stoptime - $starttime) * 1000;
				$status = floor($status);
			}
			
			 return $status;
		}
		$ping = pingDomain('www.ollies.cz');
		//$ping = 3000;
		//pr($ping);
		if ($ping == -1){

			die(json_encode(['result'=>false,'message'=>'Není připojení k internetu, nebo není dostupný web www.ollies.cz']));
		} else {
			if ($ping > 500 && $ping < 2000){
				die(json_encode(['result'=>true,'ping'=>$ping,'type'=>'show','message'=>'Máte prodlevy na rychlosti odezvy internetu, kontaktujte Vašeho poskytovatele. Aktuální odezva je: '.$ping.'ms']));
			} else if ($ping > 2000){
				die(json_encode(['result'=>false,'ping'=>$ping,'message'=>'Váš internet není dostupný (nebo má moc dlouhou odezvu)']));
				
			} else {
				die(json_encode(['result'=>true,'ping'=>$ping]));
			}
		}
	
	}

	// run node.js server
	public function runServer(){
		$this->autoRender = false;
		
		$script = "node escPosServer.js &"; 
		//pr(getcwd());
		//pr($script);die();
		//$script = "forever start escPosServer.js &"; 
        exec($script, $out);
        //pr(getcwd());
        //pr($out);
        if (isset($out[0]) && $out[0] == 'Server is listening'){
            die(json_encode(['result'=>true,'message'=>$out[0]]));
        } else {
            die(json_encode(['result'=>false,'message'=>(isset($out[0])?$out[0]:'Chyba spusteni node serveru')]));
            
        }
	}

	/**
	 * get products group
	 */
	public function getProductsGroup(){
		$this->getSettings();
		
		$this->settings->data->domain = strtr($this->settings->data->domain,['http'=>'https']);
		$curlUrl = $this->settings->data->domain.'/products_atr/category';
		//pr($curlUrl);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $curlUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
		// curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
		curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		curl_close ($ch);
		if (empty($result)){
			$result = [
				'result'=>false,
				'message'=>'Není spojení se serverem pro nahrání skupin produktů, zkontrolujte připojení k internetu a dostupnost www.chachar.cz',
			];
        
        } else {
			$urlProvozList = $this->settings->data->domain.'/api/provoz';
			//pr($urlProvozList);
			$options  = array('http' => array('user_agent' => 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13'));
			$context  = stream_context_create($options);
			$provoz_load = file_get_contents($urlProvozList, false, $context);
			//pr($provoz_load);
			if ($provoz_load){
				$this->loadModel('Settings');
				$resultDb = $this->Settings->updateAll(
					// ['operations'=>$provoz_load], // fields
					['nodelete'=>1,'operations'=>$provoz_load], // fields
					['id' => 1]
				);
				//{
					
					//pr($resultDb);
					//die(json_encode(['result'=>false,'message'=>'Chyba uložení nastavení']));
				//}
				//$provoz_list = json_decode($provoz_load);
			}
			//pr($provoz_list);
			//die();
			
			
			$resultGroup = json_decode($result);

			//pr($resultGroup);die();
			$saveGroup = [];
			$this->color_list = [
				1=>'#fcd1d1',
				2=>'#a8f7ee',
				3=>'#f9a7a7',
				4=>'#f9a7a7',
				5=>'#f7c2a8',
				6=>'#a8c5f7',
				7=>'#f6f7a8',
				8=>'#e6a8f7',
				9=>'#a8f7f4',
				10=>'#a8c2f7',
				11=>'#a8f7cb',
				12=>'#b6f7a8',
				13=>'#dff7a8',
			];
			$this->loadModel('ProductGroups');
			$saveGroup[0] = [
				'id'=>1,
				'name'=>'Pizzy 32',
				'order_num'=>1,
				'nodelete'=>1,
				'color'=>'#fce8e8',
			];
			$saveGroup[2] = [
				'id'=>2,
				'name'=>'Pizzy 45',
				'order_num'=>2,
				'nodelete'=>1,
				'color'=>'#f9a7a7',
			];
			$saveGroup[3] = [
				'id'=>3,
				'name'=>'Přídavky',
				'order_num'=>3,
				'nodelete'=>1,
				'color'=>'#6FF3B5',
			];
			$saveGroup[100] = [
				'id'=>1000,
				'name'=>'Akce',
				'order_num'=>1000,
				'nodelete'=>1,
				'color'=>'#F78491',
			];
			$disable_cats = [0,1,2,3,4,10];
			// pr($resultGroup);die();
			foreach($resultGroup AS $k=>$r){
				
				if (!in_array($k,$disable_cats)){
				$saveGroup[] = [
					//'id'=>$this->ProductGroups->getGroup($r->id),
					'id'=>$r->id,
					'order_num'=>$k,
					'nodelete'=>1,
					'web_id'=>$r->id,
					'name'=>$r->name,
					'color'=>(isset($this->color_list[$r->id])?$this->color_list[$r->id]:''),
				];
				}
				

			}
			
			$entities = $this->ProductGroups->newEntities($saveGroup);
			// pr($entities);die();
			foreach($entities AS $ent){
				if (!$this->ProductGroups->save($ent)){
					die(json_encode([
						'result'=>false,
						'message'=>'Chyba ulozeni kategorie',
					]));
				}
			}
			//pr($this->color_list);
			//pr($saveGroup);
		}
		
        
        die(json_encode(['result'=>true,'message'=>'Kategorie nahrány']));
		//pr($result);die();
	}

	private function getAddons(){
		$options  = array('http' => array('user_agent' => 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13'));
		$context  = stream_context_create($options);
		$addons = file_get_contents($this->settings->data->domain.'/products_atr/suroviny', false, $context);
		return ($addons);
	}

	/**
     * get products
     */
    public function getProducts(){
		$this->autoRender = false;
		$this->getSettings();
		
        $data = [];
        
        //pr($this->settings);
        if (!isset($this->settings->data->domain)){
            die(json_encode(['result'=>false,'message'=>'Není definována url pro online']));
        }
        
        $this->settings->data->domain = strtr($this->settings->data->domain,['http'=>'https']);
        $curlUrl = $this->settings->data->domain.'/products/json';
        //pr($curlUrl);
        
        $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $curlUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
        curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
		curl_setopt($ch, CURLOPT_TIMEOUT, 8);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		$result = curl_exec($ch);
		//pr('curl result');

		
		//pr($result);die();
		curl_close ($ch);
		if (empty($result)){
			$result = [
				'result'=>false,
				'message'=>'Není spojení se serverem pro nahrání produktů, zkontrolujte připojení k internetu a dostupnost www.chachar.cz',
			];
        
        } else {
			$result = [
				'result'=>true,
				'message'=>'Produkty byly importovány',
				'data'=>$this->convertProducts(json_decode($result)),
			];
		}
		
		//pr($result);
		die(json_encode($result));
	}

	private function catConvert($data){
		$pizza_cats = [
			1,2,3,4,5
		];
		$convert_cats = [
			11=>8, // napoje

		];
		if (in_array($data->cat_id,$pizza_cats) && strpos($data->code,'A') == true){
			$cat_id = 1;
		}
		else if (in_array($data->cat_id,$pizza_cats) && strpos($data->code,'B') == true){
			$cat_id = 2;
		}
		else {
			if (isset($convert_cats[$data->cat_id])){
				$cat_id = $convert_cats[$data->cat_id];
				
			} else {
				$cat_id = $data->cat_id;
				
			}
		}
		return $cat_id;
		//pr($data);die();
	}

	private function convertProducts($data){
		$products = [];
		

		$products[] = [
			'id'=>1000,
			'name'=>'Doprava',
			'code'=>'dop',
			'web_id'=>1000,
			'product_group_id'=>1000,
			'web_group_id'=>1000,
			'num'=>1000,
			'price'=>19,
			'nodelete'=>1,
		];

		$this->loadModel('Products');
		foreach($data AS $d){
			if ($d->cat_id != 100){
			$products[] = [
				'id'=>$this->Products->getProduct($d->code),
				'name'=>$d->name,
				'code'=>$d->code,
				'web_id'=>$d->id,
				'product_group_id'=>$this->catConvert($d),
				'web_group_id'=>$d->cat_id,
				'num'=>$d->num,
				'price'=>$d->price,
				'nodelete'=>1,
			];
			}
			//pr($products);die();
		}

		// nahrani pridavku
		$addon = json_decode($this->getAddons());
		// pr($addon);die();
		foreach($addon AS $ad){
			$products[] = [
				'id'=>$ad->id,
				'name'=>$ad->name,
				'code'=>$ad->kod,
				'web_id'=>$ad->id,
				'product_group_id'=>3,
				'web_group_id'=>$ad->group_id,
				'num'=>$ad->id,
				'price'=>$ad->cena,
				'nodelete'=>1,
			];
		}

		$entities = $this->Products->newEntities($products);
		//pr($entities);die();
		foreach($entities AS $ent){
			if (!$this->Products->save($ent)){
				die(json_encode([
					'result'=>false,
					'message'=>'Chyba ulozeni produktu',
				]));
			}
		}
		return $products;
		//pr($products);die();
	}

	public function getDeliveryOrders(){
		$this->getSettings();
		$this->settings->data->domain = strtr($this->settings->data->domain,['http'=>'https']);
		
		$curlUrl = $this->settings->data->domain.'/api/deliveryOrders/'.$this->settings->data->system_id.'?new';
		//pr($curlUrl);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $curlUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
        curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		curl_close ($ch);
		//pr($result);
		if (empty($result)){
			$result = [
				'result'=>false,
				'message'=>'Není spojení se serverem pro nahrání stavu objednávek řidičů, zkontrolujte připojení k internetu a dostupnost www.chachar.cz',
			];
        
        } else {
			//$result = json_decode($result,true);
			die($result);
		}
		die(json_encode(['result'=>true]));
	}


	/**
     * get online orders
     */
	public function getOnline(){
        $this->autoRender = false;
        $data = [];
        $this->getSettings();
        //pr($this->settings);
        if (!isset($this->settings->data->domain)){
            die(json_encode(['result'=>false,'message'=>'Není definována url pro online']));
        }
        
        $this->settings->data->domain = strtr($this->settings->data->domain,['http'=>'https']);
        $curlUrl = $this->settings->data->domain.'/export_order/'.$this->settings->data->system_id.'?json2&newPokladna';
        //pr($curlUrl);
        
        $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $curlUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
        curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		curl_close ($ch);
		//pr('curl result');
        //pr($result);
        if (empty($result)){
			$result = [
				'result'=>false,
				'message'=>'Není spojení se serverem pro nahrání online objednávek, zkontrolujte připojení k internetu a dostupnost www.chachar.cz',
			];
        
        } else {
            $result = json_decode($result,true);
			if (count($result['data']) > 0){
				$dataOrders = $this->convertOnline($result['data']);
				//pr($aaa);
				if (count($dataOrders) > 0){
					$result = [
						'result'=>true,
						'online'=>$this->isOnline,
						'data'=>$dataOrders,
					];
				} else {
					$result = [
						'result'=>false,
						'message'=>'Nenalezeny žádné online objednávky'
					];
				
				}
            } else {
                $result = [
                    'result'=>false,
                    'data'=>'Nenalezeny žádné online objednávky',
                ];
            }
			
		}
		if (isset($this->request->query['debug'])){
			pr($result);
		}
        //pr($result);

        die(json_encode($result));
    }


	private function convertOnline($data){
        $orders = [];
        //pr($data);
        $this->loadModel('Orders');
        $this->loadModel('Clients');
        foreach($data AS $d){
			if (!$this->Orders->getOrderId($d['RozvozXml']['id'])){
			// $d['RozvozXml']['telefon'] = '731956030';
            // $d['RozvozXml']['telefon_pref'] = '420';
            $phone = $d['RozvozXml']['telefon'];
            $phonePrefix = strtr($d['RozvozXml']['telefon_pref'],['+'=>'']);
            //pr($data);
            $clientData = $this->Clients->getClient($phonePrefix,$phone);
			//pr($clientData);
			// pr($clientData['client_addresses']);
            $address = [];
            if ($clientData['client_addresses']){
                foreach($clientData['client_addresses'] AS $adr){
                    if ($adr->street == $d['RozvozXml']['ulice'] && $d['RozvozXml']['mesto'] == $adr->city){
                        $addr_id = $adr->id;
                    }
                }
            }
            $address[] = [
                'id' => (isset($addr_id)?$addr_id:''), 
                'street' => $d['RozvozXml']['ulice'], 
                'city' => $d['RozvozXml']['mesto'], 
                'zip' => '', 
                'lat' => $d['RozvozXml']['lat'], 
                'lng' => $d['RozvozXml']['lng'], 
                
            ];
            // slouceni nove adresy a ulozenych
            if ($clientData['client_addresses']){
                $address = array_merge($address,$clientData['client_addresses']);
            }    
            
            $client = [
                'name'=>$d['RozvozXml']['name'],
                'first_name'=>$d['RozvozXml']['jmeno'],
                'last_name'=>$d['RozvozXml']['prijmeni'],
                'email'=>$d['RozvozXml']['email'],
                'phone'=>$phone,
                'phone_pref'=>$phonePrefix,
                'client_addresses'=>$address,
                'orders'=>(isset($clientData->orders)?$clientData->orders:''),
			] ;
			// pr($clientData);
            if ($clientData){
                $client['id'] = $clientData->id;
            }

			$this->order_items = [];
			$this->loadModel('Products');
            foreach($d['RozvozXml']['products'] AS $prod){
                $product = $this->Products->findProduct($prod['code']);
				if ($product){
					$this->order_items[] = [
						'name'=>$prod['name'],
						'count'=>$prod['ks'],
						'code'=>$prod['code'],
						'price'=>$prod['price'],
						'product_group_id'=>$product->product_group_id,
						'web_group_id'=>$product->web_group_id,
						'product_id'=>$product->id,
						'num'=>$product->code,
						'id'=>$product->id,
					];
				}
            }
			if (!empty($this->order_items)){
				$orders[] = [
					'web_id'=>$d['RozvozXml']['id'],
					'source_id'=>2,
					'street'=>$d['RozvozXml']['ulice'],
					'city'=>$d['RozvozXml']['mesto'],
					'note'=>$d['RozvozXml']['poznamka'],
					'lat'=>$d['RozvozXml']['lat'],
					'lng'=>$d['RozvozXml']['lng'],
					'transfer_id'=>$d['RozvozXml']['predano_id'],
					'payment_id'=>$d['RozvozXml']['platba_type'],
					'gopay_id'=>$d['RozvozXml']['paymentSessionId'],
					'dj_id'=>$d['RozvozXml']['dj_id'],
					'xml_type_id'=>$d['RozvozXml']['xml_type'],
					'client'=>$client,
					'order_items'=>$this->order_items
				];
			}
			$this->isOnline = $d['RozvozXml']['online'];
			
			}
            //pr($orders);die();
            
		}
		
		return $orders;
        //$save_entity = $this->Orders->newEntity($saveData,['associated' => ["Clients"=>['validate' => 'default','accessibleFields'=>['id' => true]], "Clients.ClientAddresses","OrderItems"]]);
       
    }


	public function test(){
		$session = $this->request->session();
		$session->write('testa', 'en');
		
		die('a');
	}	

    public function login(){
		$session = $this->request->session();
		$test = $session->read('testa');
		//$this->Session->write('test','a');
		//pr($test);
		pr($_SESSION);
		phpinfo();
		//$this->Session->write('a',['a'=>'b']);
		die('a');
		/*
		$results = [
			'result'=>true,
			'message'=>__('Přihlášení v pořádku'),
			'data'=>[
				'user'=>[
					'hash'=>'abcdaa',
					'id'=>1,
					'name'=>'Fastest',
				]
			]
		];
		$this->setJsonResponse();
		$this->set([
			'results' => $results,
			'_serialize' => 'results'
		]);
		*/
	}

	public function sendReport(){
		//pr($_POST);
		// pr($this->request);die();
		//$_POST = json_decode($_POST);
		$data = $this->request->data['saveData'];
		//pr($data);
		
		$ch = curl_init();
        $post = $data;
        //pr($post);
		$curlUrl = API_URL.'api/saveMessage/';
        curl_setopt($ch, CURLOPT_URL, $curlUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec ($ch);
		//pr('curl result');
		if (empty($result)){
			$result = [
				'result'=>false,
				'message'=>'Není spojení se serverem pro odeslání hlášení, zkontrolujte připojení k internetu a dostupnost www.chachar.cz',
			];
		} else {
            //pr($result);
			$result = json_decode($result,true);
            if (json_encode($result) == null){
                pr($result);
            }
        }

		die(json_encode(['result'=>true,'message'=>'Chyba poslána na technickou podporu']));
	}


	public function sendError(){
		
		//pr($_POST);
		$_POST = json_decode($_POST['data'],true);
		// if (!isset($_POST['error']) || $_POST['error'] == '[object ProgressEvent]'){
		// 	die(json_encode(['result'=>false,'message'=>'Nelze poslat na podporu']));
		// }
		//pr($_POST);die();
		if (!isset($_POST['error'])){
			die(json_encode(['result'=>false,'message'=>'Nelze poslat na podporu']));
		}
		$data = $_POST;
		//pr($data);die();
		$this->autoRender = false;
		$to      = 'test@fastest.cz';
		$subject = 'Chyba pokladny';
		$message = 'hello';
		$headers = 'From: podpora@fastest.cz' . "\r\n" .
			'Reply-To: podpora@fastest.cz' . "\r\n" .
			'X-Mailer: PHP/' . phpversion();
		
		//mail($to, $subject, $message, $headers);
		
		$this->loadModel('Logs');
		$saveData = $this->Logs->newEntity([
			'system_id'=>$data['system_id'],
			'url'=>$data['url'],
			'message'=>$data['message'],
			'error'=>substr($data['error'],0,3000),
		]);
		//pr($saveData);die();
		$this->Logs->save($saveData);
	
		$this->Logs->deleteAll(['created <' => date('Y-m-d 00:00:00', strtotime("-3 days"))]);

		$ch = curl_init();
        $post = $data;
        //pr($post);
		$curlUrl = API_URL.'api/saveLog/';
        curl_setopt($ch, CURLOPT_URL, $curlUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec ($ch);
		//pr('curl result');
		if (empty($result)){
			$result = [
				'result'=>false,
				'message'=>'Není spojení se serverem pro uložení logu chyby, zkontrolujte připojení k internetu a dostupnost www.chachar.cz',
			];
		} else {
            //pr($result);
			$result = json_decode($result,true);
            if (json_encode($result) == null){
                pr($result);
            }
        }

		die(json_encode(['result'=>true,'message'=>'Chyba poslána na technickou podporu']));
	}

	

	/**
	 * zobrazeni reportu chyb pokladny
	 */
	public function showLogs(){
		$this->loadModel('Logs');
		$data = $this->Logs->find()
		->order('id DESC')
		->select(['created','message','url'])
		->limit(10)
		->map(function($r){
			$r->created = $r->created->format('d.m.Y H:i:s');
			//pr($r);
			return $r;
		})	
		->toArray();
		if (count($data)>0){
			die(json_encode(['result'=>true,'data'=>$data]));
			
		} else {
			die(json_encode(['result'=>false,'message'=>'Nenalezeny žádné chyby']));
			
		}
	}

	public function getMaps($system_id){
		
		$ch = curl_init();
        $post = [];
        //pr($post);
		$curlUrl = API_URL.'api/map_areas/getMaps/'.$system_id;
        curl_setopt($ch, CURLOPT_URL, $curlUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec ($ch);
		//pr('curl result');
		if (empty($result)){
			$result = [
				'result'=>false,
				'message'=>'Není spojení se serverem pro nahrani map rozvozu, zkontrolujte připojení k internetu a dostupnost www.chachar.cz',
			];
		} else {
            //pr($result);
			$result = json_decode($result,true);
            if (json_encode($result) == null){
                pr($result);
            }
        }
		//pr($result);
		require_once(ROOT . DS .'src/polygon.php');
		// Points to encode
		$maps_result = [];
		if (isset($result['data'])){
			foreach($result['data'] AS $k=>$data){
				$points_load = \Polyline::decode($data['coords']);
				$points_load = \Polyline::pair($points_load);
				
				$points = [];
				foreach($points_load AS $kpoint=>$point){
					$points[] = [
						'lat'=>$point[0],
						'lng'=>$point[1],
					];
				}
				unset($result['data'][$k]['coords']);
				$result['data'][$k]['id'] = $k; 
				$result['data'][$k]['path'] = $points; 
				$maps_result['data'][] = $result['data'][$k];
				 
			}
			$result['data'] = $maps_result['data'];
		}
		//sort($result);
		//pr($result);
		// $points = array(
		// 		array(41.89084,-87.62386),
		// 		array(41.89086,-87.62279),
		// 		array(41.89028,-87.62277),
		// 		array(41.89028,-87.62385),
		// 		array(41.89084,-87.62386)
		// 	);
		// $path = '}ijoHihfnBxmTbsOk}N}z\_~@gv@ps@irYarHdcJqbHfyM';
		// $encoded = \Polyline::encode($points);
		
		die(json_encode($result));
	}


	public function switchOnline(){
		
        $this->getSettings();
		$curlUrl = $this->settings->data->domain.'/api/statusOnlineChange/'.$this->settings->data->system_id;
		//pr($curlUrl);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $curlUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
		// curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
		curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		curl_close ($ch);

		if (empty($result)){
			$result = [
				'result'=>false,
				'message'=>'Není spojení se serverem pro nahrání skupin produktů, zkontrolujte připojení k internetu a dostupnost www.chachar.cz',
			];
        
        } else {
			//pr($result);
			$result = json_decode($result);
		}
		die(json_encode($result));
	}
}


<?php
namespace App\Controller;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;
use Cake\Cache\Cache;
use Cake\I18n\Time;
use App\Component\vIComponent;
use Cake\Datasource\ConnectionManager;

class ProductGroupsController extends AppController{
    
    /**
     * vytvoreni select listu
     */
    public function getSelectList($check=true){
        if ($check == false || isset($this->request->query['firstLoad'])){
            
            return $select_list = [
                'no_yes'=>$this->no_yes,
            ];
        } else {
            return false;
        }  
        
        
    }

    /**
     * INDEX orders
     */
    public function index()
    {
        $this->loadComponent('vI');

        $select_list = $this->getSelectList();
        //pr($select_list);die();
        $conditions = [];
        $fields_defined = [
            0=>['col'=>'id','title'=>'ID','type'=>'text'],
            1=>['col'=>'name','title'=>'Název','type'=>'text'],
            2=>['col'=>'order_num','title'=>'Pořadí','type'=>'text'],
            3=>['col'=>'created','title'=>'Vytvořeno','type'=>'datetime'],
            //3=>['col'=>'zdroj_id','title'=>'Zdroj','type'=>'list','list_data'=>'zdroj_list'],
        ];

        $this->filtration_defined = [
            'id'=>['col'=>'id','title'=>'ID','type'=>'text'],
            'name'=>['col'=>'name','title'=>'Jméno','type'=>'text_like'],
            //'created'=>['col'=>'created','title'=>'Vytvořeno','type'=>'date_range'],
            //'modified'=>['col'=>'modified','title'=>'Upraveno','type'=>'date'],
        ];
        
        if (isset($this->request->data['conditions'])){
            //pr($this->request->data['conditions']);
            $conditions = $this->vI->convertConditions($conditions,$this->request->data['conditions'],$this->filtration_defined);
            //pr($conditions);die();
        }
        
       
        $posibility = [
            0=>['link'=>'/api/status/ProductGroups/','title'=>__('Status'),'class'=>'fa-check-circle-o','type'=>'ajax','params'=>'status'],
            1=>['link'=>'./edit/','title'=>__('Editovat'),'class'=>'fa-edit','type'=>'router','params'=>'edit'],
            2=>['link'=>'/api/trash/ProductGroups/','title'=>__('Smazat'),'class'=>'fa-trash','type'=>'ajax','params'=>'trash','confirm'=>__('Opravdu smazat?')],
        ];
        
        $top_actions = [
            0=>['link'=>'./edit/','title'=>__('Nová položka'),'class'=>'fa-edit','type'=>'router','params'=>'edit'],
        ];

        
        $fields = $this->vI->fieldsConvert($fields_defined);
        //pr($fields);die();
        
        $mapper = function ($data, $key, $mapReduce) {
            //$data->operating = (($data->operating == '')?0:1);
            //$data->problem = (($data->problem == '')?0:1);

            
            $mapReduce->emit($data);  
        };

        $query = $this->ProductGroups->find()
            ->select($fields)
            ->where($conditions)
            ->cache(function ($query) {
				return 'product_group_data-' . md5(serialize($query->clause('where')));
            })
            ->mapReduce($mapper)
        ;

        
        if (isset($this->request->query['firstLoad'])){
            $this->request->query['sort'] = 'order_num';
            $this->request->query['direction'] = 'ASC';
        }
        //$data = $query;
        //pr($query->toArray());die();
        
        
        $this->loadComponent('Paginator');
        $data_list = $this->paginate($query);
        
        
        
        $pagination = $this->vI->convertPagination();
        //pr($pagination['page']);
        $results = [
            'result'=>true,
            'data'=>$data_list->toArray(),
            'data_count'=>count($data_list->toArray()),
            'pagination'=>$pagination,
            'table_th'=>$fields_defined,
            'filtration'=>array_values($this->filtration_defined),
            'select_list'=>$select_list,
            'posibility'=>$posibility,
            'top_actions'=>$top_actions,
            'conditions'=>(object) $conditions,
        ];
        
        $this->setJsonResponse($results);
    }

    public function loadTree(){
        
        $data = $this->ProductGroups->groupListTreeEdit();

        die(json_encode(['result'=>true,'data'=>$data]));
    }

    private function genList($menu){
        foreach($menu AS $m){
            $this->treeList[$m['id']] = $m['id'];
            if (isset($m['children'])){
                $this->genList($m['children']);
            }
        }
        return $this->treeList;
    }

    
    public function saveTree(){
		//pr($this->request->data);die('a');
		// $connection = ConnectionManager::get('default');
		// $results = $connection->execute('TRUNCATE TABLE product_groups'); 
		
		$menus = $this->request->data['data'];
        $this->treeList = [];
        $this->genList($menus);
        
        $currenList = $this->ProductGroups->find('list',['keyField' => 'id','valueField' => 'id'])->toArray();
        $deleteIds = array_diff($currenList,$this->treeList);

        if (isset($deleteIds)){
            foreach($deleteIds as $removeId){
                if($removeId){
                    $remItem = $this->ProductGroups->get($removeId);
                    if($remItem){
                        $this->ProductGroups->removeFromTree($remItem);
                        $this->ProductGroups->delete($remItem);
                    }
                }
            }
        }

        // pr($diff);
        // pr($currenList);
        // pr($this->treeList);
        // pr($menus);die();
        
        //$menus[0]['name'] = 'test testa';
		foreach($menus as $menu){ 
			$this->saveNode($menu,1);
		}
		Cache::clear(false);
		die(json_encode(['result'=>true]));
	}
    
    
    private function findLast(){
        $last = $this->ProductGroups->find()
        ->order('id DESC')
        ->toArray();
        pr($last);die('a');
        return $last->id + 1;
    }

	private function saveNode($node, $level){
        //pr($node);
        //pr($node);die('a');
		$entity = $this->ProductGroups->newEntity([
            'name'=>$node['name'],
            'level'=>$level,
            'parent_id'=>( isset($node['parent_id']) && !empty($node['parent_id']) ? $node['parent_id'] : null)
        ]);
        if(isset($node['id']) && $node['id']){
                $entity->id = $node['id'];
                // $entity->id = null;
            //}
        } else {
            //$node['id'] = $this->findLast();
            $entity->id = '';
           //pr($entity);die();
        }
        //pr($ProductGroups);
        //pr($entity);die();
        // pr($entity);
		$this->ProductGroups->save($entity);

        if(isset($node['children'])){
            foreach($node['children'] as $subnode){
                $subnode['parent_id'] = $entity->id;
                $this->saveNode($subnode, $level + 1);
            }
        }
    } 

    /**
     * editace
     */
    public function edit($id=null){
        $select_list = $this->getSelectList(false);
        $this->loadComponent('vI');
        //pr($select_list);die();
        
        // save data
        if (isset($this->request->data['saveData'])){
            $saveData = $this->request->data['saveData'];
            $saveData = $this->vI->convertTime($saveData);
            $save_entity = $this->ProductGroups->newEntity($saveData);
            //pr($save_entity); die();
            
            $this->vI->checkErrors($save_entity);

            if (!$resultDb = $this->ProductGroups->save($save_entity)){
                $results = [
                    'result'=>false,
                    'message'=>__('Chyba uložení')
                ];    
            } else {
                $results = [
                    'result'=>true,
                    'message'=>__('Uloženo'),
                    'data'=>$resultDb
                ];  
            }
            
            

            $this->setJsonResponse($results);
        
        // load data
        } else {
            $defaultValues = [
                'nodelete'=>0,
            ];
            if ($id != null){
                // pokud je edit
                if ($id != 'new'){
                    $conditions = ['id'=>$id];
                    $data = $this->ProductGroups->find()
                    ->select()
                    ->where($conditions)
                    ->first()
                ;
                // pokud je nova polozka
                } else {
                   
                    $data = $this->vI->emptyEntity('ProductGroups'); 
                    
                }
                $data = $this->vI->convertLoadData($data,$defaultValues);        
                //pr($data);die();
                $validations = $this->vI->getValidations('ProductGroups');
            } else {

            }

            $results = [
                'result'=>true,
                'validations'=>(isset($validations)?$validations:null),
                'data'=>(isset($data)?$data:''),
                'select_list'=>$this->vI->SelectList($select_list),
            ];  
            //pr($results);die();  
            $this->setJsonResponse($results);
        }
        $this->clearCache('product_group_data');
    }


    
}
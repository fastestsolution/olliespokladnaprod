<?php
namespace App\Controller;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Exception\Exception;
use Cake\View\Helper\PaginatorHelper;
use App\Exception\ValidationException;
use Cake\Cache\Cache;
use Cake\I18n\Time;
use App\Component\vIComponent;

class ProductsController extends AppController{
    
    /**
     * vytvoreni select listu
     */
    public function getSelectList($check=true){
        if ($check == false || isset($this->request->query['firstLoad'])){
            $this->loadModel('ProductGroups');
            $this->product_groups_list = $this->ProductGroups->groupListTree();
            $this->groups_list = $this->ProductGroups->groupList();
            $this->groups_list_tree = $this->ProductGroups->groupTreeList();
            //pr($this->product_groups_list);die();
            return $select_list = [
                'group_trzba_list'=>$this->group_trzba_list,
                'price_tax_list_conf'=>$this->price_tax_list_conf,
                'price_tax_list'=>$this->price_tax_list,
                'no_yes'=>$this->no_yes,
                'product_groups_list'=>$this->groups_list_tree,
                'groups_list'=>$this->groups_list,
            ];
        } else {
            return false;
        }  
        
        
    }

    /**
     * INDEX orders
     */
    public function index()
    {
        $this->loadComponent('vI');

        $select_list = $this->getSelectList();
        //pr($select_list);die();
        $conditions = [];
        // $conditions = ['id'=>9];
        $fields_defined = [
            0=>['col'=>'id','title'=>'ID','type'=>'text'],
            1=>['col'=>'name','title'=>'Název','type'=>'text'],
            2=>['col'=>'product_group_id','title'=>'Skupina','type'=>'list','list_data'=>$select_list['product_groups_list']],
            3=>['col'=>'price','title'=>'Cena','type'=>'text'],
            4=>['col'=>'price2','title'=>'Cena zam.','type'=>'text'],
            5=>['col'=>'tax_id','title'=>'DPH','type'=>'list','list_data'=>$select_list['price_tax_list']],
            //6=>['col'=>'amount','title'=>'Gramáž','type'=>'text'],
            6=>['col'=>'code','title'=>'Kod','type'=>'text'],
            7=>['col'=>'num','title'=>'Číslo','type'=>'text'],
            8=>['col'=>'created','title'=>'Vytvořeno','type'=>'datetime'],
            9=>['col'=>'group_trzba_id','title'=>'Skupina tržba','type'=>'list','list_data'=>$select_list['group_trzba_list']],
            //3=>['col'=>'zdroj_id','title'=>'Zdroj','type'=>'list','list_data'=>'zdroj_list'],
        ];

        $this->filtration_defined = [
            'id'=>['col'=>'id','title'=>'ID','type'=>'text'],
            'name'=>['col'=>'name','title'=>'Jméno','type'=>'text_like'],
            'num'=>['col'=>'num','title'=>'Číslo','type'=>'text_like'],
            'code'=>['col'=>'code','title'=>'Kod','type'=>'text_like'],
            'product_group_id'=>['col'=>'product_group_id','title'=>'Skupina','type'=>'select','list'=>$this->vI->filtrSelectList($select_list['product_groups_list'])],
            'group_trzba_id'=>['col'=>'group_trzba_id','title'=>'Skupina tržba','type'=>'select','list'=>$this->vI->filtrSelectList($select_list['group_trzba_list'])],
            
            //'created'=>['col'=>'created','title'=>'Vytvořeno','type'=>'date_range'],
            //'modified'=>['col'=>'modified','title'=>'Upraveno','type'=>'date'],
        ];
        
        if (isset($this->request->data['conditions'])){
            //pr($this->request->data['conditions']);
            $conditions = $this->vI->convertConditions($conditions,$this->request->data['conditions'],$this->filtration_defined);
            //pr($conditions);die();
        }
        
       
        $posibility = [
            0=>['link'=>'/api/status/Products/','title'=>__('Status'),'class'=>'fa-check-circle-o','type'=>'ajax','params'=>'status'],
            1=>['link'=>'./edit/','title'=>__('Editovat'),'class'=>'fa-edit','type'=>'router','params'=>'edit'],
            2=>['link'=>'/api/trash/Products/','title'=>__('Smazat'),'class'=>'fa-trash','type'=>'ajax','params'=>'trash','confirm'=>__('Opravdu smazat?')],
        ];
        
        $top_actions = [
            0=>['link'=>'./edit/','title'=>__('Nová položka'),'class'=>'fa-edit','type'=>'router','params'=>'edit'],
            1=>['link'=>'./api/products/importProducts/','title'=>__('Importovat produkty'),'class'=>'fa-edit','type'=>'ajax','params'=>'importProducts'],
        ];


        if (isset($conditions['product_group_id']) && $conditions['product_group_id']>0){
            $this->loadModel('ProductConnects');
            $product_ids_load = $this->ProductConnects->find()->where(['product_group_id'=>$conditions['product_group_id']])->toArray();
            unset($conditions['product_group_id']);
            if ($product_ids_load){
                $product_ids = [];
                foreach($product_ids_load AS $load){
                    $product_ids[] = $load->product_id;
                }
                if (!empty($product_ids)){
                    $conditions['id IN'] = $product_ids;
                }
            }
            
        }
        
        $fields = $this->vI->fieldsConvert($fields_defined);
        //pr($fields);die();
        
        $mapper = function ($data, $key, $mapReduce) {
            //$data->operating = (($data->operating == '')?0:1);
            //$data->problem = (($data->problem == '')?0:1);

            
            $mapReduce->emit($data);  
        };

        $query = $this->Products->find()
            ->select($fields)
            ->where($conditions)
            ->cache(function ($query) {
				return 'product_data-' . md5(serialize($query->clause('where')));
            })
            ->mapReduce($mapper)
        ;

        
        //$data = $query;
        //pr($query->toArray());die();
        
        
        $this->loadComponent('Paginator');
        $data_list = $this->paginate($query);
        
        
        
        $pagination = $this->vI->convertPagination();
        //pr($pagination['page']);
        $results = [
            'result'=>true,
            'data'=>$data_list->toArray(),
            'data_count'=>count($data_list->toArray()),
            'pagination'=>$pagination,
            'table_th'=>$fields_defined,
            'filtration'=>array_values($this->filtration_defined),
            'select_list'=>$select_list,
            'posibility'=>$posibility,
            'top_actions'=>$top_actions,
            'conditions'=>(object) $conditions,
        ];
        
        $this->setJsonResponse($results);
    }


    /**
     * kontrola existence kodu
     */
    private function checkCode(){
        //pr($this->request);
        $con = [
            'code'=>$this->request->data['saveData']['code'],
        ];
        if (!empty($this->request->data['saveData']['id'])){
            $con['id !='] = $this->request->data['saveData']['id'];
        }

        $find = $this->Products->find()
        ->select([])
        ->where($con)
        ->first();
        if ($find){
            $results = [
                'result'=>false,
                'message'=>__('Kód produktu je již použit'),
            ];
            die(json_encode($results));  
    
        }
    }

    /**
     * import produktu z cloud
     */
    public function importProducts(){	
        $this->getSettings();
        
        $post = [];
        //pr($post);
        $errors = [];
        $curlUrl =  API_URL . 'api/exportProducts/'.$this->settings->data->system_id;

		$result = $this->conCurl($curlUrl,$post);
        
        if (!$result){
            die(json_encode(['result'=>false,'message'=>'Chyba stazeni produktu']));
        } else {
            $data = json_decode($result,true);
            /*foreach($data as $d){
                if(is_array($d)){
                    pr($this->Products->newEntity($d, ['validate'=> false]));
                }
            } 
            die();*/
            if ($data && $data['result'] == true){
                if(isset($data['branches'])){
                        $this->loadModel('Branches');
                        $this->Branches->truncateTable();
                        $entitiesGroup = $this->Branches->newEntities($data['branches'], ['validate'=> false]);
      
                        foreach($entitiesGroup AS $entity){
                            try { 
                                $this->Branches->save($entity);
                            } catch (\Exception $e) {
                                if(!isset($errors['branches'])){
                                    $errors['branches'] = [];
                                }
                                $errors['branches'][] = $e->getMessage();
                                //die(json_encode(['result'=>false,'message'=>$e]));
                            }
                        }
                }

                if (isset($data) && $data['result'] == true){
                    if(isset($data['product_recipes'])){
                        $this->loadModel('ProductRecipes');
                        $this->ProductRecipes->truncateTable();
                        $entitiesGroup = $this->ProductRecipes->newEntities($data['product_recipes'], ['validate'=> false]);
                    
                        foreach($entitiesGroup AS $entity){
                            try {   
                                $this->ProductRecipes->save($entity);
                            } catch (\Exception $e) {
                                if(!isset($errors['product_recipes'])){
                                    $errors['product_recipes'] = [];
                                }
                                $errors['product_recipes'][] = $e->getMessage();
                                //die(json_encode(['result'=>false,'message'=>$e]));
                            }
                        }
                    }
                }


                if(isset($data['groups'])){
                    $this->loadModel('ProductGroups');
                    $this->ProductGroups->truncateTable();
                    $entitiesGroup = $this->ProductGroups->newEntities($data['groups'], ['validate'=> false]);
                    
                    foreach($entitiesGroup AS $entity){
                        try { 
                            $this->ProductGroups->save($entity);

                            
                        } catch (\Exception $e) {
                            if(!isset($errors['groups'])){
                                $errors['groups'] = [];
                            }
                            $errors['groups'][] = $e->getMessage();
                            //die(json_encode(['result'=>false,'message'=>$e]));
                        }
                    }
                }
                
 

                if (isset($data['addons'])){

                    $this->loadModel('ProductAddons');
                    $this->ProductAddons->truncateTable();
                    $entitiesAddons = $this->ProductAddons->newEntities($data['addons'], ['validate'=> false]);
                    
                    foreach($entitiesAddons AS $entity){
                        try { 
                            $this->ProductAddons->save($entity);

                            
                        } catch (\Exception $e) {
                            if(!isset($errors['addons'])){
                                $errors['addons'] = [];
                            }
                            $errors['addons'][] = $e->getMessage();
                            //die(json_encode(['result'=>false,'message'=>$e]));
                        }
                        
                    }
                    
                    $this->loadModel('ProductConnAddons');
                    $this->ProductConnAddons->truncateTable();
                    $entitiesConnAddons = $this->ProductConnAddons->newEntities($data['con_addons'], ['validate'=> false]);
                    
                    foreach($entitiesConnAddons AS $entity){
                        try { 
                            $this->ProductConnAddons->save($entity);

                            
                        } catch (\Exception $e) {
                            if(!isset($errors['con_addons'])){
                                $errors['con_addons'] = [];
                            }
                            $errors['con_addons'][] = $e->getMessage();
                            //die(json_encode(['result'=>false,'message'=>$e]));
                        }
                        
                    }
                }

                if(isset($data['data'])){
                    $this->loadModel('Products');
                    $this->Products->truncateTable();

                    $this->loadModel('ProductConnects');
                    $this->ProductConnects->truncateTable();

                    // $entities = $this->Products->newEntities($data['data'], ['validate'=> false]);
						
                    //  pr($entities);die();
                    foreach($data['data'] AS $item){
                        try { 
							$entity = $this->Products->newEntity($item, ['validate'=> false]);
							if(isset($item['is_mirror']) && $item['is_mirror']){
								$entity->is_mirror = 1;
							}
                            $this->Products->save($entity);
                            
                        } catch (\Exception $e) {
                            if(!isset($errors['products'])){
                                $errors['products'] = [];
                            }
                            $errors['products'][] = $e->getMessage();
                        // die(json_encode(['result'=>false,'message'=>$e]));
                        }
                        
                    }
                }

                if(!empty($errors)){
                    pr($errors);
                    $errs = '';
                    foreach($errors as $section => $er){
                        $errs .= $section . PHP_EOL;
                        $errs . implode(  PHP_EOL , $er);
                    }
                    @file_put_contents( ROOT . '/tmp/logs/import_product_errors.log', date('Y-m-d H:i:s') . ' - ' . $errs . PHP_EOL, FILE_APPEND );
                }
            }
                file_put_contents( LOGS . 'last_products_update.txt', date('Y-m-d H:i:s'));
        }    
        die(json_encode(['result'=>true,'message'=>'Produkty importovány','clearCache'=>true]));
        
    }


    /**
     * editace
     */
    public function edit($id=null){
        $select_list = $this->getSelectList(false);
        $this->loadComponent('vI');
        //pr($select_list);die();
        
        // save data
        if (isset($this->request->data['saveData'])){
            $saveData = $this->request->data['saveData'];
            $saveData = $this->vI->convertTime($saveData);

            $this->checkCode();
            // pr($saveData);
            
            $save_entity = $this->Products->newEntity($saveData);
            //pr($save_entity); die();
            
            $this->vI->checkErrors($save_entity);

            if (!$resultDb = $this->Products->save($save_entity)){
                $results = [
                    'result'=>false,
                    'message'=>__('Chyba uložení')
                ];    
            } else {
                $connects = [];
                foreach($saveData['connects'] AS $c){
                    if ($c['checked'] == 1){
                        $c['product_id'] = $resultDb->id;
                        $c['product_group_id'] = $c['id'];
                        unset($c['id']);
                        $connects[] = $c;
                    }
                }

                $this->loadModel('ProductConnects');
                $this->ProductConnects->deleteAll(['product_id' => $resultDb->id]);
                $save_entity_connect = $this->ProductConnects->newEntities($connects);
                foreach($save_entity_connect AS $sc){
                    //pr($sc);
                    $res = $this->ProductConnects->save($sc);
                
                }
                $results = [
                    'result'=>true,
                    'message'=>__('Uloženo'),
                    'data'=>$resultDb
                ];  
            }
            
            

            $this->setJsonResponse($results);
        
        // load data
        } else {
            $defaultValues = [
                'nodelete'=>0,
            ];
            if ($id != null){
                // pokud je edit
                $mapper = function ($data, $key, $mapReduce) {
                    //$data->operating = (($data->operating == '')?0:1);
                    //$data->problem = (($data->problem == '')?0:1);
                    $connectList = [];
                    // pr($data->product_connects);die();
                    if (!empty($data->product_connects)){
                        foreach($data->product_connects AS $c){
                            $connectList[$c->product_group_id] = $c;
                        }
                        $data->product_connects = $connectList;
                    }

                    
                    $mapReduce->emit($data);  
                };
        
                if ($id != 'new'){
                    $conditions = ['id'=>$id];
                    $data = $this->Products->find()
                    ->contain([
                        'ProductConnects' => function($q){
                            return $q;
                            
                        },
                    ])
                    ->select()
                    ->where($conditions)
                    ->mapReduce($mapper)
                    ->first()
                ;
                // pokud je nova polozka
                } else {
                   
                    $data = $this->vI->emptyEntity('Products'); 
                    
                }
                $data = $this->vI->convertLoadData($data,$defaultValues);        
                $data->addons = json_decode($data->addons);
                $connects = [];
                //pr($data);die();
                // pr($this->groups_list_tree);die();
                //if (!empty($data->addons)){
                    foreach($this->groups_list_tree AS $id=>$gr){
                        if (isset($data->product_connects[$id])){
                            //pr($id);   
                        }
                        // $addons[] = ['id'=>$id,'name'=>$gr,'checked'=>(in_array($id,$data->addons)?true:false)];
                        $connects[] = [
                            'id'=>$id,
                            'name'=>$gr,
                            'checked'=>(isset($data->product_connects[$id])?1:0),
                            /*
                            'price'=>(isset($data->product_connects[$id])?$data->product_connects[$id]->price:''),
                            'price2'=>(isset($data->product_connects[$id])?$data->product_connects[$id]->price2:''),
                            'price3'=>(isset($data->product_connects[$id])?$data->product_connects[$id]->price3:''),
                            'price4'=>(isset($data->product_connects[$id])?$data->product_connects[$id]->price4:''),
                            */
                            'pref'=>(isset($data->product_connects[$id])?$data->product_connects[$id]->pref:''),
                        ];
                    }
                //}
                // pr($connects);die();
                unset($data->product_connects);
                $data->connects = $connects;
                $data->test = [
                    ['id'=>1,'name'=>'test1'],
                    ['id'=>2,'name'=>'test2'],

                ];
                //$data->addonsList = $addons;
                //pr($addons);die();
                // if (!empty($data->addons)){
                //     foreach($this->groups_list AS $k=>$name){
                        
                //         if (in_array($k,$data->addons)){
                //             $this->groups_list[$k] = true;
                //         } else {
                //             $this->groups_list[$k] = false;
                            
                //         }
                //     }
                // }
                // $data->selectAddon = $this->groups_list;
                //$data->groups_list = $this->groups_list;
                // pr($data);die();
                $validations = $this->vI->getValidations('Products');
            } else {

            }
            $results = [
                'result'=>true,
                'validations'=>(isset($validations)?$validations:null),
                'data'=>(isset($data)?$data:''),
                'select_list'=>$this->vI->SelectList($select_list),
            ];  
            //pr($results);die();  
            $this->setJsonResponse($results);
        }
        $this->clearCache('product_data');
    }


    public function saveAddon(){
        $conditions = [];
        $data = $this->Products->find()
            ->select()
            ->where($conditions)
            ->toArray();
            
        $this->loadModel('ProductConnAddons');    
        $list_load = $this->ProductConnAddons->find()
            ->select()
            ->where()
            ->toArray();
        $list = [];
        foreach($list_load AS $l ){
            $list[$l->product_id][] = $l->product_addon_id;
        }
        //pr($list);
        foreach($data AS $k=>$d){
            if (isset($list[$d->id])){
            $resultDb = $this->Products->updateAll(
                ['addons'=>json_encode($list[$d->id])], // fields
                ['id' => $d->id]
            );
            }
        }
        pr($data);
        die('a');
    }
}
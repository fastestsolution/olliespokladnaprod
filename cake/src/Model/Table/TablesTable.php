<?php
namespace App\Model\Table;
use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class TablesTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);
        //$this->belongsTo("Clients");
        
        //$this->hasMany("OrderItems");

        $this->addBehavior('Timestamp');
        $this->addBehavior('Trash');

    }

    public function beforeSave(Event $event)
    {
       
        return true;
    }

    

    public function tableList(){
     	
            $query = $this->find()
              //->contain(['ZakazkaConnects',])
              ->where([])
              ->select([
              ])
            ;
              
            $dataLoad =   $query->toArray();
            //pr($dataLoad);die();
            if ($dataLoad){
                $data_list = [];
                foreach($dataLoad AS $d){
                    $data_list[$d->id] = $d->name;
                }
                return $data_list;
            }  else {
                return false;
            }
        
    }

    public function tableListAll(){
     	
        $query = $this->find()
          //->contain(['ZakazkaConnects',])
          ->where([])
          ->select([
              'id',
              'name',
              'table_type_id',
              'group_id',
          ])
        ;
          
        $dataLoad =   $query->toArray();
        //pr($dataLoad);die();
        if ($dataLoad){
            $data_list = [];
            foreach($dataLoad AS $d){
                $data_list[$d->id] = $d;
            }
            return $data_list;
        }  else {
            return false;
        }
    
}
    
    public function loadTableGroups(){
        $mapperOrder = function ($d, $key, $mapReduce) {
            
            //$d->time = $d->created->format('H:i');
            //$d->created = $d->created->format('d.m.Y H:i:s');
            //pr($d->created);    
            
            $mapReduce->emit($d);
        };
    
        $conditions = [
        ];
        $data = $this->find()
        ->select([
            'id',
            'name',
            'date_open',
            'num',
            'group_id',
            'on_map',
        ])
        ->where($conditions)
        ->mapReduce($mapperOrder)
        ->order('Tables.num ASC')
        ->toArray();
        // pr($data);die();
        $tables = [];
            foreach($data AS $table){
                //$table
                $tables[$table->group_id][] = $table;
            }
        ksort($tables);
        
        
        
        //pr($tables);
		$data = [];
		foreach($tables AS $group_id=>$t){
			$data[] = [
				'group_id'=>$group_id,
				'tables'=>$t,
			];
        }
        
		//pr($data);
		//die();
        return $data;
    }


    public function validationDefault(Validator $validator){

        $validator
            //->requirePresence('name', true,   __("Jméno musí být vyplněno"))
            //->notEmpty('name',__('Jméno musí být vyplněno'))
            
            //->requirePresence('source_id', true,   __("Zdroj musí být vyplněn"))
            ->notEmpty('source_id',__('Zdroj musí být vyplněn'))
        ;


        /*$validator

          ->requirePresence('email', true,   __("Email musí být vyplněn"))

          ->notEmpty('email');*/

        return $validator;

    }

}
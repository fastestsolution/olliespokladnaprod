<?php
namespace App\Model\Table;
use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

class StockMirrorsTable extends Table
{
    public function initialize(array $config){
        parent::initialize($config);
        $this->addBehavior('Timestamp');
    }


    public function validationDefault(Validator $validator){

        $validator
            ->requirePresence('name', true,   __("Musíte zadat nazev zrcadla"))
            ->notEmpty('name',__("Musíte zadat nazev zrcadla"));

        return $validator;
    }
}
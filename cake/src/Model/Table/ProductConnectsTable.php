<?php
namespace App\Model\Table;
use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

class ProductConnectsTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->belongsTo("Products");
        
        $this->addBehavior('Timestamp');
        
    }

    public function beforeSave(Event $event)
    {
        //$event->data['entity']->name = $event->data['entity']->last_name.' '.$event->data['entity']->first_name; 
        //pr($event);die();
        
        return $event;
    }

    public function truncateTable(){
        $connection = ConnectionManager::get('default');
        $results = $connection->execute('TRUNCATE TABLE product_connects');
        //pr($results);
    }

    public function connectList(){
		$query = $this->find()
		  //->contain(['ZakazkaConnects',])
		  ->select([
			'product_id',
			'product_group_id',
          ])
        
        ;
		  
		$data_load =   $query->toArray();
        //pr($data_load);die();
        $data_list = [];
        foreach($data_load AS $d){
            $data_list[$d->product_group_id][] = $d->product_id;
        }
        //pr($data_list);die();
        return $data_list;  
	}

   

    public function validationDefault(Validator $validator){

        $validator
            //->requirePresence('name', true,   __("Musíte zadat jméno"))
            //->notEmpty('name',__("Musíte zadat jméno"))
            
        ;

        /*$validator

          ->requirePresence('email', true,   __("Email musí být vyplněn"))

          ->notEmpty('email');*/

        return $validator;

    }

}
<?php
namespace App\Model\Table;
use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class SettingsTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);
        
        $this->addBehavior('Timestamp');
        $this->addBehavior('Trash');

    }

    public function beforeSave(Event $event)
    {
        //$event->data['entity']->name = $event->data['entity']->last_name.' '.$event->data['entity']->first_name; 
        //pr($event);die();
        
        return $event;
    }

    public function operationsList($array=false){
		
		$query = $this->find()
		  ->where(['id'=>1])
		  ->select([
			'operations',
          ])
          ->cache(function ($query) {
			return 'product_group_data-provozList' . md5(serialize($query->clause('where')));
            })
        ;
		$data =   $query->first();
        //pr($data);
        $data_list = json_decode($data->operations);
        if ($array){
            $data_list = json_decode($data->operations,true);
        } else {
            $data_list = json_decode($data->operations);
            
        }  
        return $data_list;  
	}	
  
    public function getSetting(){
		
		$query = $this->find()
		  //->contain(['ZakazkaConnects',])
		  ->where(['id'=>1])
		  ->select([
			'id',
			'name',
			'data',
			'logo',
			'printer',
          ])
          ->cache(function ($query) {
			return 'setting_data-load' . md5(serialize($query->clause('where')));
            })
        ;
		  
		$data =   $query->first();
        if (!$data){
            die(json_encode(['result'=>false,'message'=>'Není vytvořeno nastavení']));
        }
        $data->data = json_decode($data->data);
        return $data;  
	}	
    

    public function validationDefault(Validator $validator){

        $validator
            //->requirePresence('name', true,   __("Musíte zadat jméno"))
            //->notEmpty('name',__("Musíte zadat jméno"))
            
        ;

        /*$validator

          ->requirePresence('email', true,   __("Email musí být vyplněn"))

          ->notEmpty('email');*/

        return $validator;

    }

}
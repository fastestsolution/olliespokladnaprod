<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\ORM\Entity;
use Cake\Event\Event;


require_once( ROOT . '/vendor/filipsedivy/php-eet/src/Utils/UUID.php');
require_once( ROOT . '/vendor/filipsedivy/php-eet/src/Utils/Format.php');
require_once( ROOT . '/vendor/filipsedivy/php-eet/src/Receipt.php');
require_once( ROOT . '/vendor/filipsedivy/php-eet/src/Dispatcher.php');
require_once( ROOT . '/vendor/filipsedivy/php-eet/src/Exceptions/CertificateException.php');
require_once( ROOT . '/vendor/filipsedivy/php-eet/src/Exceptions/ServerException.php');
require_once( ROOT . '/vendor/filipsedivy/php-eet/src/Exceptions/ClientException.php');
require_once( ROOT . '/vendor/filipsedivy/php-eet/src/Certificate.php');
require_once( ROOT . '/vendor/filipsedivy/php-eet/src/SoapClient.php');
require_once( ROOT . '/vendor/robrichards/xmlseclibs/src/XMLSecurityKey.php');
require_once( ROOT . '/vendor/robrichards/xmlseclibs/src/XMLSecurityDSig.php');
require_once( ROOT . '/vendor/robrichards/wse-php/src/WSSESoap.php');

use FilipSedivy\EET\Exceptions\CertificateException;
use FilipSedivy\EET\Certificate;
use FilipSedivy\EET\SoapClient;
use FilipSedivy\EET\Dispatcher;
use FilipSedivy\EET\Receipt; 
use FilipSedivy\EET\Utils\UUID;
use FilipSedivy\EET\Utils\Format;

class EetQueuesTable extends Table
{
	private $testing = false; //Ostry vs Testovaci provoz
	public $error = null;
	private $settings = [
		'pobocka_id' => null,
		'id_provoz' => null,
		'id_pokl' => null,
		'dic' => null, //'CZ28596030'
		'dic_poverujici' => null,
		'certificate' => null,
		'password' => null
	];
	private $taxes = [
		1 => 0.1736,	//21%
		2 => 0.1304,	//15%
		3 => 0.0909		//10%
	];
	private $cert_path = 'uploaded/eet_certificates/';

	public function initialize(array $config)
	{
		$this->hasOne('Orders');
	}

	public function afterSave(Event $event, Entity $entity)
    {
		//Pri ulozeni noveho zaznamu do fronty EET se automaticky posle pozadavek na EET server
		if($entity->isNew()){
			$entity = $this->sendToEetServer($entity);
		}
        return $event;
	}

	/**
	 * Pokud pobocka nema nastaven vlastni certifikat a jsme prepnuti do ostreho provozu, pak funkce vrati false, jinak vraci true a zaroven nahrava nastaveni potrebne pro EET
	 */
	private function loadSettings(){
		
		$SettingTable = TableRegistry::get("Settings");
		$systemSetting = $SettingTable->getSetting();

		$BranchesTable = TableRegistry::get("Branches");
		$branch = $BranchesTable->find()->where(["Branches.id" => $systemSetting->data->system_id])->first();

		if($branch){
			//pr(var_dump(isset($branch->eet_active) && (bool) $branch->eet_active == true)); die();
			if(isset($branch->eet_active) && (bool) $branch->eet_active == true){
				if(isset($branch->eet_branch_id)){
					$branch_id = $branch->eet_branch_id;
				}else{
					$branch_id = 11; //Ollies Praha - balbin
				}
				$cert_name = 'test_cacert.p12';
				if(!isset($branch->eet_certificate) || !$branch->eet_certificate){
					$this->settings = [
						'pobocka_id' => $branch_id,
						'id_provoz' => $branch_id,
						'id_pokl' => $branch_id,
						'dic' => 'CZ28596030', 
						'dic_poverujici' => 'CZ28596030',
						'certificate' => $this->cert_path . $cert_name,	//Testovaci certifikat
						'password' => 'eet'	//Testovaci certifikat
					]; 
				}else{
					$cert_name = (!empty($branch->eet_certificate) ? $branch->eet_certificate : null);
					$this->settings = [
						'pobocka_id' => $branch_id,
						'id_provoz' => $branch_id,
						'id_pokl' => $branch_id,
						'dic' => $branch->eet_dic, 
						'dic_poverujici' => $branch->eet_dic_pover,
						'certificate' => ($cert_name ? $this->cert_path . $cert_name : ''),	
						'password' => (!empty($branch->eet_cert_pass) ? $branch->eet_cert_pass : '')	
					];
				}
				if( $cert_name == 'test_cacert.p12'){
					$this->testing = true;
				}
		   	}else{
				return false;
			}
		}else{
			return false;
		}
		return true;
	}

	public function sendToEetServer($entity){
		$this->error = null;
		
		if(!$this->loadSettings()){
			return false;
		}

		$eetObj = $this->prepare($entity->order_id, $entity->storno);

		if($eetObj !== false && ($result = $this->send($eetObj)) !== false){
			$entity->fik = $result['fik'];
			$entity->bkp = $result['bkp'];
			$entity->uid = $result['uid'];
			$entity->sended = $result['date_send'];

			@file_put_contents( ROOT . '/tmp/logs/eet.log', date('Y-m-d H:i:s') . ' - ' . $entity->order_id . ' '. json_encode($result) . PHP_EOL, FILE_APPEND );

			try{
				$this->updateAll([
					'fik' => $result['fik'],
					'bkp' => $result['bkp'],
					'uid' => $result['uid'],
					'price' => $eetObj->celk_trzba,
					'sended' => $result['date_send']
				],[
					'id'  => $entity->id
				]);
			}catch(\PDOException $e){
				@file_put_contents( ROOT . '/tmp/logs/eet.log', date('Y-m-d H:i:s') . ' - ' . $entity->order_id . ' '  . $e->getMessage() . PHP_EOL, FILE_APPEND );
			}
		}
		return $entity;
	}

	private function prepare($orderId, $storno = false){

		if($orderId <= 0){
			$this->error = 'EET prazdne ID objednavky';
			return false;
		}

		$order = $this->getData($orderId);
		$calcPrices = $this->calculate($order);

		$date_send = Time::now();
		
		if($storno == true) {
			$calcPrices->priceAll = (float) -1 * $calcPrices->priceAll;
		}
		
		$uuid = UUID::v4();

		$r = new Receipt;
			$r->uuid_zpravy = $uuid;
			$r->id_provoz = $this->settings['id_provoz'];		//$setting["id_pokl"];//$setting["id_provoz"]; $_SESSION['Auth']['User']['pokl_number'];
			$r->id_pokl = $this->settings['id_pokl'];			//$setting["id_pokl"];//$setting["id_pokl"]; $_SESSION['Auth']['User']['pokl_number'];
			$r->dic_popl = $this->settings['dic'];				//$setting["dic_popl"];
			$r->dic_poverujiciho = $this->settings['dic_poverujici'];
			$r->pobocka_id = $this->settings['pobocka_id'];		//$setting["id_pokl"];//$_SESSION['Auth']['User']['pokl_number'];
			$r->porad_cis = $orderId; 							//možná nastavit UNIQUE ID
			$r->dat_trzby = new \DateTime();
			$r->celk_trzba = $calcPrices->priceAll;
			$r->storno = $storno;

			// DPH 21%
			$r->dan1 = $calcPrices->priceVat1;
			$r->zakl_dan1 = $calcPrices->taxVat1;
			// DPH 15%
			$r->dan2 =  $calcPrices->priceVat2;
			$r->zakl_dan2 = $calcPrices->taxVat2;
			//DPH 10%
			$r->dan3 = $calcPrices->priceVat3;
			$r->zakl_dan3 = $calcPrices->taxVat3;
		
			/*@file_put_contents( ROOT . '/tmp/logs/eet.log', date('Y-m-d H:i:s') . ' - ' . $entity->order_id . ' '. json_encode($r) . PHP_EOL, FILE_APPEND );
			die('STOP');*/
		return $r;
	}

	private function send($receipt){
		if(!$this->settings['certificate'] || trim($this->settings['certificate']) == ''){
			$this->error = 'Soubor EET certifikatu neni nastaven';
			return false;
		} 
		if(!$this->settings['password'] || trim($this->settings['password']) == ''){
			$this->error = 'Heslo k EET certifikatu neni nastaveno';
			return false;
		} 
		try{
			$certificate = new Certificate($this->settings['certificate'] , $this->settings['password']);
		}catch(\FilipSedivy\EET\Exceptions\CertificateException $e){
			$this->error = 'Soubor EET certifikatu nenalezen nebo nema spravne heslo ('.$this->settings['certificate'].') ' . $e->getMessage() ;
			//die('ERR '.$e->getMessage()); 
			return false;
		}
		try {
		$dispatcher = new Dispatcher($certificate);
			if($this->testing){  
				$dispatcher->setPlaygroundService();
			}else{
				$dispatcher->setProductionService();
			}

			$dispatcher->send($receipt);
		} catch (\FilipSedivy\EET\Exceptions\ClientException $e) {
			@file_put_contents( ROOT . '/tmp/logs/eet.log', date('Y-m-d H:i:s') . ' - ' . $receipt->porad_cis . ' ' . $e->getMessage() . PHP_EOL, FILE_APPEND );
			return false;
		}

		$result = array(
			"fik" => $dispatcher->getFik(),
			"bkp" => $dispatcher->getBkp(),
			"uid" => $receipt->uuid_zpravy,
			"date_send" => $receipt->dat_trzby,
			"storno" => $receipt->storno,
			"price" => $receipt->celk_trzba
		);
				
		return $result;
	}

	private function calculate($order = null){
		$res = new \stdClass();
		$res->priceAll = 0;
		$res->priceVat1 = 0;
		$res->taxVat1 = 0;
		$res->priceVat2 = 0;
		$res->taxVat2 = 0;
		$res->priceVat3 = 0;
		$res->taxVat3 = 0;
		if(isset($order)){
			if(isset($order["order_items"])){
				foreach($order["order_items"] as $item){
					$itemPrice = (float) ( $item["count"] * $item["price"]);
					$res->priceAll += $itemPrice;
					if($item['tax_id'] == 2){ //21
						$vatPrice = $itemPrice * $this->taxes[1];
						$res->taxVat1 += $vatPrice;
						$res->priceVat1 += ( $itemPrice - $vatPrice );
					}else if($item['tax_id'] == 1){ //15
						$vatPrice = $itemPrice * $this->taxes[2];
						$res->taxVat2 += $vatPrice;
						$res->priceVat2 += ( $itemPrice - $vatPrice );
					}else if($item['tax_id'] == 4){ //10
						$vatPrice = $itemPrice * $this->taxes[3];
						$res->taxVat3 += $vatPrice;
						$res->priceVat3 += ( $itemPrice - $vatPrice );
					}
				}
			}
		}
		return $res;
	}

	private function getData($orderId){

		$Orders = TableRegistry::get("Orders");

		$data = $Orders->find()
			->where(["Orders.id" => $orderId])
			->contain(["OrderItems", "OrderItems.Products", "OrderItems.Users"])
			->hydrate(false)
			->first();

		return $data;
	}



}




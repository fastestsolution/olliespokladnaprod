<?php
namespace App\Model\Table;
use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class OldDeliverysTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->table('fastest__rozvozces');
        //$this->addBehavior('Timestamp');
        //$this->addBehavior('Trash');

    }
    public static function defaultConnectionName() {
        
        $conn = 'default2';
        return $conn;
    }

    public function beforeSave(Event $event)
    {
       
        return true;
    }


}
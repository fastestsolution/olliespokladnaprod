<?php
namespace App\Model\Table;
use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

class BranchesTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->hasMany("ProductConnects");
        $this->hasMany("ProductConnAddons");
        
        $this->addBehavior('Timestamp');
        $this->addBehavior('Trash');

    }

    public function beforeSave(Event $event)
    {
        //$event->data['entity']->name = $event->data['entity']->last_name.' '.$event->data['entity']->first_name; 
        //pr($event);die();
        
        return $event;
    }

    public function branchesList(){
		return $this->find('list', ['keyField' => 'id','valueField' => 'name'])->cache(function ($query) {
                                                                                    return 'branchesList';
                                                                                })->order('name ASC')->toArray();
    }
    
    public function getProduct($code){
		
		$query = $this->find()
		  //->contain(['ZakazkaConnects',])
		  ->where(['code'=>$code])
		  ->select([
			'id',
          ])
        ;
		  
		$data =   $query->first();
        //pr($data);die();
        if ($data){
            return $data->id;
        }  else {
            return false;
        }
    }	
    

    public function truncateTable(){
        $connection = ConnectionManager::get('default');
        $results = $connection->execute('TRUNCATE TABLE branches');
        //pr($results);
    }


    public function groupTrzbaList(){
		
		$query = $this->find('list',['keyField' => 'id','valueField' => 'group_trzba_id'])
		  //->contain(['ZakazkaConnects',])
		  ->where([
              //'status'=>1
          ])
		  ->select([
			'id',
			'group_trzba_id',
          ])
        //   ->cache(function ($query) {
		// 	return 'group_trzba_id-list' . md5(serialize($query->clause('where')));
        //     })
        ;
		  
		$data_list =   $query->toArray();
        
        return $data_list;  
    }	

    public function productList(){
        $this->tax_list = [
            1=>0.1304, // 15
            2=>0.1736, // 21
            3=>0, // 0
            4=>0.0909, // 10
    

        ];

        $mapper = function ($data, $key, $mapReduce) {
            if (isset($data->product_connects) && !empty($data->product_connects)){
                $data->price_list = [];
                if (!isset($data->tax_id)){
                    $data->tax_id = 1;
                }
                $data->price_without_tax = $data->price - $data->price * $this->tax_list[$data->tax_id];
              
                foreach($data->product_connects AS $pc){
                    $data->price_list[$pc->product_group_id] = [
                        'price'=>($pc->price>0)?$pc->price:$data->price,
                        'price_without_tax'=>($pc->price>0)?$pc->price - $pc->price * $this->tax_list[$data->tax_id]:$data->price_without_tax,
                        'price2'=>($pc->price2>0)?$pc->price2:$data->price,
                        'price2_without_tax'=>($pc->price2>0)?$pc->price2 - $pc->price2 * $this->tax_list[$data->tax_id]:$data->price_without_tax,
                        'price3'=>($pc->price3>0)?$pc->price3:$data->price,
                        'price3_without_tax'=>($pc->price3>0)?$pc->price3 - $pc->price3 * $this->tax_list[$data->tax_id]:$data->price_without_tax,
                        'price4'=>($pc->price4>0)?$pc->price4:$data->price,
                        'price4_without_tax'=>($pc->price4>0)?$pc->price4 - $pc->price4 * $this->tax_list[$data->tax_id]:$data->price_without_tax,
                        
                    ];
                }
                unset($data->product_connects);
                if (isset($data->product_conn_addons) && !empty($data->product_conn_addons)){
                    $addons = [];
                    foreach($data->product_conn_addons AS $cd){
                        $addons[] = $cd->product_addon_id;
                    }
                    $data->addons = json_encode($addons);
                    unset($data->product_conn_addons);
                }
            }
            //$data->close_order = (($data->close_order)?1:0);
            $mapReduce->emit($data);
        };

        $query = $this->find()
		  ->contain(['ProductConnects','ProductConnAddons'])
		  ->where([
            //   'id'=>419
          ])
		  ->select([
          ])
        
          ->mapReduce($mapper)
        ;
		  
        $dataLoad =   $query->toArray();
        // pr($dataLoad);die();
        
        
        $this->ProductAddons = TableRegistry::get('ProductAddons');
        $queryAddon = $this->ProductAddons->find()
		//   ->contain(['ProductConnAddons',])
		  ->where([
            //   'id'=>419
          ])
		  ->select([
			'id',
			'name',
			'price',
			'group_id',
          ])
        //   ->cache(function ($query) {
		// 	return 'product_group_addon_data-list' . md5(serialize($query->clause('where')));
        //     })
        ;
		  
        $data_list_addon =   $queryAddon->toArray();
        $this->addon_list = [];
        foreach($data_list_addon AS $d){
            $this->addon_list[$d->id] = $d;
        }
        // pr($this->addon_list);die();
        if ($dataLoad){
            $data_list = [];
            foreach($dataLoad AS $d){
                if (!isset($d->tax_id) || empty($d->tax_id)){
                    $d->tax_id = 1;
                }
                $d->price_without_tax = $d->price - $d->price * $this->tax_list[$d->tax_id];
                //pr($d->price);
                //pr($d->price_without_tax);
                //die();
                if ($d->id == 75){
                    //pr($d);die();
                }
                $d->addons = json_decode($d->addons);
                if (!isset($d->addonList)){
                    $d->addonList = [];
                }
                if (empty($d->addons)){
                    //die('a');
                    $d->addons = [];
                } else {
                    // pr($this->addon_list);die();
                    // pr($d->addons);die();
                    foreach($d->addons AS $ad){
                        if (isset($this->addon_list[$ad])){
                            $d->addonList[] = $this->addon_list[$ad];
                            // die('a');
                        }
                    }
                    
                
                }
                $data_list[$d->id] = $d;
                
            }
            // pr($data_list);die();
            return $data_list;
        }  else {
            return false;
        }
	}	
    
    public function findProduct($code){
		
		$query = $this->find()
		  //->contain(['ZakazkaConnects',])
		  ->where(['code'=>$code])
		  ->select([
          ])
        ;
		  
		$data =   $query->first();
        //pr($data);die();
        if ($data){
            return $data;
        }  else {
            return false;
        }
	}	
  

  

    

    public function validationDefault(Validator $validator){

        $validator
            ->requirePresence('name', true,   __("Musíte zadat jméno"))
            ->notEmpty('name',__("Musíte zadat jméno"))
            
            ->requirePresence('code', true,   __("Musíte zadat kód"))
            ->notEmpty('code',__("Musíte zadat kód"))
            
            //->requirePresence('product_group_id', true,   __("Musíte zadat skupinu produktu"))
            //->notEmpty('product_group_id',__("Musíte zadat skupinu produktu"))
            
            // ->requirePresence('price', true,   __("Musíte zadat cenu"))
            // ->notEmpty('price',__("Musíte zadat cenu"))
            
            
        ;

        /*$validator

          ->requirePresence('email', true,   __("Email musí být vyplněn"))

          ->notEmpty('email');*/

        return $validator;

    }

}
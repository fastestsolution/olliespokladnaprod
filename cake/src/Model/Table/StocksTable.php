<?php
namespace App\Model\Table;
use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

class StocksTable extends Table
{
    public $plus_types = [1,5,7,8,9,11,12];     //Skladove polozky (typ) ktere jsou plusove
    public $minus_types = [2,4,6,10,3,10];      //Skladove polozky (typ) ktere se musi odecitat
    

    public function initialize(array $config)
    {
        parent::initialize($config);
        
        $this->belongsTo('Products', ['foreignKey'=>'stock_item_product_id']);
        $this->belongsTo('StockGlobalItems', ['foreignKey'=>'stock_item_id']);
        $this->belongsTo('ProductGroups', ['foreignKey'=>'system_id']);
        $this->belongsTo('Branches', ['foreignKey'=>'system_id']);
        $this->belongsTo('FromBranches', ['foreignKey'=>'system_id_to', 'className'=>'Branches']);

        $this->addBehavior('AutoRemote', [
            'url' => 'api/storeLocalStocks/',
            'debugFile' => 'stock_cloud.log'
        ]);
        
        $this->addBehavior('Timestamp');

    }

    /**
	 * seznam pro zrcadlo
	 */
	public function stockMirrorList($system_id=null){ 
		

        $Products = TableRegistry::get('Products');
        $productsIds = $this->Products->find()
                            ->contain(['ProductRecipes'])
                            ->select(['Products.id'])
                            ->where(['Products.is_mirror'=>1])
                            ->toArray();

        $listProductsGlobal = [];
        if ($productsIds){
            foreach($productsIds AS $pid){
                if ($pid->product_recipes){
                    foreach($pid->product_recipes AS $pr){
                        $listProductsGlobal[] = $pr->stock_item_global_id;
                    }
                }
            }
        }

        $ids = [];
        //pr($listProductsGlobal);
        if(!empty($listProductsGlobal )){
            $StockGlobalItems = TableRegistry::get('StockGlobalItems');
            $stockItemsList = $StockGlobalItems->skladItems($listProductsGlobal);
            //pr($stockItemsList);
            if($stockItemsList){
                foreach($stockItemsList AS $k=>$sl){
                    $ids[] = $k;
                }
            }
        }

        $items_list = [];
        if(!empty($ids)){
            $conditions = [
                'stock_item_id IN' =>$ids,
            ];
            if ($system_id){
                $conditions['system_id'] = $system_id;
            }

            $items_list_load = $this->find()
                ->where($conditions)
                ->contain('StockGlobalItems')
                ->select([
                    'id',
                    'stock_item_id',
                    'stock_type_id',
                    'value',
                    'StockGlobalItems.id',
                    'StockGlobalItems.name',
                    'StockGlobalItems.jednotka_id',
                ])
                ->order('StockGlobalItems.name ASC')
                ->toArray();
			
            foreach($items_list_load AS $l){
                if (!isset($items_list[$l->stock_item_id])){
                    $items_list[$l->stock_item_id] = $l;
                    $items_list[$l->stock_item_id]['sum'] = 0;
                }
                // 1=>'Příjem',
                // 2=>'Převodka',
                // 3=>'Odpis',
                // 4=>'Prodej',
                // 5=>'Zrcadlo plus',
                // 6=>'Zrcadlo minus',
                // 7=>'Příjem Makro',
                // 8=>'Příjem BidFood',
                
                // 1=>'Příjem',
                // 2=>'Převodka minus',
                // 11=>'Převodka plus',
                // 12=>'Storno objednavky',
                // 3=>'Odpis',
                // 4=>'Prodej',
                // 5=>'Zrcadlo plus',
                // 6=>'Zrcadlo minus',
                // 9=>'Zrcadlo plus - oprava',
                // 10=>'Zrcadlo minus - oprava',
                // 7=>'Příjem Makro',
                // 8=>'Příjem BidFood',
               
                $l->value = trim($l->value);
                $items_list[$l->stock_item_id]['sum'] = trim($items_list[$l->stock_item_id]['sum']);
                    
                if (in_array($l->stock_type_id,$this->plus_types)){
                    $items_list[$l->stock_item_id]['sum'] += $l->value;
                }

                if (in_array($l->stock_type_id,$this->minus_types)){
                        $items_list[$l->stock_item_id]['sum'] -= $l->value;
                }
            }
        }

        $result = [];
        if(!empty($items_list)){
            foreach($items_list AS $item){
                $result[] = $item;
            }
        }
		return($result);	
	}
}
<?php
namespace App\Model\Table;

use Cake\Auth\DefaultPasswordHasher;
use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class UsersTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('Timestamp');
        $this->addBehavior('Trash');

    }

    public function beforeSave(Event $event)
    {
       
        return true;
    }

    public function userList(){
     	
        $query = $this->find()
          //->contain(['ZakazkaConnects',])
          ->where([])
          ->select([
          ])
        ;
          
        $dataLoad =   $query->toArray();
        //pr($dataLoad);die();
        if ($dataLoad){
            $data_list = [];
            foreach($dataLoad AS $d){
                $data_list[$d->id] = $d->name;
            }
            return $data_list;
        }  else {
            return false;
        }
    
    }

    

    public function validationDefault(Validator $validator){

        $validator
            ->requirePresence('username', true,   __("Uživatelské jméno musí být vyplněno"))
            ->notEmpty('username');

        $validator
            ->requirePresence('name', true,   __("Jméno musí být vyplněno"))
            ->notEmpty('name');



        /*$validator

          ->requirePresence('email', true,   __("Email musí být vyplněn"))

          ->notEmpty('email');*/

        return $validator;

    }

}
<?php
namespace App\Model\Table;
use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Validation\Validator;

use Cake\Datasource\ConnectionManager;

class ProductAddonsTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);
        
        $this->addBehavior('Timestamp');
        //$this->addBehavior('Trash');
        

    }

    public function beforeSave(Event $event)
    {
        //$event->data['entity']->name = $event->data['entity']->last_name.' '.$event->data['entity']->first_name; 
        //pr($event);die();
        
        return $event;
    }

    public function truncateTable(){
        $connection = ConnectionManager::get('default');
        $results = $connection->execute('TRUNCATE TABLE product_addons');
        //pr($results);
    }


  
    public function addonList(){
		
		$query = $this->find()
		  //->contain(['ZakazkaConnects',])
		  ->where([
              //'status'=>1
          ])
		  ->select([
			'id',
			'name',
			'price',
			'group_id',
          ])
        //   ->cache(function ($query) {
		// 	return 'product_group_addon_data-list' . md5(serialize($query->clause('where')));
        //     })
        ;
		  
		$data_list_load =   $query->toArray();
        $data_list = [];
        foreach($data_list_load AS $d){
            $data_list[$d->id] = $d;
        }
        return $data_list;  
    }	
    
    public function validationDefault(Validator $validator){

        $validator
            ->requirePresence('name', true,   __("Musíte zadat jméno"))
            ->notEmpty('name',__("Musíte zadat jméno"))
            
        ;

        /*$validator

          ->requirePresence('email', true,   __("Email musí být vyplněn"))

          ->notEmpty('email');*/

        return $validator;

    }

}
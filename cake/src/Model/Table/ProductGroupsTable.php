<?php
namespace App\Model\Table;
use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Validation\Validator;

use Cake\Datasource\ConnectionManager;

class ProductGroupsTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->hasMany("ProductConnects");
        
        $this->addBehavior('Timestamp');
        $this->addBehavior('Tree');
        //$this->addBehavior('Trash');

    }

    public function beforeSave(Event $event)
    {
        //$event->data['entity']->name = $event->data['entity']->last_name.' '.$event->data['entity']->first_name; 
        //pr($event);die();
        
        return $event;
    }

    public function truncateTable(){
        $connection = ConnectionManager::get('default');
        $results = $connection->execute('TRUNCATE TABLE product_groups');
        //pr($results);
    }


    public function getGroup($web_id){
		
		$query = $this->find()
		  //->contain(['ZakazkaConnects',])
		  ->where(['web_id'=>$web_id])
		  ->select([
			'id',
          ])
        ;
		  
		$data =   $query->first();
        //pr($data);die();
        if ($data){
            return $data->id;
        }  else {
            return false;
        }
	}	
  
    public function groupList(){
		
		$query = $this->find(/*'list',['keyField' => 'id','valueField' => 'name']*/)
            ->select([
                'id',
                'value'=>'name',
            ])
            ->where(['level'=>2])
           /* ->cache(function ($query) {
                return 'product_group-list';
            })*/;
		  //debug($query );
          return $query->toArray();
    }	
    
    public function groupListTree(){
		
		$query = $this->find('threaded')
		  //->contain(['ZakazkaConnects',])
		  ->where([

              //'status'=>1
          ])
		  ->select([
              'id',
              'name',
              'level',
              'parent_id',
          ])
          ->cache(function ($query) {
			return 'product_group_data-treeList' . md5(serialize($query->clause('where')));
            })
        ;
		  
		$data_list =   $query->toArray();
        
        return $data_list;  
    }	
    
    public function groupListTreeEdit(){
		
		$query = $this->find('threaded')
		  //->contain(['ZakazkaConnects',])
		  ->where([

              //'status'=>1
          ])
		  ->select([
              'id',
              'name',
              'level',
              'parent_id',
          ])
        //   ->cache(function ($query) {
		// 	return 'product_group_data-treeList' . md5(serialize($query->clause('where')));
        //     })
        ;
		  
		$data_list =   $query->toArray();
        //pr($data_list);
        return $data_list;  
    }	
    
    public function fullpathList(){
		$list = [];
		$query = $this->find()
		  ->where([
          ])
		  ->select([
              'id',
              'name',
              'parent_id',
          ])
          ->order('lft ASC');
		  
        $data_list = $query->toArray();
        foreach($data_list as $item){
            $list[$item->id] = (isset($item->parent_id) && $item->parent_id && isset($list[$item->parent_id]) ? $list[$item->parent_id] . ' -> ' : '')  . $item->name;
        }
        return $list;  
    }	

    public function groupTreeList(){
		
		$query = $this->find('treeList',['spacer'=>'- '])
		  //->contain(['ZakazkaConnects',])
		  ->where([

              //'status'=>1
          ])
		  ->select([
              'id',
              'name',
              'level',
              'parent_id',
          ])
          ->order('lft ASC')
         // ->cache(function ($query) {
			//return 'product_group_data-treeListSelectList' . md5(serialize($query->clause('where')));
            //})
        ;
		  
        $data_list =   $query->toArray();
        //pr($data_list);die();
        
        return $data_list;  
    }	
    
    private function getPath($id){
        $path = $this->find('path',['for' => $id])->toArray();
        $name = '';
        foreach($path AS $p){
            $name .= $p->name.' ';
        }
        return $name;
        //pr($name);
    }
    
    public function groupListDeadline(){
		
		$query = $this->find('treeList',['spacer' => ' - '])
		// $query = $this->find('path',['for' => 2])
		  //->contain(['ZakazkaConnects',])
		  ->where([
              'level'=>3
              //'status'=>1
          ])
		  ->select([
			// 'id',
			// 'name',
          ])
        ;
		  
		$data_list_load =   $query->toArray();
        $data_list = [];
        foreach($data_list_load AS $k=>$d){
            $data_list[$k] = $this->getPath($k);
        }
        //pr($data_list);die();
        return $data_list;  
	}	
    
    public function loadGroupProducts(){
		
		$query = $this->find()
            /*
            ->contain(['Products'=>['fields'=>[
              'id',
              'product_group_id',
              'web_group_id',
              'name',
              'code',
              'price',
              'amount',
              'num',
            ],'sort'=>['num'=>'ASC']]
            ])
            
          ->contain(['ProductConnects',
          'ProductConnects.Products'
          ])
          */  
          ->order('lft ASC')
		  ->where([])
		  ->select([
          ])
          //->order('ProductGroups.ordeidr_num ASC')
          //->cache(function ($query) {
			//return 'product_data-order' . md5(serialize($query->clause('where')));
            //})
        ;
		  
		$data_load =   $query->toArray();
        $data_list = [];
        foreach($data_load AS $dat){
            if (empty($dat->parent_id)) $dat->parent_id = 0;    
            
            $data_list[$dat->level][$dat->parent_id]['count'] = 0;
            $data_list[$dat->level][$dat->parent_id]['data'][] = $dat;
        }
        
        foreach($data_list AS $level=>$dd){
            foreach($dd AS $k=>$d){
                $data_list[$level][$k]['count'] = count($d['data']);
       
            }
        
        }
        /*
        foreach($data_list AS $level=>$dd){
            foreach($dd AS $k=>$d){
                //pr(count($dd);die();
                if (empty($d->parent_id)) $d->parent_id = 0;    
                $data_list[$level][$d->parent_id] = [];
                $data_list[$level][$d->parent_id]['count'] = count($dd); 
                $data_list[$level][$d->parent_id]['data'][] = $d;
                //unset($dd); 
            }
        }
        */
        // pr($data_list);die();
        return $data_list;  
	}	

    

    public function validationDefault(Validator $validator){

        $validator
            ->requirePresence('name', true,   __("Musíte zadat jméno"))
            ->notEmpty('name',__("Musíte zadat jméno"))
            
        ;

        /*$validator

          ->requirePresence('email', true,   __("Email musí být vyplněn"))

          ->notEmpty('email');*/

        return $validator;

    }

}
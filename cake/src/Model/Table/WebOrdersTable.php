<?php
namespace App\Model\Table;
use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class WebOrdersTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->hasMany("WebOrderItems");

        $this->addBehavior('Timestamp');
        $this->addBehavior('Trash');
    }

    public function validationDefault(Validator $validator){
        return $validator;
    }

}
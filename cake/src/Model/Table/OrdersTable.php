<?php
namespace App\Model\Table;
use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class OrdersTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->belongsTo("Clients");
        
        $this->hasOne("EetQueues", ['conditions'=>['EetQueues.storno'=>0]]); 
        $this->hasOne("EetStorno", ['className' => 'App\Model\Table\EetQueuesTable','conditions'=>['EetStorno.storno'=>1]]);
        $this->hasMany("OrderItems");

        $this->addBehavior('Timestamp');
        $this->addBehavior('Trash');

        $this->addBehavior('AutoRemote', [
            'url' => 'api/storeLocalOrders/',
            'debugFile' => 'orders_cloud.log'
        ]);

    }


    public function getOrdersListSum($from,$to){
        
        $conditions = [
            'id >'=>$from,
            'id <='=>$to,
        ];
        $data_load = $this->find()
        //->contain('OrderItems')
        ->select([
            'storno',
            'total_price',
            'payment_id',
        ])
        ->where($conditions)
        //->mapReduce($mapperOrder)
        ->order('Orders.id DESC')
        ->toArray();

        $this->sum_list = [
            'total_price'=>0,
            'pay_cash'=>0,
            'pay_card'=>0,
            'pay_voucher'=>0,
            'pay_discount'=>0,
            'pay_employee'=>0,
            'pay_company'=>0,
            'sum_storno'=>0,

        ];
        foreach($data_load AS $l){
            if($l->storno == 0){
                $this->sum_list['total_price'] += $l->total_price;
               
                if ($l->payment_id == 1){
                    $this->sum_list['pay_cash'] += $l->total_price;
                }
                if ($l->payment_id == 2){
                    $this->sum_list['pay_card'] += $l->total_price;
                }
            }else{
                $this->sum_list['sum_storno'] += $l->total_price;
            }
        }
        // pr($this->sum_list);die();
        return $this->sum_list;
    }
    public function getOrdersList(){
        $mapperOrder = function ($d, $key, $mapReduce) {
            //$d->time = $d->created->format('H:i');
            //$d->created = $d->created->format('d.m.Y H:i:s');
            //pr($d->created);    
            
            $mapReduce->emit($d);
        };
    
        $conditions = [
            'distributor_id IS'=>null,
            'storno'=>0,
        ];
        $data = $this->find()
        ->contain('OrderItems')
        ->select()
        ->where($conditions)
        ->mapReduce($mapperOrder)
        ->order('Orders.id DESC')
        ->toArray();
        return $data;
    }

    public function checkOpenOrder(){
        
        $conditions = [
            'distributor_id IS'=>null,
            'storno'=>0,
        ];
        $data = $this->find()
        ->select(['id'])
        ->where($conditions)
        ->order('Orders.id DESC')
        ->toArray();
        
        return count($data);
    }
  
    public function getOrderId($web_id){
        
        $conditions = [
            'web_id'=>$web_id,
        ];
        $data = $this->find()
        ->select(['id'])
        ->where($conditions)
        ->first();
        if ($data){
            return $data;
        } else {
            return false;
        }
    }
  

    

    public function validationDefault(Validator $validator){

        $validator
            //->requirePresence('name', true,   __("Jméno musí být vyplněno"))
            //->notEmpty('name',__('Jméno musí být vyplněno'))
            
            //->requirePresence('source_id', true,   __("Zdroj musí být vyplněn"))
            //->notEmpty('source_id',__('Zdroj musí být vyplněn'))
        ;


        /*$validator

          ->requirePresence('email', true,   __("Email musí být vyplněn"))

          ->notEmpty('email');*/

        return $validator;

    }

}
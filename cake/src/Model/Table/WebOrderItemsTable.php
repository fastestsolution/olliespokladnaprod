<?php
namespace App\Model\Table;
use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class WebOrderItemsTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->belongsTo("WebOrders");

        $this->addBehavior('Timestamp');
    }

    public function validationDefault(Validator $validator){
        return $validator;
    }

}
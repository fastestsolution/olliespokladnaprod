<?php
namespace App\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\Network\Session;

/**
 * Automaticke odeslani dane entity pri ulozeni na vzdalenou url pomoc postu
 */
class AutoRemoteBehavior extends Behavior{
    private $url = null;
    private $debugFile = null;
    public $sended = null;
    private $autoSend = true;

    public function initialize(array $config){
        $this->Session = new Session();

        if(isset($config['url'])){
            $this->url = $config['url'];
        }
        if(isset($config['debugFile'])){
            $this->debugFile = $config['debugFile'];
        }
    }
   
    /**
     * Pri ulozeni noveho zaznamu odesilame automaticky udaje do cloudu
     */
    public function afterSave(Event $event, Entity $entity){
		if($entity->isNew() && $this->autoSend){
             if(($res = $this->sendToCloud($entity)) !== false){
                $entity = $res;
             }
		}
        return $event;
    }

    /**
     * Setter pro nastaveni privatni promene
     */
    public function setAutoSend($value) {
        $this->autoSend = $value;
    }

    /**
     * Ziskani velikosti 
     */
    public function getQueueSize(){
        return (int) $this->_table->find()->select(['count'=>'COUNT(id)'])->where(['sended IS NULL'])->first()->count;
    }

    /**
     * Odeslani zaznamu ktere se nepodarilo odeslat pri prvotnim vytvoreni (nedostupny internet)
     */
    public function sendQueue($limit = 10, $sendEet = false){
        $result = ['c' => 0, 'ok' => 0, 'e' =>0 ];
        $queue = $this->_table->find()->where(['sended IS NULL'])->limit($limit);
        if ($sendEet) {
            $queue->contain(["EetQueues"]);
        }
        foreach($queue as $item){
            $result['c']++;
            if (isset($item->eet_queue->fik)) {
                $item->fik = $item->eet_queue->fik;
            }
            if (isset($item->eet_queue->bkp)) {
                $item->bkp = $item->eet_queue->bkp;
            }
            if($this->sendToCloud($item) !== false){
                $result['ok']++;
            }else{
                $result['e']++;
            }
        }
        return $result;
    }

    /**
     * Funkce pro odeslani na cloud
     */
    public function sendToCloud($entity){
        if($this->url){
            $loggedUser = $this->Session->read('Auth.User');
            if($loggedUser){
                $entity->user_name = $loggedUser['name'];
                $entity->user_id = $loggedUser['id'];
                $entity->terminal_id = $loggedUser['terminal_id'];
            }
            
            $this->curl_send( API_URL . $this->url , ['save_data'=>json_encode($entity)]);
            
            if($this->sended){
                $entity->sended = $this->sended;
                if($entity->id){
                    $this->_table->updateAll(['sended' => $this->sended], ['id' => $entity->id]);
                }
            }
           
            return $entity;
        }else{
            $this->log('Neni nadefinovano URL pro odeslani'); 
            return false;
        }
    }
    
    /**
     * Curl request na cloud
     */
    protected function curl_send($url, $data){
        $this->sended = null;
		$ch =  curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
                curl_setopt($ch, CURLOPT_TIMEOUT, 10);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $result = curl_exec ($ch);
                
                if($result === false){
                    $result = 'error: '. curl_error($ch) . ' ' . $url;
                }else{
                    $json = json_decode($result, true);
                    if(!$json){
                        $result = $result;
                    }else if($json['result'] == true){
                        $result = 'ok ' . (isset($json['id']) && $json['id'] ? $json['id'] : ''); 
                        if($json['sended']){
                            $this->sended = $json['sended'];
                        }
                    }else{
                        $result = $json['message'];
                    }
                }
        curl_close($ch);
        
        $this->log($result); 
    }
    
    /**
     * Logovani vysledku pripadne chyb do souboru v tmp directory
     */
    private function log($msg){
        if($this->debugFile){
            @file_put_contents( ROOT . '/tmp/logs/' . $this->debugFile, date('Y-m-d H:i:s') . ' - ' . $msg . PHP_EOL, FILE_APPEND);
        }
    }
}
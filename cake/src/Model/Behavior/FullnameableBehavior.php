<?php
namespace App\Model\Behavior;

use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Behavior;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\Utility\Inflector;

class FullnameableBehavior extends Behavior{
 
    public function initialize(array $config){
       
    }
   
    public function make_fullname( Entity $entity){
       
        $entity->name = (empty($entity->last_name) && isset($entity->company_name) ? $entity->company_name : $entity->first_name.' '.$entity->last_name);
        if(isset($entity->_translations) && is_array($entity->_translations)){
            foreach($entity->_translations as $lang => $item){
                if(isset($entity->_translations[$lang]->name)){
                    //$entity->_translations[$lang]->name = $entity->_translations[$lang]->first_name .' ' . $entity->_translations[$lang]->last_name;
                    $entity->_translations[$lang]->name = (empty($entity->_translations[$lang]->last_name) && isset($entity->_translations[$lang]->company_name) ? 
                            $entity->_translations[$lang]->company_name : 
                            $entity->_translations[$lang]->first_name. ' ' .$entity->_translations[$lang]->last_name
                    );
                }
            }
        }
    }

    public function beforeSave(Event $event, EntityInterface $entity){
        $this->make_fullname($entity);
    }
       
}
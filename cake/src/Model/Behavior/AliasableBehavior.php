<?php
namespace App\Model\Behavior;

use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Behavior;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\Utility\Inflector;

class AliasableBehavior extends Behavior{
 
    public function initialize(array $config){
       
    }
   
    public function make_alias( Entity $entity){
        if((!$entity->name || $entity->name == '') && isset($entity->_translations['cz']->name)){
            $entity->name = $entity->_translations['cz']->name;
        }
       
        $entity->alias = friendlyUri($entity->name);
        if(isset($entity->_translations) && is_array($entity->_translations)){
            foreach($entity->_translations as $lang => $item){
                if(isset($entity->_translations[$lang]->alias)){
                    $entity->_translations[$lang]->alias = friendlyUri($entity->_translations[$lang]->name);
                }
            }
        }
    }

    public function beforeSave(Event $event, EntityInterface $entity){
        $this->make_alias($entity);
    }
       
}
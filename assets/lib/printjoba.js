
var iconv = require('iconv-lite'),
cmds = require('./commands');
var Image = require('./image');


/**
* Creates a new Printjob object that can process escpos commands
* which can then be printed with an attached USB escpos printer.
*
* @class
*/
var Printjob = function () {

this._queue = [];

}



Printjob.prototype = {

encode: function () {
    var buf = Buffer.from([0x1b, 0x52, 0x0C]);
    return this;
},

fontSize: function () {
    this._queue.push(Buffer.from([0x1b, 0x4d, 0x01]));
    return this;
},

text: function (text,width,type) {
    // console.log('TEST',text);
    /** PRICE FORMAT */
    function priceFormat(price){
            price = price.toFloat();
            return price
            .toFixed(2) // always two decimal digits
            .replace(".", ",") // replace decimal point character with ,
            .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") + ",-" // use . as a separator
        
        //return price;
    }
    
    if (width && width > 0 && text.length){
        if (type == 'price'){
            text = priceFormat(text);
        
        }
        //console.log(width);
        //console.log(text.length);
        //console.log(text);
        rozdil = width - text.length;
        //console.log('r',rozdil);
        space = '';
        if (rozdil > 0){
            for (i = 1; i <= rozdil; i++){
                space += ' ';
            }
            text += space + '  ';
        } else {
            text = text.substr(0,text.length + rozdil)+'  ';
        }
    } else {
        if (type == 'price'){
            text = priceFormat(text);
        }
    }
    /**/
    //console.log(text);
    //buff = Buffer.from(text);
    //Ä›ĹˇÄŤĹ™ĹľĂ˝ĂˇĂ­Ă© Ĺ ÄŚĹĹ˝ĂťĂĂŤĂ‰
    specialCharacters = {
          "Ä›": 216,
          "Äš": 183,
          "Ĺ": 252,
          "Ĺ™": 253,
          "Ăť": 237,
          "Ă˝": 236,
          "Ăˇ": 160,
          "Ă": 181,
          "ÄŚ": 172,
          "ÄŤ": 159,
          "Ĺ ": 230,
          "Ĺˇ": 231,
          "Ĺ˝": 166,
          "Ĺľ": 167,
          "Ă©": 130,
          "ĂŤ": 214,
          "Ă­": 161,
          "Ă‰": 144,
          "Ä": 209,
          "Ä‘": 208,
          "Ä†": 143,
          "Ä‡": 134,
          "Ăź": 225,
          "áşž": 225,
          "Ă¶": 148,
          "Ă–": 153,
          "Ă„": 142,
          "Ă¤": 132,
          "ĂĽ": 129,
          "Ăś": 154,
          "Ă©": 130,
          "ĹĄ": 156,
          "ĹŻ": 133,
          "Ă˝": 236,
          "|": 0x09,
    };
    buff = text.toString();

    var endBuff = null;
    for(var i=0; i<buff.length; i++){
        
        var value = buff[i];
        var tempBuff = Buffer.from(value);
        for(var key in specialCharacters){
            if(value == key){
                tempBuff = Buffer.from([specialCharacters[key]]);
                break;
            } else {
            
                
            }
        }
        if(endBuff) {
            endBuff = Buffer.concat([endBuff,tempBuff]);
        } else {
            endBuff = tempBuff;
        }
    }
    text_convert = endBuff;
    
    if (text_convert)
        this._queue.push( text_convert );

    return this;

},


/**
  * Add new line(s) to the printjob.
  *
  * @param {number} [count=1] Number of new lines to be added to the current printjob.
  */
newLine: function (count) {

    // DEFAULTS
    count = count || 1;

    var buf = Buffer.from([ cmds.CTL_CR, cmds.CTL_LF ]);
    for (var i = 0; i < count; i++) {
        if (buf)
        this._queue.push(buf);
    }

    return this;

},

newLine2: function (count) {

    // DEFAULTS
    count = count || 1; 

    var buf = Buffer.from([cmds.CTL_LF ]);
    for (var i = 0; i < count; i++) {
        if (buf)
        this._queue.push(buf);
    }

    return this;

},

pad: function (count) {

    // DEFAULTS
    count = count || 1;

    var buf =  Buffer.from([ 0x1b, 0x4a, 0xff, 0x1b, 0x4a, 0xff ]);
    for (var i = 0; i < count; i++) {
        if (buf)
        this._queue.push(buf);
    }

    return this;

},

/**
  * Set text formatting for the current printjob.
  *
  * @param {string} [format='normal'] Text format (one of: 'normal', 'tall', 'wide')
  */
setTextFormat: function (format) {

    // DEFAULTS
    format = format.toLowerCase() || 'normal';

    var formats = {
        normal: 	cmds.TXT_NORMAL,
        tall: 		cmds.TXT_2HEIGHT,
        wide: 		cmds.TXT_2WIDTH
    };

    var cmd = formats[ format ];

    if (cmd) {
        this._queue.push(Buffer.from(cmd));
    }
    else {
        throw new Error('Text format must be one of: ', Object.keys(formats).join(', '));
    }

    return this;

},

/**
  * Set text alignment for the current printjob.
  *
  * @param {string} [count='left'] Text alignment (one of: 'left', 'center', 'right')
  */
setTextAlignment: function (align) {

    // DEFAULTS
    align = align.toLowerCase() || 'left';

    var aligns = {
        left: 		cmds.TXT_ALIGN_LT,
        center: 	cmds.TXT_ALIGN_CT,
        right: 		cmds.TXT_ALIGN_RT
    };

    var cmd = aligns[ align ];
    if (cmd) {
        this._queue.push(Buffer.from(cmd));
    }
    else {
        throw new Error('Text alignment must be one of: ', Object.keys(aligns).join(', '));
    }

    return this;

},

/**
  * Set underline for the current printjob.
  *
  * @param {boolean} [underline=true] Enables/disables underlined text
  */
setUnderline: function (underline) {

    // DEFAULTS
    if (typeof underline !== 'boolean') {
        underline = true;
    }

    var cmd = underline ? cmds.TXT_UNDERL_ON : cmds.TXT_UNDERL_OFF;
    this._queue.push(Buffer.from(cmd));

    return this;

},

setTab: function(){
    
    var cmd = cmds.CTL_HT;
    this._queue.push(Buffer.from(cmd));

    return this;
},

/**
  * Set text bold for the current printjob.
  *
  * @param {boolean} [bold=true] Enables/disables bold text
  */
setBold: function (bold) {

    // DEFAULTS
    if (typeof bold !== 'boolean') {
        bold = true;
    }

    var cmd = bold ? cmds.TXT_BOLD_ON : cmds.TXT_BOLD_OFF;
    this._queue.push(Buffer.from(cmd));

    return this;

},
qrimage: function(content, options, callback){
  var self = this;
  if(typeof options == 'function'){
    callback = options;
    options = null;
  }
  options = options || { type: 'png', mode: 'dhdw' };
  var buffer = qr.imageSync(content, options);
  var type = [ 'image', options.type ].join('/');
  getPixels(buffer, type, function (err, pixels) {
    if(err) return callback && callback(err);
    self.raster(new Image(pixels), options.mode);
    callback && callback.call(self, null, self);
  });
  return this;
},


imageAdd: function(logo){
        console.log('llll',logo);
        if(!logo){
            return this;
        }
            var pixels = [];
            for (var i = 0; i < logo.height; i++) {
                var line = [];
                for (var j = 0; j < logo.width; j++) {
                  var idx = (logo.width * i + j) << 2;
                  line.push({
                    r: logo.data[idx],
                    g: logo.data[idx + 1],
                    b: logo.data[idx + 2],
                    a: logo.data[idx + 3]
                  });
                }
                pixels.push(line);
            }
            //console.log(pixels);
            var imageBuffer = Buffer.from([]);
            for (var i = 0; i < logo.height; i++) {
                    for (var j = 0; j < parseInt(logo.width/8); j++) {
                      var byte = 0x0;
                      for (var k = 0; k < 8; k++) {
                        var pixel = pixels[i][j*8 + k];
                        if(pixel.a > 126){ // checking transparency
                          grayscale = parseInt(0.2126 * pixel.r + 0.7152 * pixel.g + 0.0722 * pixel.b);

                          if(grayscale < 128){ // checking color
                            var mask = 1 << 7-k; // setting bitwise mask
                            byte |= mask; // setting the correct bit to 1
                          }
                        }
                      }
                      var tmpBfr =  Buffer.from([byte]);
                      imageBuffer = Buffer.concat([imageBuffer, tmpBfr]);
                    }
            }
              // Print raster bit image
              // GS v 0
              // 1D 76 30	m	xL xH	yL yH d1...dk
              // xL = (this.width >> 3) & 0xff;
              // xH = 0x00;
              // yL = this.height & 0xff;
              // yH = (this.height >> 8) & 0xff;
              // https://reference.epson-biz.com/modules/ref_escpos/index.php?content_id=94
            img = [];
              //console.log(logo.height);
              this._queue.push(Buffer.from([0x1d, 0x76, 0x30, 0x00]));
              this._queue.push(Buffer.from([(logo.width >> 3) & 0xff]));
              this._queue.push(Buffer.from([0x00]));
              this._queue.push(Buffer.from([logo.height & 0xff]));
              this._queue.push(Buffer.from([(logo.height >> 8) & 0xff]));

            // append data
            this._queue.push(imageBuffer);
    return this;
},

/**
 * [image description]
 * @param  {[type]} image   [description]
 * @param  {[type]} density [description]
 * @return {[type]}         [description]
 */
image : function(image, density){
    if(!(image instanceof Image)) 
        throw new TypeError('Only escpos.Image supported');
    density = density || 'd24';
    var n = !!~[ 'd8', 's8' ].indexOf(density) ? 1 : 3;
    
    //var header = _.BITMAP_FORMAT['BITMAP_' + density.toUpperCase()];
    var header = cmds.S_RASTER_N;
    var bitmap = image.toBitmap(n * 8);
    var self = this;

    bitmap.data.forEach(function (line) {
        this._queue.push(Buffer.from(header));
        this._queue.push(Buffer.from(line.length / n));
        this._queue.push(Buffer.from(line));
        this._queue.push(Buffer.from('_.EOL'));
    }.bind(this));
    console.log(this.queue);
 return this;
},

/**
  * Set text font for the current printjob.
  *
  * @param {string} [font='A'] Text font (one of: 'A', 'B')
  */
setEncode: function(){
    this._queue.push(Buffer.from(cmds.ENCODE));
    return this; 
},

setFont: function (font) {

    // DEFAULTS
    font = font.toUpperCase() || 'A';

    var fonts = {
        A: 	cmds.TXT_NORMAL,
        B: 	cmds.TXT_2HEIGHT
    };
    if (fonts[ font ]) {
        this._queue.push(Buffer.from(fonts[ font ]));
    }
    else {
        throw new Error('Font must be one of: ', Object.keys(fonts).join(', '));
    }

    return this;

},

separator: function () {

    var i = 0
    var line = ''
    var width = 42;
    while (i < width) {
        line += '-'
        i++
    }

    return this.text(line);

},

/**
  * Cuts paper on the current printjob.
  */
cut: function () {
    this._queue.push(Buffer.from(cmds.PAPER_FULL_CUT));
    return this;
},

/**
  * Kicks cash drawer on the current printjob.
  * Default parameters are for Epson TM-88V from: http://keyhut.com/popopen.htm
  *
  * @param {number} [pin=2] Pin number used to send the pulse (one of: 2, 5)
  * @param {number} [t1=110] Pulse ON time in ms (0 <= t1 <= 510)
  * @param {number} [t2=242] Pulse OFF time in ms (0 <= t2 <= 510)
  */
cashdraw: function (pin, t1, t2) {

    // DEFAULTS
    pin = pin || 2;
    t1 = t1 || 110;
    t2 = t2 || 242;

    var buf = Buffer.from(5); 

    if (pin == 2) {
        Buffer.from(cmds.CD_KICK_2).copy(buf);
    }
    else if (pin == 5) {
        Buffer.from(cmds.CD_KICK_5).copy(buf);
    }
    else {
        throw new Error('Pin must be one of: 2, 5');
    }

    if (t1 >= 0 && t2 >= 0 && t1 <= 242 && t2 <= 242) {
        // Pulse ON/OFF times in 2ms increments
        buf.writeUInt8(t1/2, 3);
        buf.writeUInt8(t2/2, 4);
    }
    else {
        throw new Error('Pulse timings must be between 0 and 242 inclusive.');
    }

    this._queue.push(buf);

    return this;

},

printData: function () {
    var init = Buffer.from(cmds.HW_INIT);

    var queue = this._queue.slice(0);		// Clone queue
        queue.unshift(init);				// Prepend init command
    if (queue.toString().length != ''){
        return Buffer.concat(queue);
    }

}

}



module.exports = Printjob;



